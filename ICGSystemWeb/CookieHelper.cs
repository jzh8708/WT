﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;


public class CookieHelper
{
    private static int expiresDay = 7;

    public CookieHelper()
    {

    }

    public static object Get(string groupName, string key)
    {
        HttpCookie cookie = HttpContext.Current.Request.Cookies[groupName];
        if (cookie != null)
        {
            object obj = HttpContext.Current.Request.Cookies[groupName][key];
            if (obj != null)
            {
                return obj;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }


    public static void Set(string groupName, string key, string cookieValue)
    {
        HttpCookie cookie;
        if (Get(groupName, key) == null)
        {
            cookie = new HttpCookie(key);
        }
        else
        {
            cookie = HttpContext.Current.Request.Cookies[groupName];
        }
        cookie.Values[key] = cookieValue;
        if (expiresDay > 0)
        {
            cookie.Expires = DateTime.Now.AddDays(expiresDay);
        }
        HttpContext.Current.Response.AppendCookie(cookie);
    }

    public static void RemoveAll()
    {
        System.Web.HttpContext.Current.Session.Contents.Clear();
        foreach (string cookiename in System.Web.HttpContext.Current.Request.Cookies.AllKeys)
        {
            if (cookiename != "langpath" && cookiename != "loginname")
            {
                HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies[cookiename];
                cookie.Expires = DateTime.Today.AddDays(-5);
                System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
    }

    public static void Remove(string groupName, string cookieName)
    {
        if (Get(groupName, cookieName) != null)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[groupName];
            cookie.Expires = DateTime.Now.AddDays(-15);
            HttpContext.Current.Response.AppendCookie(cookie);
        }
    }
}