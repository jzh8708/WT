﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App.Master" AutoEventWireup="true"
    CodeBehind="VideoManager.aspx.cs" Inherits="ICGSystemWeb.VideoManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMaaster" runat="server">
    <div class="row-fluid sortable" id="divmaillist">
        <div class="box span12" id="divshow">
            <div class="box-header well" data-original-title>
                <h2 id="hlistname">
                    <i class="icon-user"></i>
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th id="thlable009" style="width: 30%">
                                视频名称
                            </th>
                            <th style="width: 15%">
                                视频大小
                            </th>
                            <th style="width: 15%">
                                上传时间
                            </th>
                            <th style="width: 40%">
                                操作
                            </th>
                        </tr>
                    </thead>
                    <tbody id="datalist">
                    </tbody>
                </table>
                <div align="center">
                    <input type="button" class="btn" value="加载更多" id="btnmore" style="display: none" />
                </div>
                <div class="row-fluid">
                    <div class="span12">
                    </div>
                    <div class="span12 center" id="datainfoa">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid sortable" id="div1">
        <div class="box span12" id="div2">
            <div class="box-header well" data-original-title>
                <h2 id="h1">
                    <i class="icon-user"></i>
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <div class="control-group success">
                    <label class="control-label" for="inputSuccess">
                        视频名称</label>
                    <div class="controls">
                        <input class="validate[maxSize[36],custom[noSpecialCaracters]]" id="txttitle" type="text" />
                    </div>
                </div>
                <div class="control-group success">
                    <label class="control-label" for="inputSuccess">
                        上传视频</label>
                    <div class="controls">
                        <input id="btnvideoup" value="上传" type="button" /> <a id="hrefname"></a>
                    </div>
                </div>
                <div class="control-group success">
                    <label class="control-label" for="inputSuccess">
                        截图</label>
                    <div class="controls">
                        <input id="btnimgup" value="上传" type="button" /><a id="imgname"></a>
                    </div>
                </div>
            </div>
            <div align="left">
                <button type="button" class="btn btn-primary" id="btnsave">
                    保存</button>
                <input type="button" class="btn" value="取消" id="btncancel" />
            </div>
            <br />
        </div>
    </div>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript" src="Script/ajax_upload.js"></script>
    <script type="text/javascript">
        var href = "", poster = "", toptime = "";
        var videosize = 0;

        $(function () {
            // 文件上传 ------------------------- begin(华丽的分隔符) ---------------------------
            var button = $('#btnvideoup'), interval;
            new AjaxUpload(button, {
                action: 'userajax/FileMail.ashx',
                name: 'imgFile',
                onSubmit: function (file, ext) {
                    this.disable();
                    interval = window.setInterval(function () {
                        var text = button.text();
                    }, 200);
                },
                onComplete: function (file, response) {
                    window.clearInterval(interval);
                    this.enable();
                    response = response.replace(/<pre>/ig, "").replace(/<\/pre>/ig, ""); //过滤

                    var obj = $.parseJSON(response);
                    if (obj[0].filetruename == "ferror") { alert("该文件类型不允许上传!"); return false; }
                    if (obj[0].filetruename == "big") { alert("文件过大!"); return false; }


                    href = obj[0].filepath;
                    videosize = obj[0].filesize;

                    $("#hrefname").html(obj[0].filetruename);
                }
            });


            var buttonimg = $('#btnimgup'), interval;
            new AjaxUpload(buttonimg, {
                action: 'userajax/FileMail.ashx',
                name: 'imgFile',
                onSubmit: function (file, ext) {
                    this.disable();
                    interval = window.setInterval(function () {
                        var text = buttonimg.text();
                    }, 200);
                },
                onComplete: function (file, response) {
                    window.clearInterval(interval);
                    this.enable();
                    response = response.replace(/<pre>/ig, "").replace(/<\/pre>/ig, ""); //过滤

                    var obj = $.parseJSON(response);
                    if (obj[0].filetruename == "ferror") { alert("该文件类型不允许上传!"); return false; }
                    if (obj[0].filetruename == "big") { alert("文件过大!"); return false; }

                    poster = obj[0].filepath;
                    $("#imgname").html(obj[0].filetruename);
                }
            });


            $("#btnsave").click(function () {

                var videotitle = $("#txttitle").val();

                if (videotitle == "") {
                    alerttip("请填写标题");
                    return;
                }
                else if (href == "") {
                    alerttip("请上传视频文件");
                    return;
                }
                else if (poster == "") {
                    alerttip("请上传视频截图");
                    return;
                }

                $.post("userajax/Video.aspx", { "type": "add", "title": videotitle, "href": href, "poster": poster, "videosize": videosize }, function (datare) {
                    if (datare == "1") {
                        success("添加成功");
                        href = "";
                        poster = "";
                        $("#txttitle").val("");
                        toptime = "";
                        ReLoad();
                    }
                })
            });

            ReLoad();

        })
        //dom end 

        function ReLoad() {

            $("#datalist").html("");

            $.post("userajax/Video.aspx", { "type": "getlist", "toptime": toptime }, function (datare) {

                var obj = $.parseJSON(datare);

                if (obj == null) return;

                toptime = obj[obj.length - 1].createtime;

                for (var i = 0; i < obj.length; i++) {
                    $("#datalist").append("<tr id='TRList" + obj[i].title + "'><td>" + obj[i].title + "</td><td class='center'>" + obj[i].videosize + "KB</td><td class='center'>" + obj[i].createtime + "</td><td class='center'><a class='btn btn-info' onclick=domains('view','" + obj[i].ID + "')><i class='icon-edit icon-white'></i>修改</a>&nbsp;<a class='btn btn-danger' onclick=domains('delete','" + obj[i].ID + "')><i class='icon-trash icon-white'></i> 删除</a></td></tr>");
                }
            })
        }
    </script>
</asp:Content>
