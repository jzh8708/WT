﻿$(function () {


    jQuery("#divsearch").validationEngine(); //验证

    var toptime = "";
    var step = 20; //每次点击显示20条数据日志

    //更多
    $("#btnmore").click(function () {
        GetUserList();
    });

    //搜索(时间)
    $("#btnsearchs").click(function () {

        if ($("#d1").val() == "" || $("#d2").val() == "") {
            alerttip("请选择日期段");
            return;
        }

        var name = $("#txtsearch").val();

        $("#tbodyList").html("");

        $.post("userajax/OpreateLog.aspx", { "type": "Searchdate", "d1": $("#d1").val(), "d2": $("#d2").val(), "name": name }, function (data) {
            if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
            if (data == "no") {
                $("#anodata").hide();
                $("#btnmore").hide();
                $("#acount1").html("0条结果!"); return;
            }
            $("#btnmore").hide();
            $("#anodata").hide();

            var objresult = $.parseJSON(data);

            $("#acount1").html(objresult.length + "条结果!");

            for (var i = 0; i < objresult.length; i++)
                $("#tbodyList").append("<tr id='td" + objresult[i].GUID + "'><td><input name='checkboxD'  onclick=CK('" + objresult[i].GUID + "')  type='checkbox' myguid='" + objresult[i].GUID + "' /></td><td>" + objresult[i].OPREATETIME + "</td><td class='center'>" + objresult[i].ORGNAME + "</td><td class='center'>" + objresult[i].CONTENT + "</td><td class='center'><span class='label label-important'  onclick=doma('" + objresult[i].GUID + "')>删除</span></td></tr>");
        });
    });

    //根据当前用户获取日志列表数据
    function GetUserList() {

        if (toptime == "")
            $("#tbodyList").html("");

        $.post("userajax/OpreateLog.aspx", { "type": "Get", "toptime": toptime }, function (data) {
            if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
            if (data == "no") {
                $("#anodata").show();
                $("#btnmore").hide();
                return;
            }

            $("#anodata").hide();

            var objresult = $.parseJSON(data);

            toptime = objresult[objresult.length - 1].OPREATETIME; //最后的时间

            if (objresult.length < step)
                $("#btnmore").hide();
            else
                $("#btnmore").show();

            for (var i = 0; i < objresult.length; i++)
                $("#tbodyList").append("<tr id='td" + objresult[i].GUID + "'><td><input name='checkboxD' onclick=CK('" + objresult[i].GUID + "') type='checkbox' myguid='" + objresult[i].GUID + "' /></td><td>" + objresult[i].OPREATETIME + "</td><td class='center'>" + objresult[i].ORGNAME + "</td><td class='center'>" + objresult[i].CONTENT + "</td><td class='center'><span class='label label-important'  onclick=doma('" + objresult[i].GUID + "')>删除</span></td></tr>");

        })
    }

    GetUserList();

    //删除
    $("#selectalldelete").click(function () {


        var idlist = "";

        var ob = $("input[name='checkboxD']");
        $(ob).each(function () {
            if ($(this).attr("checked") == "checked")
                idlist = idlist + $(this).attr("myguid") + ",";
        });
        if (idlist.length > 1)
            idlist = idlist.substr(0, idlist.length - 1);

        if (idlist.length < 1) {
            $("#selectalldelete").hide();
            alert("未选择需要删除的数据!");
            return;
        }
        if (confirm("确定删除吗?") == false) return;
        $.post("userajax/OpreateLog.aspx", { "type": "delist", "id": idlist }, function (data) {
            if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
            if (data == "yes") {
                var array = [];
                array = idlist.split(',');
                for (var i = 0; i < array.length; i++)
                    $("#td" + array[i]).remove();
            }
            else
                alert("删除失败");
        });
    });
    //全选
    var isck = 0;
    $("#Buttonsleall").click(function () {
        if (isck == 0) {
            var objall = $("input[name = 'checkboxD']");
            $(objall).each(function () {
                $(this).attr("checked", true);
            });
            isck = 1;
            $("#Buttonsleall").val("取消");
            $("#selectalldelete").show();
            return;
        }
        else {
            var objall = $("input[name = 'checkboxD']");
            $(objall).each(function () {
                $(this).attr("checked", false);
            });
            isck = 0;
            $("#Buttonsleall").val("全选");
            $("#selectalldelete").hide();
            return;
        }
    });

    //清空
    $("#btncleardata").click(function () {
        $("#txtsearch").val("");
        $("#d1").val("");
        $("#d2").val("");
    });

})
                //dome end

            var idarray = [];
            function CK(id) {
                $("#selectalldelete").show();
                return;
            }

            function CKexists(id) {
                for (var i = 0; i < idarray.length; i++) {
                    if (idarrayp[i] == id)
                        return i;
                }
                return -1;
            }

            function DeleteoneID(id) {
                for (var i = 0; i < idarray.length; i++) {
                    if (idarrayp[i] == id) {
                        idarray.pop(i);
                        break;
                    }
                }
            }

            //单个删除
            function doma(id) {
               
                if (confirm("确定删除吗?") == false) return;
                $.post("userajax/OpreateLog.aspx", { "type": "de", "id": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "yes")
                        $("#td" + id).remove();
                    else
                        alert("删除失败");
                });
            }
		 