﻿		var userid = 1000;
        var startpageindex = 1, step = 7;//分页容量大小
        $(function () {

            //页面加载执行
            jQuery("#DivUserAdd").validationEngine(); //验证
            jQuery("#DivEdit").validationEngine(); //验证
            jQuery("#divsearch").validationEngine(); //验证


            //根据当前用户获取用户列表
            function GetUserList() {

                if (startpageindex == 1)
                    $("#tbodyList").html(""); 

                $.post("Ajax/UserMag.aspx", { "type": "getli", "userid": userid, "startpageindex": startpageindex }, function (data) {
				    if(data=="-999"){alert("登录失效.请重新登录!");return false;}
                    if (data == "no") {
                        $("#anodata").show();
                        $("#btnmore").hide(); 
                        return;
                    }

                    $("#anodata").hide();

                    var objresult = $.parseJSON(data);

                    startpageindex = parseInt(startpageindex + objresult.length);

                    if (objresult.length < step)
                        $("#btnmore").hide();
                    else
                        $("#btnmore").show();

                    for (var i = 0; i < objresult.length; i++)
                        $("#tbodyList").append("<tr id='TRList" + objresult[i].USERID + "'><td>" + objresult[i].MYUSERNAME + "</td><td class='center'>" + objresult[i].LASTLOGINTIME + "</td><td class='center'><span  id='sp" + objresult[i].USERID + "' mytype='" + objresult[i].USERTYPE + "'  onclick=maildo2('" + objresult[i].USERTYPE + "','role','" + objresult[i].USERID + "')    class='" + (objresult[i].USERTYPE == "0" || objresult[i].USERTYPE == 0 ? "label label-important" : "label label-warning") + "'> " + (objresult[i].USERTYPE == "0" || objresult[i].USERTYPE == 0 ? "内网用户" : "外网用户") + "</span></td> <td class='center'><span  id='sptwo" + objresult[i].USERID + "' mytype='" + objresult[i].ISALLOWEDLOGIN + "'  onclick=maildo2('" + objresult[i].ISALLOWEDLOGIN + "','isalogin','" + objresult[i].USERID + "')   class='label" + (objresult[i].ISALLOWEDLOGIN == "1" || objresult[i].ISALLOWEDLOGIN == 1 ? " label-success" : "") + "'>" + (objresult[i].ISALLOWEDLOGIN == "1" || objresult[i].ISALLOWEDLOGIN == 1 ? " 可登录" : "不可登录") + "</span></td>    <td class='center'><table border='0'><tr border='0'><td><a   class='btn btn-success'  onclick=maildo('view','" + objresult[i].USERID + "')><i class='icon-zoom-in icon-white'></i> 查看</a></td><td><a class='btn btn-info'    onclick=maildo('edit','" + objresult[i].USERID + "')><i class='icon-edit icon-white'></i> 编辑</a></td><td><a class='btn'  id='apass" + objresult[i].USERID + "'  onclick=maildo('repass','" + objresult[i].USERID + "')>  重置密码</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteoutbox','" + objresult[i].USERID + "')><i class='icon-trash icon-white'></i>   删除</a></td></tr></table></td></tr>");
                })
            }

            GetUserList();

            //获取更多
            $("#btnmore").click(function () {
                GetUserList();
            });
            //搜索
            $("#txtsearch").keyup(function () {
                SearchDtList();
            });
            //新增
            $("#spadd").click(function () {
                //$("#DivUserAdd").modal("show");
				location.href = "ManageUser.aspx";
            });
            //新增确定
            $("#laadd").click(function () {
                if (!jQuery('#DivUserAdd').validationEngine('validate')) {
                    return;
                }
                var arraycol = [];
                arraycol.push("MYUSERNAME");
                arraycol.push("USERTRUENAME");
                arraycol.push("SEX");
                arraycol.push("UKEY");
                arraycol.push("USERADDRESS");
                arraycol.push("USERMOBLEPHONE");
                arraycol.push("USERPHONE");
                arraycol.push("FAX");
                arraycol.push("USERIDCARDNUM");
                arraycol.push("USEREMAIL");
                arraycol.push("USERCOLLEGE");
                arraycol.push("USERPOSTCODE");
                arraycol.push("USERPOST");
                arraycol.push("USERBIRTHDAY");
                arraycol.push("COMPANYTEL");
                for (var i = 0; i < arraycol.length; i++)
                    arraycol[i] = arraycol[i] + ":" + $("input[myid='" + arraycol[i] + "']").val();

                $.post("Ajax/UserMag.aspx", { "type": "new", "val": arraycol.toString() }, function (data) {
                    if(data=="-999"){alert("登录失效.请重新登录!");return false;}
					if (data == "1") {
                        $("#DivUserAdd").modal("hide");
                        $("#txtsearch").val($("input[myid='" + "MYUSERNAME" + "']").val());
                        SearchDtList();
                    }
                    else if (data == "-1000") {
                        alert("此单位已存在！");
                        return false;
                    }
                    else if (data == "-2000") {
                        alert("此Ukey已存在！");
                        return false;
                    }
                })
            });
            //搜索
			var scname="scname";
            function SearchDtList() {

                var name = $("#txtsearch").val();
				
				if(scname==name){return;}
				scname=name;

                if (name == "") { startpageindex = 1; $("#acount").html(""); GetUserList(); return; }

                if (name == " " || name == "" || name == "'" || name == "," || name == "!" || name == '"' || name == "/" || name == "@") { $("#acount").html("非法字符!"); return; } //过滤字符

                $("#tbodyList").html("");

                $.post("Ajax/UserMag.aspx", { "type": "Search", "userid": userid, "content": name }, function (data) {
				    if(data=="-999"){alert("登录失效.请重新登录!");return false;}
                    if (data == "no") {
                        $("#anodata").hide();
                        $("#btnmore").hide();
                        $("#acount").html("0条结果!"); return;
                    }
                    $("#btnmore").hide();
                    $("#anodata").hide();

                    var objresult = $.parseJSON(data);

                    $("#acount").html(objresult.length + "条结果!");

                    for (var i = 0; i < objresult.length; i++)
                        $("#tbodyList").append("<tr id='TRList" + objresult[i].USERID + "'><td>" + objresult[i].MYUSERNAME + "</td><td class='center'>" + objresult[i].LASTLOGINTIME + "</td><td class='center'><span  id='sp" + objresult[i].USERID + "' mytype='" + objresult[i].USERTYPE + "'  onclick=maildo2('" + objresult[i].USERTYPE + "','role','" + objresult[i].USERID + "')    class='" + (objresult[i].USERTYPE == "0" || objresult[i].USERTYPE == 0 ? "label label-important" : "label label-warning") + "'> " + (objresult[i].USERTYPE == "0" || objresult[i].USERTYPE == 0 ? "内网用户" : "外网用户") + "</span></td> <td class='center'><span  id='sptwo" + objresult[i].USERID + "' mytype='" + objresult[i].ISALLOWEDLOGIN + "'  onclick=maildo2('" + objresult[i].ISALLOWEDLOGIN + "','isalogin','" + objresult[i].USERID + "')   class='label" + (objresult[i].ISALLOWEDLOGIN == "1" || objresult[i].ISALLOWEDLOGIN == 1 ? " label-success" : "") + "'>" + (objresult[i].ISALLOWEDLOGIN == "1" || objresult[i].ISALLOWEDLOGIN == 1 ? " 可登录" : "不可登录") + "</span></td>    <td class='center'><table border='0'><tr border='0'><td><a   class='btn btn-success'  onclick=maildo('view','" + objresult[i].USERID + "')><i class='icon-zoom-in icon-white'></i> 查看</a></td><td><a class='btn btn-info'    onclick=maildo('edit','" + objresult[i].USERID + "')><i class='icon-edit icon-white'></i> 编辑</a></td><td><a class='btn'  id='apass" + objresult[i].USERID + "'  onclick=maildo('repass','" + objresult[i].USERID + "')>  重置密码</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteoutbox','" + objresult[i].USERID + "')><i class='icon-trash icon-white'></i>   删除</a></td></tr></table></td></tr>");
                })
            }

            //点击修改
            $("#laupdate").click(function () {

                if (!jQuery('#DivEdit').validationEngine('validate')) {
                    return;
                }

                var beid = $("#laupdate").attr("myupdateid");
                if (beid == "-100") { alert("请选择！"); return; }
                var arraycol = [];
                arraycol.push("MYUSERNAME");
                arraycol.push("USERTRUENAME");
                arraycol.push("SEX");
                arraycol.push("USERADDRESS");
                arraycol.push("USERMOBLEPHONE");
                arraycol.push("USERPHONE");
                arraycol.push("FAX");
                arraycol.push("USERIDCARDNUM");
                arraycol.push("USEREMAIL");
                arraycol.push("USERCOLLEGE");
                arraycol.push("USERPOSTCODE");
                arraycol.push("USERPOST");
                arraycol.push("USERBIRTHDAY");
                arraycol.push("COMPANYTEL");
                for (var i = 0; i < arraycol.length; i++)
                    arraycol[i] = arraycol[i] + ":" + $("#" + arraycol[i]).val();

                $.post("Ajax/UserMag.aspx", { "type": "upd", "beid": beid, "val": arraycol.toString() }, function (data) {
                    if(data=="-999"){alert("登录失效.请重新登录!");return false;}
					if (data == "1") {
						$("#DivEdit").modal("hide");//close this div
                        maildo("view", beid);//show view div data
                    }
                })
            })
        })
        //dom end

        //変更用户角色
        function maildo2(a, type, id) {

            //设置角色(内网，外网)
            if (type == "role") {
                return false;
                var val = -1;

                if ($("#sp" + id).attr("mytype") == "0") {
                    if (confirm("确定变更用户为外网用户吗?"))
                        val = 1;
                    else return;
                }
                else if ($("#sp" + id).attr("mytype") == "1") {
                    if (confirm("确定变更用户为内网用户吗?"))
                        val = 0;
                    else return;
                }

                $.post("Ajax/UserMag.aspx", { "type": "settypeval", "userid": userid, "beuserid": id, "val": val }, function (data) {
                    if(data=="-999"){alert("登录失效.请重新登录!");return false;}
					if (data == "1") {
                        $("#sp" + id).attr("mytype", val);
                        if (val == 0) {
                            $("#sp" + id).html("内网用户");
                            $("#sp" + id).attr("class", "label label-important");
                        }
                        else if (val == 1) {
                            $("#sp" + id).html("外网用户");
                            $("#sp" + id).attr("class", "label label-warning");
                        }
                    }
                    else if (data == "-500") {
                        alert("自己不能进行此操作！");
                        return;
                    }
                });
            }
            //设置登录
            else if (type == "isalogin") {
                var val = -1;
                if ($("#sptwo" + id).attr("mytype") == "1" || $("#sptwo" + id).attr("mytype") == 1) {
                    if (confirm("确定变更用户为不可登录用户吗？") == true)
                        val = 0;
                    else return;
                }
                else if ($("#sptwo" + id).attr("mytype") == "0" || $("#sptwo" + id).attr("mytype") == 0) {
                    if (confirm("确定变更用户为可登录用户吗？") == true)
                        val = 1;
                    else return;
                }
                $.post("Ajax/UserMag.aspx", { "type": "setlogin", "userid": userid, "beuserid": id, "val": val }, function (data) {
                    if(data=="-999"){alert("登录失效.请重新登录!");return false;}
					if (data == "1" || data == 1) {
                        $("#sptwo" + id).attr("mytype", val);
                        if (val == 0) {
                            $("#sptwo" + id).html("不可登录");
                            $("#sptwo" + id).attr("class", "label");
                        }
                        else if (val == 1) {
                            $("#sptwo" + id).html("可登录");
                            $("#sptwo" + id).attr("class", "label label-success");
                        }
                    }
                    else if (data == "-500") {
                        alert("自己不能进行此操作！");
                        return;
                    }
                });
            }
        }

        //function for button
        function maildo(type, id) {
            //重置密码
            if (type == "repass") {
                alert("该功能暂不能使用!");
                return false;
                var val = -1;
                if (confirm("确定重置该用户的密码吗？") == false)
                    return;
                $.post("Ajax/UserMag.aspx", { "type": "resetpass", "userid": userid, "beuserid": id, "val": val }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "1") {
                        $("#apass" + id).html("重置成功");
                    }
                    else if (data == "-500") {
                        alert("自己不能进行此操作！");
                        return;
                    }
                });
            }
            //查看
            else if (type == "view") {
                $("#myModalA").modal("show");
                $.post("Ajax/UserMag.aspx", { "type": "view", "userid": userid, "viewuid": id, "val": val }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") return;
                    var arraycol = [];
                    arraycol.push("MYUSERNAME");
                    arraycol.push("ISALLOWEDLOGIN");
                    arraycol.push("LASTLOGINTIME");
                    arraycol.push("USERTRUENAME");
                    arraycol.push("USERTYPE");
                    arraycol.push("LASTLOGINIPADDS");
                    arraycol.push("SEX");
                    arraycol.push("USERADDRESS");
                    arraycol.push("USERMOBLEPHONE");
                    arraycol.push("USERPHONE");
                    arraycol.push("FAX");
                    arraycol.push("USERIDCARDNUM");
                    arraycol.push("USEREMAIL");
                    arraycol.push("USERCOLLEGE");
                    arraycol.push("USERPOSTCODE");
                    arraycol.push("USERPOST");
                    arraycol.push("USERBIRTHDAY");
                    arraycol.push("COMPANYTEL");
                    arraycol.push("CERTIDTYPE");

                    var obj = $.parseJSON(data);

                    for (var i = 0; i < arraycol.length; i++) {

                        if (obj[0][arraycol[i]].toString().length < 1) {
                            $("#TRL" + arraycol[i]).hide();
                            continue;
                        }
                        else $("#TRL" + arraycol[i]).show();

                        if (arraycol[i] == "ISALLOWEDLOGIN")
                            $("#L" + arraycol[i]).html("<span style='font-size:20px;'>" + (obj[0][arraycol[i]] == "1" ? "允许登录" : "不可登录") + "</span>");
                        else if (arraycol[i] == "USERTYPE")
                            $("#L" + arraycol[i]).html("<span style='font-size:20px;'>" + (obj[0][arraycol[i]] == "0" ? "内网" : "外网") + "</span>");
                        else
                            $("#L" + arraycol[i]).html("<span style='font-size:20px;'>" + obj[0][arraycol[i]].replace("0:00:00", "") + "</span>"); //查看
                    }
                })
            }
            else if (type == "edit") {
                $("#laupdate").attr("myupdateid", id);
                $("#DivEdit").modal("show");
                $.post("Ajax/UserMag.aspx", { "type": "view", "userid": userid, "viewuid": id, "val": val }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") return;
                    var arraycol = [];
                    arraycol.push("MYUSERNAME");
                    arraycol.push("USERTRUENAME");
                    arraycol.push("SEX");
                    arraycol.push("USERADDRESS");
                    arraycol.push("USERMOBLEPHONE");
                    arraycol.push("USERPHONE");
                    arraycol.push("FAX");
                    arraycol.push("USERIDCARDNUM");
                    arraycol.push("USEREMAIL");
                    arraycol.push("USERCOLLEGE");
                    arraycol.push("USERPOSTCODE");
                    arraycol.push("USERPOST");
                    arraycol.push("USERBIRTHDAY");
                    arraycol.push("COMPANYTEL");

                    var obj = $.parseJSON(data);

                    for (var i = 0; i < arraycol.length; i++)
                        $("#" + arraycol[i]).val(obj[0][arraycol[i]].replace("0:00:00", "")); //绑定修改

                    $("#SEX").val($("#SEX").val().substr(0, 1)); //去掉空格
                })
            }
            else if (type == "deleteoutbox") {
                if (confirm("确定删除该用户吗") == true) {
                    $.post("Ajax/UserMag.aspx", { "type": "deleteoutbox", "userid": userid, "did": id, "val": val }, function (data) {
                        if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                        if (data == "1")
                            $("#TRList" + id).remove();
                        else if (data == "-500") {
                            alert("自己不能删除自己!");
                        }
                        else {
                            alert("删除失败!");
                            return false;
                        }
                    })
                }
            }

        }