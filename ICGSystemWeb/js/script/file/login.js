﻿var url = "218.240.142.98"; //可配置
    var isarikey = 0; //证书状态

    $(function () {

        $("#btnlogin").click(function () {
            if (isarikey == 0) {
                alert("请插入Ukey...");
                return;
            }
            location.href = "https://" + url + "/UkeyCk.aspx";
        })
    })

    var mark = "";

    //获取客户端的Ukey
    function GetInfo() {

        var SECUINTER_CURRENT_USER_STORE = 1;

        var SECUINTER_MY_STORE = 0;

        var SECUINTER_KEYUSAGE_DIGITALSIGNATURE = 0x1;
        var SECUINTER_KEYUSAGE_NONREPUDIATION = 0x2;

        var SECUINTER_LDAP_NAMESTRING = 0x1;

        var storeobj;
        var certobj;
        var certsobj;
        var storecertsobj;
        var i;
        var count;

        storeobj = new ActiveXObject("SecuInter.Store");
        storeobj.Open(SECUINTER_CURRENT_USER_STORE, SECUINTER_MY_STORE);
        storecerts = storeobj.X509Certificates;
        storeobj.Close();

        certsobj = new ActiveXObject("SecuInter.X509Certificates");
        count = storecerts.Count;
        for (i = 0; i < count; i++) {
            certobj = storecerts(i);
            if (certobj.InValidity && (certobj.KeyUsage & (SECUINTER_KEYUSAGE_DIGITALSIGNATURE | SECUINTER_KEYUSAGE_NONREPUDIATION)) != 0) {
                certsobj.Add(certobj);
            }
        }

        certobj = null;
        certobj = certsobj.Select();

        if (certobj != null) {
            var strHex = certobj.SerialNumber;


            //document.getElementById('HSerialNumber').value = strHex;

            //alert(strHex);

            var strpassword = certobj.SerialNumber;

            //获取证书的名称
            var strSubject = certobj.Subject;

            var strs = new Array();

            strs = strSubject.split(',');

            var strCN = "";

            for (var p = 0; p < strs.length; p++) {
                var strp = strs[p].split('=');

                if (strp[0].indexOf('O') > 0) {
                    strCN = strp[1];
                    break;
                }
            }
            $("#TextBox_UserName").val("     " + strCN);
            $("#ukeylabel").val("     已正确插入Ukey,可以登录...");
        }
        else {
            $("#ukeylabel").val("     请插入Ukey...");
            $("#TextBox_UserName").val("     未能读取");
        }

        if (strHex != null && strHex != "")
            isarikey = 1; //$("#btnlogin").show(); //  mark = strHex;
        else
            isarikey = 0; //$("#btnlogin").hide(); // mark = "";

        setTimeout("GetInfo()", 5000);
    }

    GetInfo();

    function lg() {
        $.post("Ajax/Lg.aspx", { "type": "lg", "n1": mark }, function (data) {
            if (mark == "") {
                $("#ukeylabel").html("请插入Ukey...");
                setTimeout("GetInfo()", 2000);
                return false;
            }
            else if (data == "0")
                location.href = "http://" + url + "/index_in.aspx";
            else if (data == "1" || data == "-1")
                location.href = "http://" + url + "/index_out.aspx";
            else if (data == "-500") {
                $("#ukeylabel").html("此用户暂时无法登陆!"); //数据库无对应数据
                $("#aback").attr("href", "http://" + url + "/login.aspx")
                setTimeout("GetInfo()", 2000);
                return false;
            }
            else if (data == "-999") {
                $("#ukeylabel").html("此用户不允许登录!");
                setTimeout("GetInfo()", 2000);
                return false;
            }
            else if (data == "-5") {
                $("#ukeylabel").html("服务器异常!");
                setTimeout("GetInfo()", 2000);
                return false;
            }
            else {
                $("#ukeylabel").html(data);
                setTimeout("GetInfo()", 2000);
                return false;
            }
        });
    }