﻿				//数据容器变量
			    var data = [];
			    var userid = "1000";
                //dom函数
			    $(function () {

			        //默认加载
			        loaddata("area", "工程区县");


			        //数据统计方法提取
			        $("input[name='btnsearch']").click(function () {
			            loaddata($(this).attr("id"), $(this).val());
			        });

			        //数据查询
			        function loaddata(id, name) {
			            $("#imgprogress").show();
			            var data1 = [], data2 = [];
			            $("#tbodylist").html("");

			            $.post("Ajax/DataAnalyse.aspx", { "type": id, "userid": userid }, function (obj) {
			                if (obj == "no") { setInterval(" $('#imgprogress').hide();", 1500); return; }
			                if (obj == "-999") { alert("登录失效.请重新登录!"); return false; }
			                obj = $.parseJSON(obj);
			                for (var i = 0; i < obj.length; i++) {
			                    data1.push(obj[i].LAB);
			                    data2.push(parseFloat(obj[i].DAT));

			                    if (utypeu == 1 || utypeu == "1")
			                        $("#tbodylist").append("<tr><td><a href='BasementInformationListPage.aspx?qvalue=" + obj[i].LAB + "&&qtype=" + id + "'>" + obj[i].LAB + "</a></td><td class='center'>" + obj[i].DAT + "</td></tr>");
			                    else
			                        $("#tbodylist").append("<tr><td><a href='BasementInformationListPage2.aspx?qvalue=" + obj[i].LAB + "&&qtype=" + id + "'>" + obj[i].LAB + "</a></td><td class='center'>" + obj[i].DAT + "</td></tr>");
			                }

			                if (id == "proused")
			                    $("#tablistlink").hide();
			                else
			                    $("#tablistlink").show();

			                BinDataToView(data1, data2, name);
							$(".highcharts-legend").hide();
			            });
			        }

			        //数据显示
			        function BinDataToView(data1, data2, name) {
			            setInterval(" $('#imgprogress').hide();", 1500);
						$("#thheadtitle").html(name);
			            $('#container').highcharts({
			                chart: {
			                    type: 'column'
			                },
			                title: {
			                    text: ''
			                },
			                subtitle: {
			                    text: ''
			                },
			                xAxis: {
			                    categories: data1
			                },
			                yAxis: {
			                    min: 0,
			                    title: {
			                        text: 'Rainfall (mm)'
			                    }
			                },
			                tooltip: {
			                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                 '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
			                    footerFormat: '</table>',
			                    shared: true,
			                    useHTML: true
			                },
			                plotOptions: {
			                    column: {
			                        pointPadding: 0.2,
			                        borderWidth: 0
			                    }
			                },
			                series: [{
			                    name: name,
			                    data: data2
			                }]
			            });
			        }
			    });