﻿                //数据容器变量
			    var data = [];
			    var userid = "1000";

			    $(function () {

			        //数据统计方法提取
			        $("input[name='btnsearch']").click(function () {
			            $("#imgprogress").show();
			            data = [];
			            var id = $(this).attr("id");
			            var namea = $(this).val();
			            $("#tbodylist").html("");

			            $.post("Ajax/DataAnalyse.aspx", { "type": id, "userid": userid }, function (obj) {
			                if (obj == "-999") { alert("登录失效.请重新登录!"); return false; }
			                if (obj == "no") { setInterval(" $('#imgprogress').hide();", 1500); return; }
			                obj = $.parseJSON(obj);
			                for (var i = 0; i < obj.length; i++) {
			                    var one = [];
			                    one[0] = obj[i].LAB;
			                    one[1] = parseInt(obj[i].DAT);
			                    data.push(one);

			                    if (utypeu == 1 || utypeu == "1")
			                        $("#tbodylist").append("<tr><td><a href='BasementInformationListPage.aspx?qvalue=" + obj[i].LAB + "&&qtype=" + id + "'>" + obj[i].LAB + "</a></td><td class='center'>" + obj[i].DAT + "</td></tr>");
			                    else
			                        $("#tbodylist").append("<tr><td><a href='BasementInformationListPage2.aspx?qvalue=" + obj[i].LAB + "&&qtype=" + id + "'>" + obj[i].LAB + "</a></td><td class='center'>" + obj[i].DAT + "</td></tr>");

			                    one = null;
			                }

			                if (id == "proused")
			                    $("#tablistlink").hide();
			                else
			                    $("#tablistlink").show();

			                docReady(namea);
			            });
			        });

			        function loaddata() {

			            $("#imgprogress").show();
			            $("#tbodylist").html("");

			            $.post("Ajax/DataAnalyse.aspx", { "type": "area", "userid": userid }, function (obj) {
			                if (obj == "-999") { alert("登录失效.请重新登录!"); return false; }
			                if (obj == "no") { setInterval(" $('#imgprogress').hide();", 1500); return; }
			                obj = $.parseJSON(obj);
			                for (var i = 0; i < obj.length; i++) {
			                    var one = [];
			                    one[0] = obj[i].LAB;
			                    one[1] = parseInt(obj[i].DAT);
			                    data.push(one);

			                    if (utypeu == 1 || utypeu == "1")
			                        $("#tbodylist").append("<tr><td><a href='BasementInformationListPage.aspx?qvalue=" + obj[i].LAB + "&&qtype=" + "area" + "'>" + obj[i].LAB + "</a></td><td class='center'>" + obj[i].DAT + "</td></tr>");
			                    else
			                        $("#tbodylist").append("<tr><td><a href='BasementInformationListPage2.aspx?qvalue=" + obj[i].LAB + "&&qtype=" + "area" + "'>" + obj[i].LAB + "</a></td><td class='center'>" + obj[i].DAT + "</td></tr>");

			                    one = null;
			                }
			                docReady("工程区县");
			            });
			        }

			        //默认加载
			        loaddata();

			        function docReady(namea) {
			            setInterval(" $('#imgprogress').hide();", 1500);
						$("#thheadtitle").html(namea);
						
			            $('#container').highcharts({
			                chart: {
			                    plotBackgroundColor: null,
			                    plotBorderWidth: null,
			                    plotShadow: false
			                },
			                title: {
			                    text: ''
			                },
			                tooltip: {
			                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			                },
			                plotOptions: {
			                    pie: {
			                        allowPointSelect: true,
			                        cursor: 'pointer',
			                        dataLabels: {
			                            enabled: true,
			                            color: '#000000',
			                            connectorColor: '#000000',
			                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
			                        }
			                    }
			                },
			                series: [{
			                    type: 'pie',
			                    name: namea + ":",
			                    data: data
			                }]
			            });
			        }
			    });     //dom end