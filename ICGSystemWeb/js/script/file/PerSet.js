﻿        var userid = 1000;

        var startpageindex = 1, step = 7; //分页容量大小
        $(function () {

            //页面加载执行
            jQuery("#DivEdit").validationEngine(); //验证

            //点击修改
            $("#laupdate").click(function () {


                if (!jQuery('#DivEdit').validationEngine('validate')) {
                    return;
                }

                var arraycol = [];
                arraycol.push("MYUSERNAME");
                arraycol.push("USERTRUENAME");
                arraycol.push("SEX");
                arraycol.push("USERADDRESS");
                arraycol.push("USERMOBLEPHONE");
                arraycol.push("USERPHONE");
                arraycol.push("FAX");
                arraycol.push("USERIDCARDNUM");
                arraycol.push("USEREMAIL");
                arraycol.push("USERCOLLEGE");
                arraycol.push("USERPOSTCODE");
                arraycol.push("USERPOST");
                arraycol.push("USERBIRTHDAY");
                arraycol.push("COMPANYTEL");
                for (var i = 0; i < arraycol.length; i++)
                    arraycol[i] = arraycol[i] + ":" + $("#" + arraycol[i]).val();

                $.post("Ajax/UserMag.aspx", { "type": "upd", "beid": userid, "val": arraycol.toString() }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "1") {
                        load();
                        alert("修改成功!");
                    }
                })
            });

            load();

            function load() {
                //获取用户id
                $.post("Ajax/UserMag.aspx", { "type": "getuid" }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    userid = data;

                    $.post("Ajax/UserMag.aspx", { "type": "view", "userid": userid, "viewuid": userid, "val": userid }, function (data) {
                        if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                        if (data == "no") return;
                        var arraycol = [];
                        arraycol.push("MYUSERNAME");
                        arraycol.push("ISALLOWEDLOGIN");
                        arraycol.push("LASTLOGINTIME");
                        arraycol.push("USERTRUENAME");
                        arraycol.push("USERTYPE");
                        arraycol.push("LASTLOGINIPADDS");
                        arraycol.push("SEX");
                        arraycol.push("USERADDRESS");
                        arraycol.push("USERMOBLEPHONE");
                        arraycol.push("USERPHONE");
                        arraycol.push("FAX");
                        arraycol.push("USERIDCARDNUM");
                        arraycol.push("USEREMAIL");
                        arraycol.push("USERCOLLEGE");
                        arraycol.push("USERPOSTCODE");
                        arraycol.push("USERPOST");
                        arraycol.push("USERBIRTHDAY");
                        arraycol.push("COMPANYTEL");
                        arraycol.push("CERTIDTYPE");

                        var obj = $.parseJSON(data);


                        for (var i = 0; i < arraycol.length; i++) {
                            if (arraycol[i] == "ISALLOWEDLOGIN")
                                $("#L" + arraycol[i]).html("<span style='font-size:20px;'>" + (obj[0][arraycol[i]] == "1" ? "允许登录" : "不可登录") + "</span>");
                            else if (arraycol[i] == "USERTYPE")
                                $("#L" + arraycol[i]).html("<span style='font-size:20px;'>" + (obj[0][arraycol[i]] == "0" ? "内网" : "外网") + "</span>");
                            else
                                $("#L" + arraycol[i]).html("<span style='font-size:20px;'>" + obj[0][arraycol[i]].replace(" 0:00:00", "") + "</span>"); //查看
                        }
                    });

                })
            }

            $("#btnedit").click(function () {
                edit(userid);
            });
        })

        //edit
        function edit(id) {
            $("#laupdate").attr("myupdateid", id);
            $("#DivEdit").modal("show");
            $.post("Ajax/UserMag.aspx", { "type": "view", "userid": userid, "viewuid": userid, "val": userid }, function (data) {
                if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                if (data == "no") return;
                var arraycol = [];
                arraycol.push("MYUSERNAME");
                arraycol.push("USERTRUENAME");
                arraycol.push("SEX");
                arraycol.push("USERADDRESS");
                arraycol.push("USERMOBLEPHONE");
                arraycol.push("USERPHONE");
                arraycol.push("FAX");
                arraycol.push("USERIDCARDNUM");
                arraycol.push("USEREMAIL");
                arraycol.push("USERCOLLEGE");
                arraycol.push("USERPOSTCODE");
                arraycol.push("USERPOST");
                arraycol.push("USERBIRTHDAY");
                arraycol.push("COMPANYTEL");

                var obj = $.parseJSON(data);

                for (var i = 0; i < arraycol.length; i++)
                    $("#" + arraycol[i]).val(obj[0][arraycol[i]].replace(" 0:00:00", "")); //绑定修改

                $("#SEX").val($("#SEX").val().substr(0, 1)); //去掉空格
            })
        }
        //dom end