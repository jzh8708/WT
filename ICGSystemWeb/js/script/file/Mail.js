﻿		var userid = 1002; //test demo using value
        var filepath = []; //路径
        var filename = []; //名称
        var filesize = []; //大小
        var ckidlist = []; //判断是否存在
        var pagesizeindex = 1, persize = 10;


        $(function () {
            $("#WNewEmail").hide(); //新邮件框隐藏
            $("#divsysmsg").hide(); //提示框
            $("myModalA").hide(); //选择人list
            $("#DivViewDT").hide(); //邮件详情

            jQuery("#divall").validationEngine(); //验证
            jQuery("#divpeoplelistitle").validationEngine(); //验证

            //写新邮件
            $("#newemialdiv").click(function () {
                $("#WNewEmail").show();
                $("#divshow").hide(); //关闭列表
                $("#DivViewDT").hide();

                $("#inputSendobj").val(""); //选人id
                $("#inputSendobjName").val(""); //选人name
                $("#date01").val(""); //标题
                $("#textarea2").val("")//内容
                $("#lablefile").html(""); //显示附件名称


                filename = [];
                filepath = [];
                filesize = [];

                $("#FilelistTbody").html(""); //clear files
                $("#MailFileList2").hide();


                $("body[style='margin:4px; font:10pt Arial,Verdana; cursor:text']").html("<br _moz_editor_bogus_node='TRUE'></br>");

            });
            //取消写新邮件
            $("#btnclosedffsdfla").click(function () {
                $("#WNewEmail").hide();
                $("#divmaillist").show();
            });
            //关闭写新邮件
            $("#btncanceldiv").click(function () {
                $("#WNewEmail").hide();
                $("#divmaillist").show();
            });
            //关闭列表
            $("#close").click(function () {
                $("#divshow").hide();
            });
            //modal show   选择发送人员
            $("#inputSendobjName").click(function () {
                $("#myModalA").modal("show");
                scname = "scname";
                GetOrgName($("#searchpeopletxt").val());
            });
            //文本框查找机构
            $("#searchpeopletxt").keyup(function () {
                GetOrgName($("#searchpeopletxt").val());
            });
            //关闭详细 显示列表
            $("#A4close").click(function () {
                $("#DivViewDT").hide();
                $("#divmaillist").show();
            });
            //check 选择人员提交
            $("#lasure").click(function () {

                var inputSendobjName = "";
                ckidlist = [];

                $("#inputSendobj").val("");
                var ob = $("input[name='checkboxA']");
                $(ob).each(function () {
                    if ($(this).attr("checked") == "checked") {
                        $("#inputSendobj").val($("#inputSendobj").val() + $(this).attr("id") + ",");

                        ckidlist.push($(this).attr("id"));

                        inputSendobjName = inputSendobjName + $(this).attr("myname") + ",";
                    }
                });
                if ($("#inputSendobj").val() != "") {
                    $("#inputSendobj").val($("#inputSendobj").val().substr(0, $("#inputSendobj").val().length - 1));
                    inputSendobjName = inputSendobjName.substr(0, inputSendobjName.length - 1);
                }
                $("#inputSendobjName").val(inputSendobjName);
                $("#Checkboxall").attr("checked", false);
            });
            //全选
            $("#Checkboxall").click(function () {
                if ($("#Checkboxall").attr("checked") == "checked") {
                    var objall = $("input[name = 'checkboxA']");
                    $(objall).each(function () {
                        $(this).attr("checked", true);
                    })
                    return;
                }
                else {
                    var objall = $("input[name = 'checkboxA']");
                    $(objall).each(function () {
                        $(this).attr("checked", false);
                    })
                    return;
                }
            });
            //反选
            $("#Checkboxback").click(function () {
                if ($("#Checkboxback").attr("checked") == "checked") {
                    var objall = $("input[name = 'checkboxA']");
                    $(objall).each(function () {
                        if ($(this).attr("checked") == "checked")
                            $(this).attr("checked", false);
                    })
                    return;
                }
                else {
                    var objall = $("input[name = 'checkboxA']");
                    $(objall).each(function () {
                        if ($(this).attr("checked") == false)
                            $(this).attr("checked", true);
                    })
                    return;
                }
            });
            //提交新邮件
            $("#btnsavedata").click(function () {

                if (!jQuery('#divall').validationEngine('validate')) {
                    return;
                }

                if ($("#inputSendobj").val() == "") { alert("请选择收件人"); return false; }

                var para = {
                    "topeople": $("#inputSendobj").val(),
                    "title": $("#date01").val(),
                    "content": encodeURIComponent($("#textarea2").val()),
                    "filepath": filepath.toString(),
                    "filename": filename.toString(),
                    "filesize": filesize.toString(),
                    "type": "addmsg",
                    "ttime": $("#txtsendtime").val() + " " + $("#txthour").val() + ":" + $("#txtmini").val() + ":00",
                    "userid": userid
                };

                $.post("userajax/Mail.aspx", para, function (data) {
                    $("#inputSendobj").val(""); //选人
                    $("#date01").val(""); //标题
                    $("#textarea2").val("")//内容
                    $("#lablefile").html(""); //显示附件名称

                    filename = [];
                    filepath = [];
                    filesize = [];

                    pagesizeindex = 1;
                    GetSendBox();
                })

            });
            $("#btnmore").click(function () {
                if (gettype == 0)
                    GetResBox();
                else if (gettype == 1)
                    GetSendBox();
                else if (gettype == 2)
                    GetRebBox();
            })
            var gettype = 0; //0收件箱/1发件箱/2回收站
            var pagesizeindex = 1;
            var persize = 20; //const

            //发
            $("#A1").click(function () {
                pagesizeindex = 1;
                GetSendBox();
            });
            //收
            $("#A2").click(function () {
                pagesizeindex = 1;
                GetResBox();
            });
            //回
            $("#A3").click(function () {
                pagesizeindex = 1;
                GetRebBox();
            });


            //发
            function GetSendBox() {
                $("#thlable009").html("收件人");
                gettype = 1;
                $("#DivViewDT").hide();
                $("#divmaillist").show();

                if (pagesizeindex == 1)
                    $("#datalist").html("");

                $("#WNewEmail").hide();
                $("#divshow").show();
                $("#hlistname").html("<i class='icon-user'></i>发件箱");

                $.post("userajax/Mail.aspx", { "type": "getmysenddata", "userid": userid, "searchtype": "1", "pagesizeindex": pagesizeindex }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") { $("#datainfoa").html("无数据!"); $("#btnmore").hide(); return; }
                    $("#datainfoa").html("");

                    var objresult = $.parseJSON(data);
                    if (objresult == null) return;

                    pagesizeindex = parseInt(pagesizeindex + objresult.length);

                    if (objresult.length < persize)
                        $("#btnmore").hide();
                    else
                        $("#btnmore").show();


                    for (var i = 0; i < objresult.length; i++) {
                        $("#datalist").append("<tr id='TRList" + objresult[i].ID + "," + objresult[i].FID + "'><td>" + objresult[i].RESERVEDNAME + "</td><td class='center'>" + objresult[i].SENDTIME + "</td><td class='center'>" + objresult[i].TITLE + "</td><td class='center'><span class='label label-important'>" + "已发送" + "</span></td><td>" + (objresult[i].ISATTACHFILE == '1' ? "<span class='icon32 icon-color icon-link' title='.icon32  .icon-color  .icon-link ' onclick=maildo('file','" + objresult[i].ID + "," + objresult[i].FID + "')></span>" : " ") + "</td><td class='center'><table border='0'><tr border='0'><td><a class='btn btn-success'    onclick=maildo('view','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-zoom-in icon-white'></i> 查看</a></td><td><a class='btn btn-info'    onclick=maildo('forw','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-edit icon-white'></i> 转发</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteoutbox','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-trash icon-white'></i>   删除</a></td></tr></table></td></tr>");
                    }

                })
            }

            //收
            function GetResBox() {
                $("#thlable009").html("发件人");
                gettype = 0;
                $("#DivViewDT").hide();
                $("#divmaillist").show();

                if (pagesizeindex == 1)
                    $("#datalist").html("");

                $("#WNewEmail").hide();
                $("#divshow").show();
                $("#hlistname").html("<i class='icon-user'></i>收件箱");

                $.post("userajax/Mail.aspx", { "type": "getmysenddata", "userid": userid, "searchtype": "0", "pagesizeindex": pagesizeindex }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") { $("#datainfoa").html("无数据!"); $("#btnmore").hide(); return; }
                    $("#datainfoa").html("");
                    var objresult = $.parseJSON(data);
                    if (objresult == null) return;

                    pagesizeindex = parseInt(pagesizeindex + objresult.length);

                    if (objresult.length < persize)
                        $("#btnmore").hide();
                    else
                        $("#btnmore").show();

                    for (var i = 0; i < objresult.length; i++) {
                        $("#datalist").append("<tr id='TRList" + objresult[i].ID + "," + objresult[i].FID + "'><td>" + objresult[i].SENDUSERNAME + "</td><td class='center'>" + objresult[i].SENDTIME + "</td><td class='center'>" + objresult[i].TITLE + "</td><td class='center'>" + (objresult[i].STATUE == "0" || objresult[i].STATUE == 0 ? "<span class='label label-important'>" : "<span class='label'>") + "" + (objresult[i].STATUE == "0" || objresult[i].STATUE == 0 ? "新邮件" : "已读") + "</span></td><td>" + (objresult[i].ISATTACHFILE == '1' ? "<span class='icon32 icon-color icon-link' title='.icon32  .icon-color  .icon-link ' onclick=maildo('file','" + objresult[i].ID + "," + objresult[i].FID + "')></span>" : " ") + "</td><td class='center'><table border='0'><tr border='0'><td><a class='btn btn-success'    onclick=maildo('view','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-zoom-in icon-white'></i> 查看</a></td><td><a class='btn btn-info'    onclick=maildo('forw','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-edit icon-white'></i> 转发</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteinbox','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-trash icon-white'></i>   删除</a></td></tr></table></td></tr>");
                    }
                })
            }

            //回
            function GetRebBox() {
                $("#thlable009").html("收件人");
                gettype = 2;
                $("#DivViewDT").hide();
                $("#divmaillist").show();

                if (pagesizeindex == 1)
                    $("#datalist").html("");

                $("#WNewEmail").hide();
                $("#divshow").show();
                $("#hlistname").html("<i class='icon-user'></i>回收站");

                $.post("userajax/Mail.aspx", { "type": "getmysenddata", "userid": userid, "searchtype": "2", "pagesizeindex": pagesizeindex }, function (data) {

                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }

                    if (data == "no") { $("#datainfoa").html("无数据!"); $("#btnmore").hide(); return; }
                    $("#datainfoa").html("");
                    var objresult = $.parseJSON(data);
                    if (objresult == null) return;


                    pagesizeindex = parseInt(pagesizeindex + objresult.length);

                    if (objresult.length < persize)
                        $("#btnmore").hide();
                    else
                        $("#btnmore").show();

                    for (var i = 0; i < objresult.length; i++) {
                        $("#datalist").append("<tr id='TRList" + objresult[i].ID + "," + objresult[i].FID + "'><td>" + objresult[i].RESERVEDNAME + "</td><td class='center'>" + objresult[i].SENDTIME + "</td><td class='center'>" + objresult[i].TITLE + "</td><td class='center'><span class='label'>已进回收站</span></td><td>" + (objresult[i].ISATTACHFILE == '1' ? "<span class='icon32 icon-color icon-link' title='.icon32  .icon-color  .icon-link ' onclick=maildo('filedelete','" + objresult[i].ID + "," + objresult[i].FID + "')></span>" : " ") + "</td><td class='center'><table border='0'><tr border='0'><td><a class='btn btn-success'    onclick=maildo('backview','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-zoom-in icon-white'></i> 还原</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteon','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-trash icon-white'></i>   彻底删除</a></td></tr></table></td></tr>");
                    }
                })
            }


            load();

            //页面加载 默认显示收件箱
            function load() {
                GetResBox();
            }

            //删除附件(多个)
            $("#spdelete").click(function () {
                if (confirm("确定删除吗?") == false) return;

                if (ismyupload == 0) { filename = []; filepath = []; filesize = []; $("#spdelete").hide(); $("#lablefile").html(""); return; }
                $.post("userajax/Mail.aspx", { "type": "deletefile", "pathlist": filepath.toString() }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "1")
                        $("#spdelete").hide();
                    $("#lablefile").html("");
                })
            })

            // 文件上传 ------------------------- begin(华丽的分隔符) ---------------------------
            var button = $('#adlaimgFile'), interval;
            new AjaxUpload(button, {
                action: 'userajax/FileMail.ashx',
                name: 'imgFile',
                onSubmit: function (file, ext) {
                    this.disable();
                    interval = window.setInterval(function () {
                        var text = button.text();
                    }, 200);
                },
                onComplete: function (file, response) {
                    window.clearInterval(interval);
                    this.enable();
                    response = response.replace(/<pre>/ig, "").replace(/<\/pre>/ig, ""); //过滤
                    var obj = $.parseJSON(response);
                    if (obj[0].filetruename == "ferror") { alert("该文件类型不允许上传!"); return false; }
                    if (obj[0].filetruename == "big") { alert("文件过大!"); return false; }
                    filename.push(obj[0].filetruename);
                    filepath.push(obj[0].filepath);
                    filesize.push(obj[0].filesize);

                    LoadJSFile();
                }
            });

            // window.onload = load(); //默认加载收件箱数据
        })
        //dom end

        //标识是否为自己上传变量
        var ismyupload = 1;

        //加载列表
        function LoadJSFile() {
            $("#FilelistTbody").html("");

            if (filename.length > 0) {
                $("#MailFileList2").show();
            }
            else {
                $("#MailFileList2").hide();
                return;
            }
            for (var i = 0; i < filename.length; i++)
                $("#FilelistTbody").append("<tr id='trf" + i + "'><td>" + filename[i] + "</td><td class='center'>" + filesize[i] + "k </td><td class='center'>" + filename[i].substr(filename[i].lastIndexOf(".") + 1) + "</td><td class='center'><span class='icon icon-color icon-close'  onclick=deleteBypath('" + encodeURIComponent(filepath[i]) + "','" + i + "')> </span></td></tr>");
        }


        //删除单个附件
        function deleteBypath(id, i) {
		   if(confirm("确定删除吗?")==false)return;
		
            //不能删除邮件转发的附件
            if (ismyupload == 0) {
                filename.pop(i);
                filepath.pop(i);
                filesize.pop(i);
                $("#trf" + i).remove();
                if (filename.length < 1)
                    $("#MailFileList2").hide();
                else
                    $("#MailFileList2").show();
                return;
            }
            //物理删除
            $.post("userajax/Mail.aspx", { "type": "deletefileone", "pathlist": id }, function (data) {
                if(data=="-999"){alert("登录失效.请重新登录!");return false;}
				if (data == "1")
                    $("#trf" + i).remove();
                filename.pop(i);
                filepath.pop(i);
                filesize.pop(i);
                if (filename.length < 1)
                    $("#MailFileList2").hide();
                else
                    $("#MailFileList2").show();
            })
        }

        //function for button
        function maildo(type, id) {
            //收件箱删除 伪删除
            if (type == "deleteinbox") {
                if (confirm("确定删除吗?") == false) return;
                $.post("userajax/Mail.aspx", { "type": "deleteinbox", "userid": userid, "idlist": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    $("#TRList" + id).remove();
                    MSG();
                    GetResBox();
                });
            }
            //发件箱删除 伪删除
            else if (type == "deleteoutbox") {
                if (confirm("确定删除吗?") == false) return;
                $.post("userajax/Mail.aspx", { "type": "deleteoutbox", "userid": userid, "idlist": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    $("#TRList" + id).remove();
                    MSG();
                    GetSendBox();
                });
            }
            //回收站返回
            else if (type == "backview") {
            $.post("userajax/Mail.aspx", { "type": "backview", "userid": userid, "idlist": id }, function (data) {
                if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                $("#TRList" + id).remove();
                MSG();
                GetRebBox();
            });
            }
            //回收站彻底删除
            else if (type == "deleteon") {
                if (confirm("确定删除吗?") == false) return;
                $.post("userajax/Mail.aspx", { "type": "deleteon", "userid": userid, "idlist": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    $("#TRList" + id).remove();
                    MSG();
                    GetRebBox();
                });
            }
            //查看邮件
            else if (type == "view") {
                $("#DivViewDT").show();
                $.post("userajax/Mail.aspx", { "type": "view", "userid": userid, "idlist": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }

                   // if ($("#TRList" + id).html().toString().indexOf('新邮件') > -1)
                       // $("#TRList" + id).html($("#TRList" + id).html().replace("label label-important", "label").replace("新邮件", "已读"));

                    $("#divmaillist").hide();
                    $("#DivViewDT").show();

                    var obj = $.parseJSON(data);

                    $("#ViewTitle").html("邮件主题:" + obj[0].TITLE);
                    $("#sendlable").html("发送人:" + obj[0].SENDUSERNAME);
                    $("#reserveduname").html("接收人:" + obj[0].RESERVEDNAME);
                    $("#sendtime").html("发送时间:" + obj[0].SENDTIME);

                    $("#contentlable").html(decodeURIComponent(obj[0].CONTENT));
                    MSG();
                });
                //附件
                $("#Tbodyfilelistview").html("");
                $.post("userajax/Mail.aspx", { "type": "file", "id": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") { $("#Afileresult").html("无附件!"); $("#tablefilelist").hide(); return; }
                    $("#Afileresult").html("");
                    $("#tablefilelist").show();
                    var obj = $.parseJSON(data);
                    for (var i = 0; i < obj.length; i++) {
                        $("#Tbodyfilelistview").append("<tr><td><a target='_blank' href='Ajax/" + obj[i].FILEPATH + "'>" + obj[i].FILENAME + "</a></td><td class='center'>" + obj[i].FILESIZE + "k </td><td class='center'>" + obj[i].FILETYPE + "</td><td class='center'><span class='label label-success'> <i class='icon-download' onclick=downloadfile('" + obj[i].FILEPATH + "')></i>  </span></td></tr>");
                    }
                })
            }
            //转发
            else if (type == "forw") {
                $("#divmaillist").hide();
                $("#WNewEmail").show();

                $("#inputSendobj").val("");
                ismyupload = 0; //控制不能删除别人附件
                filename = []; filepath = []; filesize = [];

                //转发带附件的邮件
                $.post("userajax/Mail.aspx", { "type": "file", "id": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") { $("#lablefile").html(""); return; }
                    $("#lablefile").html("");
                    var obj = $.parseJSON(data);
                    for (var i = 0; i < obj.length; i++) {
                        filename.push(obj[i].FILENAME);
                        filepath.push(obj[i].FILEPATH);
                        filesize.push(obj[i].FILESIZE);
                    }
                    LoadJSFile();
                })

                $.post("userajax/Mail.aspx", { "type": "view", "userid": userid, "idlist": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }


                    // if ($("#TRList" + id).html().toString().indexOf('新邮件') > -1)
                    // $("#TRList" + id).html($("#TRList" + id).html().replace("label label-important", "label").replace("新邮件", "已读"));

                    var obj = $.parseJSON(data);

                    $("#date01").val(obj[0].TITLE);

                    $("#textarea2").val(decodeURIComponent(obj[0].CONTENT));

                    MSG();
                })
            }
            //附件
            else if (type == "filedelete" || type == "file") {
                if (type == "filedelete") { alert("回收站中无法查看附件！"); return; }
                $("#DivFiles").modal("show");
                $("#FileListdata").html("");
                $.post("userajax/Mail.aspx", { "type": "file", "id": id }, function (data) {
                    if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (data == "no") { $("#fileinfoa").html("无数据!"); return; }
                    $("#fileinfoa").html("");
                    var obj = $.parseJSON(data);
                    for (var i = 0; i < obj.length; i++) {
                        $("#FileListdata").append("<tr><td><a target='_blank' href='Ajax/" + obj[i].FILEPATH + "'>" + obj[i].FILENAME + "</a></td><td class='center'>" + obj[i].FILESIZE + "k </td><td class='center'>" + obj[i].FILETYPE + "</td><td class='center'><span class='label label-success'> <i class='icon-download' onclick=downloadfile('" + obj[i].FILEPATH + "')></i>  </span></td></tr>");
                    }
                })
            }
        }
        
        //下载
        function downloadfile(fpath) {
            $.post("Ajax/Floor.aspx", { "type": "existsfile", "filep": fpath }, function (data) {
                if (data == "noyes") {
                    alert("文件不存在!");
                    return;
                }
                else
                    window.open("Ajax/" + fpath);
                //location.href = "Ajax/" + fpath;
            });
        }

        //消息提示框
        function MSG() {
            $("#divsysmsg").show();
            setInterval("$('#divsysmsg').hide();", 2000);
        }
        //获取名称
		var scname="scname";
        function GetOrgName(name) {
		
			if(scname==name){return;}
			scname=name;
			
            if (name == "'" || name == "," || name == "!" || name == '"') { $("#acount").html("非法字符!"); return; } //过滤字符
            $("#divchecklist").html("");
            $("#imgprogress").show();
            $("#acount").html("");

            $.ajax({
                url: "userajax/Mail.aspx",
                data: { "type": "getorgname", "searchval": name },
                type: "POST",
                beforsend: function () {

                },
                success: function (a) {
                    setInterval("$('#imgprogress').hide();", 1500);
                    if (a == "-999") { alert("登录失效.请重新登录!"); return false; }
                    if (a == "no") { $("#acount").html("0条结果"); return false; } //无数据
                    var obj = $.parseJSON(a);
                    $("#acount").html(obj.length + "条结果"); //get counts number
                    for (var i = 0; i < obj.length; i++) {

                        if (exists(obj[i].USERID) == true)
                            $("#divchecklist").append("<p><input  checked='checked'  type='checkbox' myname='" + obj[i].MYUSERNAME + "' name='checkboxA' id='" + obj[i].USERID + "' />" + obj[i].MYUSERNAME + "</p>");
                        else
                            $("#divchecklist").append("<p><input type='checkbox'  myname='" + obj[i].MYUSERNAME + "' name='checkboxA' id='" + obj[i].USERID + "' />" + obj[i].MYUSERNAME + "</p>");
                    }
                }
            })
        }
        
        //判断是否选中该数据
        function exists(id) {
            for (var i = 0; i < ckidlist.length; i++) {
                if (ckidlist[i] == id)
                    return true;
            }
            return false;
        }

        //发
        function GetSendBox() {
            $("#thlable009").html("收件人");
            gettype = 1;
            $("#DivViewDT").hide();
            $("#divmaillist").show();

            $("#datalist").html("");

            $("#WNewEmail").hide();
            $("#divshow").show();
            $("#hlistname").html("<i class='icon-user'></i>发件箱");

            $.post("userajax/Mail.aspx", { "type": "getmysenddata", "userid": userid, "searchtype": "1", "pagesizeindex": pagesizeindex }, function (data) {
                if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                if (data == "no") { $("#datainfoa").html("无数据!"); $("#btnmore").hide(); return; }
                $("#datainfoa").html("");

                var objresult = $.parseJSON(data);
                if (objresult == null) return;

                pagesizeindex = parseInt(pagesizeindex + objresult.length);

                if (objresult.length < persize)
                    $("#btnmore").hide();
                else
                    $("#btnmore").show();


                for (var i = 0; i < objresult.length; i++) {
                    $("#datalist").append("<tr id='TRList" + objresult[i].ID + "," + objresult[i].FID + "'><td>" + objresult[i].RESERVEDNAME + "</td><td class='center'>" + objresult[i].SENDTIME + "</td><td class='center'>" + objresult[i].TITLE + "</td><td class='center'><span class='label label-important'>" + "已发送" + "</span></td><td>" + (objresult[i].ISATTACHFILE == '1' ? "<span class='icon32 icon-color icon-link' title='.icon32  .icon-color  .icon-link ' onclick=maildo('file','" + objresult[i].ID + "," + objresult[i].FID + "')></span>" : " ") + "</td><td class='center'><table border='0'><tr border='0'><td><a class='btn btn-success'    onclick=maildo('view','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-zoom-in icon-white'></i> 查看</a></td><td><a class='btn btn-info'    onclick=maildo('forw','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-edit icon-white'></i> 转发</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteoutbox','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-trash icon-white'></i>   删除</a></td></tr></table></td></tr>");
                }

            })
        }

        //收
        function GetResBox() {
            $("#thlable009").html("发件人");
            gettype = 0;
            $("#DivViewDT").hide();
            $("#divmaillist").show();


            $("#datalist").html("");

            $("#WNewEmail").hide();
            $("#divshow").show();
            $("#hlistname").html("<i class='icon-user'></i>收件箱");

            $.post("userajax/Mail.aspx", { "type": "getmysenddata", "userid": userid, "searchtype": "0", "pagesizeindex": pagesizeindex }, function (data) {
                if (data == "-999") { alert("登录失效.请重新登录!"); return false; }
                if (data == "no") { $("#datainfoa").html("无数据!"); $("#btnmore").hide(); return; }
                $("#datainfoa").html("");
                var objresult = $.parseJSON(data);
                if (objresult == null) return;

                pagesizeindex = parseInt(pagesizeindex + objresult.length);

                if (objresult.length < persize)
                    $("#btnmore").hide();
                else
                    $("#btnmore").show();

                for (var i = 0; i < objresult.length; i++) {
                    $("#datalist").append("<tr id='TRList" + objresult[i].ID + "," + objresult[i].FID + "'><td>" + objresult[i].SENDUSERNAME + "</td><td class='center'>" + objresult[i].SENDTIME + "</td><td class='center'>" + objresult[i].TITLE + "</td><td class='center'>" + (objresult[i].STATUE == "0" || objresult[i].STATUE == 0 ? "<span class='label label-important'>" : "<span class='label'>") + "" + (objresult[i].STATUE == "0" || objresult[i].STATUE == 0 ? "新邮件" : "已读") + "</span></td><td>" + (objresult[i].ISATTACHFILE == '1' ? "<span class='icon32 icon-color icon-link' title='.icon32  .icon-color  .icon-link ' onclick=maildo('file','" + objresult[i].ID + "," + objresult[i].FID + "')></span>" : " ") + "</td><td class='center'><table border='0'><tr border='0'><td><a class='btn btn-success'    onclick=maildo('view','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-zoom-in icon-white'></i> 查看</a></td><td><a class='btn btn-info'    onclick=maildo('forw','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-edit icon-white'></i> 转发</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteinbox','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-trash icon-white'></i>   删除</a></td></tr></table></td></tr>");
                }
            })
        }

        //回
        function GetRebBox() {
            $("#thlable009").html("收件人");
            gettype = 2;
            $("#DivViewDT").hide();
            $("#divmaillist").show();


            $("#datalist").html("");

            $("#WNewEmail").hide();
            $("#divshow").show();
            $("#hlistname").html("<i class='icon-user'></i>回收站");

            $.post("userajax/Mail.aspx", { "type": "getmysenddata", "userid": userid, "searchtype": "2", "pagesizeindex": pagesizeindex }, function (data) {

                if (data == "-999") { alert("登录失效.请重新登录!"); return false; }

                if (data == "no") { $("#datainfoa").html("无数据!"); $("#btnmore").hide(); return; }
                $("#datainfoa").html("");
                var objresult = $.parseJSON(data);
                if (objresult == null) return;


                pagesizeindex = parseInt(pagesizeindex + objresult.length);

                if (objresult.length < persize)
                    $("#btnmore").hide();
                else
                    $("#btnmore").show();

                for (var i = 0; i < objresult.length; i++) {
                    $("#datalist").append("<tr id='TRList" + objresult[i].ID + "," + objresult[i].FID + "'><td>" + objresult[i].RESERVEDNAME + "</td><td class='center'>" + objresult[i].SENDTIME + "</td><td class='center'>" + objresult[i].TITLE + "</td><td class='center'><span class='label'>已进回收站</span></td><td>" + (objresult[i].ISATTACHFILE == '1' ? "<span class='icon32 icon-color icon-link' title='.icon32  .icon-color  .icon-link ' onclick=maildo('filedelete','" + objresult[i].ID + "," + objresult[i].FID + "')></span>" : " ") + "</td><td class='center'><table border='0'><tr border='0'><td><a class='btn btn-success'    onclick=maildo('backview','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-zoom-in icon-white'></i> 还原</a></td><td><a class='btn btn-danger'    onclick=maildo('deleteon','" + objresult[i].ID + "," + objresult[i].FID + "')><i class='icon-trash icon-white'></i>   彻底删除</a></td></tr></table></td></tr>");
                }
            })
        }