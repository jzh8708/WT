﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using DBS;
using System.Data.SqlClient;

namespace ICGSystemWeb
{
    public class CommonData
    {
        public static string usercode = "usercode";

        private string nologinin = "please login in";

        public static string nodata = "nodata";

        /// <summary>
        /// 调用短信猫(发短信)
        /// </summary>
        /// <param name="totel"></param>
        /// <returns></returns>
        public static string SendMesg(string totel)
        {
            if (totel == "" || totel.Length != 11) return "formate_error";//手机号码格式错误
            //调用短信猫(给某某手机号发短信)
            return "1";//成功
        }


        public string Pay(string userid, double money, string appid)
        {
            DBHelper DB = new DBHelper();

            string sql = string.Format("select count(0) from appmanagertable where ID='{0}'", appid);
            if (DB.ExecuteSingleString(sql) != "1")
                return "该应用不存在";

            sql = string.Format("select count(0) from appmanageruser where appid='{0}' and userid='{1}'", appid, userid);
            if (DB.ExecuteSingleString(sql, null) != "1")
                return "该用户未权限查看该应用";

            sql = string.Format("select userleftmoney from usertable where userid='{0}'", userid);
            if (double.Parse(DB.ExecuteSingleString(sql, null)) < money)
                return "该用户余额不足以支付该应用的金额";

            sql = string.Format("update usertable set userleftmoney=userleftmoney-{0}", money);
            DB.ExecuteNonQuery(sql, null);//success

            return "购买成功";
        }

        /// <summary>
        /// 购买产品通用方法【包括下载】
        /// </summary>
        /// <param name="money"></param>
        /// <returns></returns>
        public static string Pay(double money)
        {
            DBHelper DB = new DBHelper();

            string userid = GetUserID();

            string sql = string.Format("select userleftmoney from usertable where userid='{0}'", userid);
            if (double.Parse(DB.ExecuteSingleString(sql, null)) < money)
                return "-1";

            sql = string.Format("update usertable set userleftmoney=userleftmoney-{0}", money);
            DB.ExecuteNonQuery(sql, null);//success

            return "1";
        }

        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static int AddLog(string title)
        {
           return  AddLog(title, 0, 0);
        }


        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static int AddLog(string title, double money, int count)
        {
            title = title.Trim();

            string sql = string.Format("insert into TAB_OPERATELOG(costmoney,getsuercount,GUID,USERID,OPREATETIME,ORGNAME,CONTENT,OTHERUSE) values (@costmoney,@getsuercount,@GUID,@USERID,@OPREATETIME,@ORGNAME,@CONTENT,@OTHERUSE)");

            SqlParameter[] para = new SqlParameter[] { 
                  new SqlParameter("@costmoney",money),//消费金额
                  new SqlParameter("@getsuercount",count),//积分
                  new SqlParameter("@GUID",Guid.NewGuid().ToString()),
                  new SqlParameter("@USERID",GetUserID()),
                  new SqlParameter("@CONTENT",title),
                  new SqlParameter("@OTHERUSE",""),
                  new SqlParameter("@ORGNAME",CommonData.GetLoginUserName()),
                  new SqlParameter("OPREATETIME",DateTime.Now)
            };

            DBHelper db = new DBHelper();

            db.ExecuteNonQuery(sql, para);

            db = null;

            return 1;
        }

        /// <summary>
        /// 获取用户的类型
        /// </summary>
        /// <param name="userid">UserID</param>
        /// <returns></returns>
        public static string GetUserTypeByUserID(string userid)
        {
            string sql = string.Format("select usertype from usertable where userid='{0}'", userid);
            DBHelper db = new DBHelper();
            userid = db.ExecuteSingleString(sql);
            db = null;
            return userid;
        }

        /// <summary>
        /// 获取用户类型(重载)
        /// </summary>
        /// <returns></returns>
        public static string GetUserTypeByUserID()
        {
            return GetUserTypeByUserID(GetUserID());
        }

        public static string TAB_MAILSENDRESERVEDCloumns = @"
                                 FID  
                                ,MAILGUID      
                                ,SENDUSERID    
                                ,RESERVEDUSERID
                                ,SENDTIME      
                                ,ISBACKMSG     
                                ,STATUE        
                                ,SENDUSERNAME  
                                ,RESERVEDNAME";



        /// <summary>
        /// datatable转换为json对象
        /// </summary>
        /// <param name="dt">表</param>
        /// <returns>string</returns>
        public static string DataTableToJson(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0)
                return "";

            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("[");//转换成多个model的形式 
            int count = dt.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                strbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strbuild.Append("\"");
                    strbuild.Append(dt.Columns[j].ColumnName);
                    strbuild.Append("\":\"");
                    strbuild.Append(dt.Rows[i][j].ToString());
                    strbuild.Append("\",");
                }
                strbuild.Remove(strbuild.Length - 1, 1);
                strbuild.Append("},");
            }
            strbuild.Remove(strbuild.Length - 1, 1);
            strbuild.Append("]");
            return strbuild.ToString();
        }


        //获取当前登录用户ID
        public static string GetUserID()
        {
            string userid = GetCookie("usercode");
            return (userid == null || userid == "" ? "-999" : userid);
        }

        /// <summary>
        /// 根据id查询对应的用户名称[ok]
        /// </summary>
        /// <param name="intid">用户id</param>
        /// <returns></returns>
        public static string GetLoginUserName()
        {
            string sql = string.Format("select usernamenike from usertable where userid='{0}'", GetUserID());

            DBHelper db = new DBHelper();

            return db.ExecuteSingleString(sql, null);
        }


        /// <summary>
        /// 根据id查询对应的用户名称[ok]
        /// </summary>
        /// <param name="intid">用户id</param>
        /// <returns></returns>
        public static string GetLoginUserName(string userid)
        {
            string sql = string.Format("select usernamenike from usertable where userid='{0}'", userid);

            DBHelper db = new DBHelper();

            string result = db.ExecuteSingleString(sql, null);

            db = null;

            return result;
        }


        /// <summary>
        /// 获取cookies
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>string</returns>
        public static string GetCookie(string key)
        {
            object obj = CookieHelper.Get(key, key);
            if (obj == null)
            {
                return "";
            }
            else
            {
                return obj.ToString();
            }
        }

        /// <summary>
        /// 设置cookies
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        /// <returns>1</returns>
        public static int SetCookie(string key, string value)
        {
            CookieHelper.Set(key, key, value);
            return 1;
        }

        /// <summary>
        /// 清空cookies
        /// </summary>
        public static void ClearCookie()
        {
            CookieHelper.RemoveAll();
        }

        /// <summary>
        /// 设置缓存 val为null时表示清空key的缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <param name="value">值</param>
        public static void SetCache(string key, object value)
        {
            if (value == null)
            {
                HttpRuntime.Cache.Remove(key);
                return;
            }
            if (HttpRuntime.Cache[key] == null)
            {
                HttpRuntime.Cache.Insert(key, value);
            }
            else
            {
                HttpRuntime.Cache[key] = value;
            }
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key">键</param>
        /// <returns>object</returns>
        public static object GetCache(string key)
        {
            return HttpRuntime.Cache[key];
        }

        /// <summary>
        /// 获取唯一id
        /// </summary>
        /// <returns></returns>
        public static string GetGuid()
        {
            return System.Guid.NewGuid().ToString();
        }

    }
}