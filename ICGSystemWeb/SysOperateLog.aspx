﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App.Master" AutoEventWireup="true" CodeBehind="SysOperateLog.aspx.cs" Inherits="ICGSystemWeb.SysOperateLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMaaster" runat="server">
             <div class="row-fluid sortable">		
				    <div class="box span12">
					    <div class="box-header well" data-original-title>
						    <h2><i class="icon-user"></i> 数据日志</h2>
						    <div class="box-icon">
						    </div>
					    </div>
					    <div class="box-content">
                        <div align="left" id="divsearch">
                        
                        日期:
                        <input type="text" class="validate[maxSize[50],custom[date]]" style="width:80px"  onclick="WdatePicker()" id="d1" />-
                        <input type="text"  class="validate[maxSize[50],custom[date]]"  style="width:80px"   onclick="WdatePicker()" id="d2" />
                        <input align="middle" type="button" style="font-size:15px" class="btn" id="btnsearchs" value="搜索" />
                        <input align="middle" type="button" style="font-size:15px" class="btn" id="btncleardata" value="清空搜索条件" />
                        
                        </div>
                        <a style="display:none" id="selectalldelete" class="btn btn-danger"><i class="icon-trash icon-white"></i>Delete</a>
                        <div align="center">
                          <a  id="acount1" style="color:Red"></a>
                        </div>
					    <table class="table table-striped table-bordered bootstrap-datatable datatable">
						      <thead>
							      <tr>
                                      <th id="selectall" style="width:5%">
                                      <input type="button"  value="全选" class="btn" id="Buttonsleall" /></th>
                                      <th style="width:15%">时间</th>
								      <th style="width:15%">操作人</th>
								      <th style="width:55%">内容</th>
								      <th style="width:10%">操作</th>
							      </tr>
						      </thead>   
						      <tbody id="tbodyList">
					 
						      </tbody>
					      </table>  
                          <div align="center">
                          <input type="button" class="btn"  value="加载更多" id="btnmore" style="display:none" />
                            <a id="anodata" style="display:none">无数据!</a>          
                          </div>
    			    </div>
		     </div>
        </div>

    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script> 
    <script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript" src="Script/My97DatePicker/WdatePicker.js"></script>
    <link   href="Script/validationEngine/css/validationEngine.jquery.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src='Script/validationEngine/languages/jquery.validationEngine-zh_CN.js'></script>
    <script type="text/javascript" src='Script/validationEngine/jquery.validationEngine.js'></script>
    <script type="text/javascript" src="js/script/file/OperateLog.js"></script>
</asp:Content>
