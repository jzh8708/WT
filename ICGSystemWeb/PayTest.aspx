﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayTest.aspx.cs" Inherits="ICGSystemWeb.PayTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>产品购买</title>
    <script type="text/javascript" src="js/jquery-1.8.0.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnpay").click(function () {
                if (confirm("确定购买吗？") == false) return;
                var sendpa = '{ "userid": "' + $("#txtuserid").val() + '", "money": "' + $("#txtmoney").val() + '", "appid": "' + $("#txtappid").val() + '" }';
                $.ajax(
                { type: "POST",
                    url: "HGXWBS.asmx/Pay",
                    data: sendpa,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (redata) {
                        alert(redata.d);
                    },
                    error: function (a) {
                        //alert(a)
                    }
                });

            })
        })
    </script>
</head>
<body>
    用户ID：<input type="text" id="txtuserid" /><br />
    应用ID：<input type="text" id="txtappid" /><br />
    金额：<input type="text" id="txtmoney" />元<br />
    <input type="button" value="登录购买" id="btnpay" />
    <br />
</body>
</html>
