﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Play.aspx.cs" Inherits="ICGSystemWeb.video.Play" %>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
    <meta charset="utf-8">
    <title>blueimp Gallery</title>
    <meta name="description" content="blueimp Gallery is a touch-enabled, responsive and customizable image and video gallery, carousel and lightbox, optimized for both mobile and desktop web browsers. It features swipe, mouse and keyboard navigation, transition effects, slideshow functionality, fullscreen support and on-demand content loading and can be extended to display additional content types.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/blueimp-gallery.css">
    <link rel="stylesheet" href="css/blueimp-gallery-indicator.css">
    <link rel="stylesheet" href="css/blueimp-gallery-video.css">
    <link rel="stylesheet" href="css/demo.css">
</head>
<body>
    <h2>
        视频播放</h2>
    <!-- The Gallery as inline carousel, can be positioned anywhere on the page -->
    <div id="blueimp-video-carousel" class="blueimp-gallery blueimp-gallery-controls blueimp-gallery-carousel">
        <div class="slides">
        </div>
        <h3 class="title">
        </h3>
        <a class="prev">‹</a> <a class="next">›</a> <a class="play-pause"></a>
    </div>

    <script src="js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript">
            var id = "";
            $(function () {
                id = $("#<%=sid.ClientID %>").val();
            })
    </script>
    <script src="js/blueimp-helper.js"></script>
    <script src="js/blueimp-gallery.js"></script>
    <script src="js/blueimp-gallery-fullscreen.js"></script>
    <script src="js/blueimp-gallery-indicator.js"></script>
    <script src="js/blueimp-gallery-video.js"></script>
    <script src="js/blueimp-gallery-vimeo.js"></script>
    <script src="js/blueimp-gallery-youtube.js"></script>
    <script type="text/javascript" src="js/jquery.blueimp-gallery.js"></script>
    <input type="hidden" runat="server" id="sid" />
    <script type="text/javascript" src="js/demo.js"></script>
    <div align="center">
        <a style="font-size: 18px">下载</a> <a style="font-size: 18px" href="../XlsModel/VideoList.aspx">
            返回</a>
    </div>
</body>
</html>
