/*
 * blueimp Gallery Demo JS 2.10.0
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global window, blueimp, $ */

$(function () {
    // 'use strict';

    // Load demo images from flickr:
    /*
    $.ajax({
    url: (window.location.protocol === 'https:' ?
    'https://secure' : 'http://api') +
    '.flickr.com/services/rest/',
    data: {
    format: 'json',
    method: 'flickr.interestingness.getList',
    api_key: '7617adae70159d09ba78cfec73c13be3' // jshint ignore:line
    },
    dataType: 'jsonp',
    jsonp: 'jsoncallback'
    }).done(function (result) {
    var carouselLinks = [],
    linksContainer = $('#links'),
    baseUrl;
    // Add the demo images as links with thumbnails to the page:
    $.each(result.photos.photo, function (index, photo) {
    baseUrl = 'http://farm' + photo.farm + '.static.flickr.com/' +
    photo.server + '/' + photo.id + '_' + photo.secret;
    $('<a/>')
    .append($('<img>').prop('src', baseUrl + '_s.jpg'))
    .prop('href', baseUrl + '_b.jpg')
    .prop('title', photo.title)
    .attr('data-gallery', '')
    .appendTo(linksContainer);
    carouselLinks.push({
    href: baseUrl + '_c.jpg',
    title: photo.title
    });
    });
    // Initialize the Gallery as image carousel:
    blueimp.Gallery(carouselLinks, {
    container: '#blueimp-image-carousel',
    carousel: true
    });
    });
    */

    var all = [];

    ReLoad();

    function ReLoad() {

        var toptime = "";


        $.post("../userajax/Video.aspx", { "type": "getlist2", "toptime": toptime, "id": id }, function (datare) {

            var obj = $.parseJSON(datare);

            if (obj == null) return;

            toptime = obj[obj.length - 1].createtime;

            for (var i = 0; i < 1; i++) {
                $("#datalist").append("<tr id='TRList" + obj[i].title + "'><td>" + obj[i].title + "</td><td class='center'>" + obj[i].videosize + "KB</td><td class='center'>" + obj[i].createtime + "</td><td></td></tr>");
                var objone = {};
                objone.title = obj[i].title;
                objone.href = "../userajax/" + obj[i].href;
                objone.type = 'video/mp4';
                objone.poster = "../userajax/" + obj[i].poster;
                all.push(objone);
                objone = null;
            }

            // Initialize the Gallery as video carousel:
            blueimp.Gallery(all, {
                container: '#blueimp-video-carousel',
                carousel: true
            });

        })
    }
});
