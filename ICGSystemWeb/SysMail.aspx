﻿<%@ Page Language="C#" MasterPageFile="~/App.Master" AutoEventWireup="true" CodeFile="SysMail.aspx.cs"
    Inherits="SysMail" Title="邮件管理" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMaaster" runat="Server">
    <br />
    <div class="sortable row-fluid">
        <a data-rel="tooltip" title="写新邮件" class="well span3 top-block" id="newemialdiv"><span
            class="icon32 icon-color icon-add"></span>
            <div>
                写新邮件</div>
            <div>
                Send Email</div>
            <span class="notification red">Send Email</span> </a><a data-rel="tooltip" title="收件箱"
                class="well span3 top-block" id="A2"><span class="icon32 icon-color icon-envelope-closed">
                </span>
                <div>
                    收件箱</div>
                <div>
                    Inbox Box</div>
                <span class="notification red">Inbox Box</span> </a><a data-rel="tooltip" title="发件箱"
                    class="well span3 top-block" id="A1"><span class="icon32 icon-color icon-envelope-open">
                    </span>
                    <div>
                        发件箱</div>
                    <div>
                        Out Box</div>
                    <span class="notification red">Out Box</span> </a><a data-rel="tooltip" title="回收站"
                        class="well span3 top-block" id="A3"><span class="icon32 icon-color icon-trash">
                        </span>
                        <div>
                            回收站</div>
                        <div>
                            Recyclebin Box</div>
                        <span class="notification red">Recyclebin Box</span> </a>
    </div>
    <div id="divsysmsg" class="popover fade top in" style="top: 0px; left: 50%; display: none;">
        <div class="arrow">
        </div>
        <div class="popover-inner">
            <h3 class="popover-title">
                系统提示
            </h3>
            <div class="popover-content" id="divopreateresult">
                <p>
                    操作成功
                    <br></br>
                    <p>
                    </p>
                </p>
            </div>
        </div>
    </div>
    <div id="myModalA" class="modal hide fade in" style="display: none;">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                ×
            </button>
            <h3>
                选择收件人:
            </h3>
        </div>
        <div class="modal-body" id="divpeoplelistitle">
            <p>
                <input type="checkbox" name="checkbox" id="Checkboxall" />全选
                <input type="text" id="searchpeopletxt" class="validate[maxSize[50],custom[noSpecialCaracters]]" /><a
                    id="acount"></a><img title="搜索" src="img/ajax-loaders/ajax-loader-1.gif" id="imgprogress"
                        style="display: none;" />
                <p>
                </p>
                <div id="divchecklist">
                </div>
            </p>
        </div>
        <div class="modal-footer">
            <a class="btn" data-dismiss="modal">关闭 </a><a class="btn btn-primary" data-dismiss="modal"
                id="lasure">确定 </a>
        </div>
    </div>
    <div id="divall">
        <div class="row-fluid sortable" id="WNewEmail" style="display: none">
            <div class="box span12">
                <div class="box-header well" data-original-title>
                    <h2>
                        <i class="icon-edit"></i>发送新邮件</h2>
                    <div class="box-icon">
                        <a class="btn btn-round" id="btnclosedffsdfla"><i class="icon"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <legend></legend>
                    <div class="control-group">
                        <div class="control-group">
                            <label class="control-label" for="inputIcon">
                                收件人:
                            </label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-envelope"></i></span>
                                    <input id="inputSendobjName" enableviewstate="false" class="validate[required]" type="text"
                                        style="width: 50%">
                                        <input id="inputSendobj" style="display: none" type="text" style="width: 60%"> </input>
                                    </input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="date01">
                            标题</label>
                        <div class="controls">
                            <input type="text" class="input-xlarge  validate[required,custom[noSpecialCaracters]] "
                                id="date01" style="width: 52.3%">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="fileInput">
                            附件</label>
                        <div class="controls">
                            <table id="MailFileList2" class="table table-condensed" style="display: none; width: 50%">
                                <thead>
                                    <tr>
                                        <th>
                                            名称
                                        </th>
                                        <th>
                                            附件大小
                                        </th>
                                        <th>
                                            类型
                                        </th>
                                        <th>
                                            删除
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="FilelistTbody">
                                </tbody>
                            </table>
                            <span id="adlaimgFile" class="icon32 icon-color icon-unlink" title=".icon32  .icon-color  .icon-unlink ">
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">
                            正文</label>
                        <div class="controls">
                            <textarea class="cleditor" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">
                            发送时间</label>
                        <div class="controls">
                            <input type="text" id="txtsendtime" class="validate[required]" style="width: 100px"
                                onclick="WdatePicker()" />
                            <input type="text" id="txthour" value="00" style="width: 30px" />
                            时
                            <input type="text" id="txtmini" value="00" style="width: 30px" />
                            分
                        </div>
                    </div>
                    <div>
                        <input type="button" class="btn btn-primary" id="btnsavedata" value="发送" />
                        <button type="reset" class="btn" id="btncanceldiv">
                            取消</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid sortable" id="divmaillist">
        <div class="box span12" id="divshow">
            <div class="box-header well" data-original-title>
                <h2 id="hlistname">
                    <i class="icon-user"></i>
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th id="thlable009" style="width: 10%">
                                发送人
                            </th>
                            <th style="width: 10%">
                                时间
                            </th>
                            <th style="width: 30%">
                                标题
                            </th>
                            <th style="width: 10%">
                                状态
                            </th>
                            <th style="width: 10%">
                                附件
                            </th>
                            <th style="width: 30%">
                                操作
                            </th>
                        </tr>
                    </thead>
                    <tbody id="datalist">
                    </tbody>
                </table>
                <div align="center">
                    <input type="button" class="btn" value="加载更多" id="btnmore" style="display: none" />
                </div>
                <div class="row-fluid">
                    <div class="span12">
                    </div>
                    <div class="span12 center" id="datainfoa">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid sortable" id="DivViewDT" style="display: none">
        <div class="box span12" id="div1">
            <div class="box-header well" data-original-title>
                <h2 id="h1">
                    <i class="icon icon-color icon-book"></i>邮件详情</h2>
                <div class="box-icon">
                    <a class="btn btn-round" id="A4close"><i class="icon"></i></a>
                </div>
            </div>
            <div class="box-content">
                <h3 id="ViewTitle" align="center">
                </h3>
                <p>
                    <a id="sendlable" style="color: Black">发送人员</a>
                </p>
                <p>
                    <a id="reserveduname" style="color: Black">接收人员</a>
                </p>
                <p>
                    <a id="sendtime" style="color: Black">发送时间</a>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <p>
                </p>
                <br />
                <br />
                <br />
                <br />
                <p id="contentlable">
                </p>
                <table id="tablefilelist" class="table table-condensed">
                    <thead>
                        <tr>
                            <th>
                                附件名称
                            </th>
                            <th>
                                附件大小
                            </th>
                            <th>
                                类型
                            </th>
                            <th>
                                下载
                            </th>
                        </tr>
                    </thead>
                    <tbody id="Tbodyfilelistview">
                    </tbody>
                </table>
                <a id="Afileresult"></a>
            </div>
        </div>
    </div>
    <div id="DivFiles" class="modal hide fade in" style="display: none;">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                ×
            </button>
            <h3>
                邮件附件列表:
            </h3>
        </div>
        <div class="modal-body">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>
                            附件名称
                        </th>
                        <th>
                            附件大小
                        </th>
                        <th>
                            类型
                        </th>
                        <th>
                            下载
                        </th>
                    </tr>
                </thead>
                <tbody id="FileListdata">
                </tbody>
            </table>
            <a id="fileinfoa"></a>
        </div>
        <div class="modal-footer">
            <a class="btn" data-dismiss="modal">关闭 </a>
        </div>
    </div>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.elfinder.min.js"></script>
    <script type="text/javascript" src="js/jquery.uploadify-3.1.min.js"></script>
    <script type="text/javascript" src="Script/ajax_upload.js"></script>
    <script type="text/javascript" src="js/charisma.js"></script>
    <link href="Script/validationEngine/css/validationEngine.jquery.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src='Script/validationEngine/languages/jquery.validationEngine-zh_CN.js'></script>
    <script type="text/javascript" src='Script/validationEngine/jquery.validationEngine.js'></script>
    <script type="text/javascript" src="js/script/file/Mail.js"></script>
    <script language="javascript" type="text/javascript" src="Script/My97DatePicker/WdatePicker.js"></script>
</asp:Content>
