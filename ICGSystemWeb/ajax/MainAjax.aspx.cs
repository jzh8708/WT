﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using MyDAL;

namespace ICGSystemWeb.ajax
{
    public partial class MainAjax : System.Web.UI.Page
    {
        ICGSQLDAl SQLDAL = new ICGSQLDAl();

        ICGCSDAL CSDAL = new ICGCSDAL();

        UserDAL UDAL = new UserDAL();

        ICGAJaxDAL AjaxDAl = new ICGAJaxDAL();

        ICGJSDAL html = new ICGJSDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");

            if (obj == null || obj.ToString() == "") { Response.Write("nodata"); Response.End(); }

            string type = obj.ToString();
            obj = null;
            if (type == "new")
                Response.Write(New());

            Response.End();
        }

        //创建新文件
        private string New()
        {
            string coulmn = Request.Form.Get("data");
            if (coulmn == null || coulmn.Length < 2)
                return "-1";

            List<string[]> list = new List<string[]>();
            string[] bigarray = coulmn.Split(',');
            string[] onearray = null;//一个字段、说明、数据类型、等

            for (int i = 0; i < bigarray.Length; i++)
            {
                onearray = bigarray[i].Split('#');
                list.Add(onearray);
                onearray = null;
            }

            //list 只用于cs 前端生成  把枚举enum--》int处理开始
            if (list != null && list.Count > 0)
            {
                for (int j = 0; j < list.Count; j++)
                {
                    if (list[j][2] == "enum")
                        list[j][2] = "int";
                }
            }

            string jsonstring = CommonData.DataTableToJson(GetDTBYString(bigarray));//val 
            string result = "";

            if (Request.Form.Get("usertableid") == "-1")
                result = UDAL.AddDB(Request.Form.Get("userid"), Request.Form.Get("proid"), Request.Form.Get("tablename"), jsonstring);
            else
            {
                UDAL.Update(Request.Form.Get("tablename"), jsonstring, Request.Form.Get("usertableid"));
                result = Request.Form.Get("usertableid");
            }

            bigarray = null;

            CSDAL.NewCSFile(Request.Form.Get("userproname"), Request.Form.Get("tablename") + "OP", Request.Form.Get("tablename"), Server.MapPath("CS/"), list);

            AjaxDAl.NewCSFileAspx(Request.Form.Get("userproname"), Request.Form.Get("tablename") + "OP", Request.Form.Get("tablename"), Server.MapPath("AJAXFILE/"), list);

            AjaxDAl.NewCSFileAjaxCS(Request.Form.Get("userproname"), Request.Form.Get("tablename") + "OP", Request.Form.Get("tablename"), Server.MapPath("AJAXFILE/"), list);

            html.NewHtmlFile(Request.Form.Get("userproname"), Request.Form.Get("tablename"), Request.Form.Get("tablename"), Server.MapPath("HTML/"), list);

            return SQLDAL.NewSQLFile(Request.Form.Get("tablename"), Server.MapPath("SQL/"), list) + "#" + result;
        }

        private DataTable GetDTBYString(string[] array)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TNAME", typeof(string));//0 名称
            dt.Columns.Add("TDESCRIPTION", typeof(string));//1 描述
            dt.Columns.Add("SELECTVAL", typeof(string));//2 类型
            dt.Columns.Add("TLENGTH", typeof(string));//3  //长度
            dt.Columns.Add("TCHECKBOXVAL", typeof(string));//4  //必填
            dt.Columns.Add("ENUMID", typeof(string));//枚举ID//5
            dt.Columns.Add("ENUMNAME", typeof(string));//枚举名称//6
            dt.Columns.Add("ISENUM", typeof(string));//是否为枚举字段//7
            dt.Columns.Add("QUERYCOLUMN", typeof(string));//是否为列表查询字段//8
            
            DataRow dw = null;
            string[] ds = null;
            foreach (string a in array)
            {
                ds = a.Split('#');
                if (ds[0] == "ID" || ds[0] == "CreatTime") continue;
                dw = dt.NewRow();
                dw.ItemArray = ds;
                dt.Rows.Add(dw);
                dw = null;
            }
            return dt;
        }
    }
}