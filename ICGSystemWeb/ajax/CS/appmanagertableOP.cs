using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class appmanagertableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string appid,string appname,string appsrc,string appcount,string appicopath,string userid,string guid)
{
string sql = string.Format("insert into appmanagertable (ID,appid,appname,appsrc,appcount,appicopath,userid) values(@ID,@appid,@appname,@appsrc,@appcount,@appicopath,@userid)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@appid",appid),
                  new SqlParameter("@appname",appname),
                  new SqlParameter("@appsrc",appsrc),
                  new SqlParameter("@appcount",appcount),
                  new SqlParameter("@appicopath",appicopath),
                  new SqlParameter("@userid",userid)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string appid,string appname,string appsrc,string appcount,string appicopath,string userid,string guid)
{
string sql = string.Format("update  appmanagertable set ID=@ID,appid=@appid,appname=@appname,appsrc=@appsrc,appcount=@appcount,appicopath=@appicopath,userid=@userid where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@appid",appid),
                     new SqlParameter("@appname",appname),
                     new SqlParameter("@appsrc",appsrc),
                     new SqlParameter("@appcount",appcount),
                     new SqlParameter("@appicopath",appicopath),
                     new SqlParameter("@userid",userid),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from appmanagertable where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, appid, appname, appsrc, appcount, appicopath, userid, CreatTime from appmanagertable where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} appid,appname,appsrc,appcount,appicopath,userid, ID,CreatTime from appmanagertable where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

