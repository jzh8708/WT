using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class grouptableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string groupid,string groupname,string groupcreattime,string createpeoplename,string belonguserid,string guid)
{
string sql = string.Format("insert into grouptable (ID,groupid,groupname,groupcreattime,createpeoplename,belonguserid) values(@ID,@groupid,@groupname,@groupcreattime,@createpeoplename,@belonguserid)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@groupid",groupid),
                  new SqlParameter("@groupname",groupname),
                  new SqlParameter("@groupcreattime",groupcreattime),
                  new SqlParameter("@createpeoplename",createpeoplename),
                  new SqlParameter("@belonguserid",belonguserid)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string groupid,string groupname,string groupcreattime,string createpeoplename,string belonguserid,string guid)
{
string sql = string.Format("update  grouptable set ID=@ID,groupid=@groupid,groupname=@groupname,groupcreattime=@groupcreattime,createpeoplename=@createpeoplename,belonguserid=@belonguserid where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@groupid",groupid),
                     new SqlParameter("@groupname",groupname),
                     new SqlParameter("@groupcreattime",groupcreattime),
                     new SqlParameter("@createpeoplename",createpeoplename),
                     new SqlParameter("@belonguserid",belonguserid),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from grouptable where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, groupid, groupname, groupcreattime, createpeoplename, belonguserid, CreatTime from grouptable where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} groupid,groupname,groupcreattime,createpeoplename,belonguserid, ID,CreatTime from grouptable where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

