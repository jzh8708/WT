using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class userappshoptableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string userid,string appname,string isbought,string trycounts,string boughttime,string appicopath,string apppath,string guid)
{
string sql = string.Format("insert into userappshoptable (ID,userid,appname,isbought,trycounts,boughttime,appicopath,apppath) values(@ID,@userid,@appname,@isbought,@trycounts,@boughttime,@appicopath,@apppath)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@userid",userid),
                  new SqlParameter("@appname",appname),
                  new SqlParameter("@isbought",isbought),
                  new SqlParameter("@trycounts",trycounts),
                  new SqlParameter("@boughttime",boughttime),
                  new SqlParameter("@appicopath",appicopath),
                  new SqlParameter("@apppath",apppath)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string userid,string appname,string isbought,string trycounts,string boughttime,string appicopath,string apppath,string guid)
{
string sql = string.Format("update  userappshoptable set ID=@ID,userid=@userid,appname=@appname,isbought=@isbought,trycounts=@trycounts,boughttime=@boughttime,appicopath=@appicopath,apppath=@apppath where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@userid",userid),
                     new SqlParameter("@appname",appname),
                     new SqlParameter("@isbought",isbought),
                     new SqlParameter("@trycounts",trycounts),
                     new SqlParameter("@boughttime",boughttime),
                     new SqlParameter("@appicopath",appicopath),
                     new SqlParameter("@apppath",apppath),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from userappshoptable where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, userid, appname, isbought, trycounts, boughttime, appicopath, apppath, CreatTime from userappshoptable where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} userid,appname,isbought,trycounts,boughttime,appicopath,apppath, ID,CreatTime from userappshoptable where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

