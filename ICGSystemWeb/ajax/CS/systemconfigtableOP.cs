using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class systemconfigtableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string sysid,string webimg,string poweredby,string usermoneychangecount,string opreatclickcount,string userrestpassword,string guid)
{
string sql = string.Format("insert into systemconfigtable (ID,sysid,webimg,poweredby,usermoneychangecount,opreatclickcount,userrestpassword) values(@ID,@sysid,@webimg,@poweredby,@usermoneychangecount,@opreatclickcount,@userrestpassword)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@sysid",sysid),
                  new SqlParameter("@webimg",webimg),
                  new SqlParameter("@poweredby",poweredby),
                  new SqlParameter("@usermoneychangecount",usermoneychangecount),
                  new SqlParameter("@opreatclickcount",opreatclickcount),
                  new SqlParameter("@userrestpassword",userrestpassword)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string sysid,string webimg,string poweredby,string usermoneychangecount,string opreatclickcount,string userrestpassword,string guid)
{
string sql = string.Format("update  systemconfigtable set ID=@ID,sysid=@sysid,webimg=@webimg,poweredby=@poweredby,usermoneychangecount=@usermoneychangecount,opreatclickcount=@opreatclickcount,userrestpassword=@userrestpassword where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@sysid",sysid),
                     new SqlParameter("@webimg",webimg),
                     new SqlParameter("@poweredby",poweredby),
                     new SqlParameter("@usermoneychangecount",usermoneychangecount),
                     new SqlParameter("@opreatclickcount",opreatclickcount),
                     new SqlParameter("@userrestpassword",userrestpassword),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from systemconfigtable where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, sysid, webimg, poweredby, usermoneychangecount, opreatclickcount, userrestpassword, CreatTime from systemconfigtable where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} sysid,webimg,poweredby,usermoneychangecount,opreatclickcount,userrestpassword, ID,CreatTime from systemconfigtable where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

