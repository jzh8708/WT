using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class xlstableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string type,string xlsname,string xlsshougai,string xlstopimgpath,string needcount,string tocount,string needmoney,string downloadcount,string filepath,string filesize,string uploadtime,string uploaduserid,string uploaduername,string guid)
{
string sql = string.Format("insert into xlstable (ID,type,xlsname,xlsshougai,xlstopimgpath,needcount,tocount,needmoney,downloadcount,filepath,filesize,uploadtime,uploaduserid,uploaduername) values(@ID,@type,@xlsname,@xlsshougai,@xlstopimgpath,@needcount,@tocount,@needmoney,@downloadcount,@filepath,@filesize,@uploadtime,@uploaduserid,@uploaduername)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@type",type),
                  new SqlParameter("@xlsname",xlsname),
                  new SqlParameter("@xlsshougai",xlsshougai),
                  new SqlParameter("@xlstopimgpath",xlstopimgpath),
                  new SqlParameter("@needcount",needcount),
                  new SqlParameter("@tocount",tocount),
                  new SqlParameter("@needmoney",needmoney),
                  new SqlParameter("@downloadcount",downloadcount),
                  new SqlParameter("@filepath",filepath),
                  new SqlParameter("@filesize",filesize),
                  new SqlParameter("@uploadtime",uploadtime),
                  new SqlParameter("@uploaduserid",uploaduserid),
                  new SqlParameter("@uploaduername",uploaduername)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string type,string xlsname,string xlsshougai,string xlstopimgpath,string needcount,string tocount,string needmoney,string downloadcount,string filepath,string filesize,string uploadtime,string uploaduserid,string uploaduername,string guid)
{
string sql = string.Format("update  xlstable set ID=@ID,type=@type,xlsname=@xlsname,xlsshougai=@xlsshougai,xlstopimgpath=@xlstopimgpath,needcount=@needcount,tocount=@tocount,needmoney=@needmoney,downloadcount=@downloadcount,filepath=@filepath,filesize=@filesize,uploadtime=@uploadtime,uploaduserid=@uploaduserid,uploaduername=@uploaduername where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@type",type),
                     new SqlParameter("@xlsname",xlsname),
                     new SqlParameter("@xlsshougai",xlsshougai),
                     new SqlParameter("@xlstopimgpath",xlstopimgpath),
                     new SqlParameter("@needcount",needcount),
                     new SqlParameter("@tocount",tocount),
                     new SqlParameter("@needmoney",needmoney),
                     new SqlParameter("@downloadcount",downloadcount),
                     new SqlParameter("@filepath",filepath),
                     new SqlParameter("@filesize",filesize),
                     new SqlParameter("@uploadtime",uploadtime),
                     new SqlParameter("@uploaduserid",uploaduserid),
                     new SqlParameter("@uploaduername",uploaduername),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from xlstable where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, type, xlsname, xlsshougai, xlstopimgpath, needcount, tocount, needmoney, downloadcount, filepath, filesize, uploadtime, uploaduserid, uploaduername, CreatTime from xlstable where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} xlsname,xlstopimgpath,needcount,tocount,needmoney,downloadcount,uploadtime,uploaduserid,uploaduername, ID,CreatTime from xlstable where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

