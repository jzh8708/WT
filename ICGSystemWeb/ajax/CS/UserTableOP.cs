using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class usertableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string userid,string userfileid,string username,string userpass,string usergroupid,string isallowlogin,string userleftmoney,string usercount,string userheadlinkname,string userlastlogintime,string guid)
{
string sql = string.Format("insert into usertable (ID,userid,userfileid,username,userpass,usergroupid,isallowlogin,userleftmoney,usercount,userheadlinkname,userlastlogintime) values(@ID,@userid,@userfileid,@username,@userpass,@usergroupid,@isallowlogin,@userleftmoney,@usercount,@userheadlinkname,@userlastlogintime)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@userid",userid),
                  new SqlParameter("@userfileid",userfileid),
                  new SqlParameter("@username",username),
                  new SqlParameter("@userpass",userpass),
                  new SqlParameter("@usergroupid",usergroupid),
                  new SqlParameter("@isallowlogin",isallowlogin),
                  new SqlParameter("@userleftmoney",userleftmoney),
                  new SqlParameter("@usercount",usercount),
                  new SqlParameter("@userheadlinkname",userheadlinkname),
                  new SqlParameter("@userlastlogintime",userlastlogintime)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string userid,string userfileid,string username,string userpass,string usergroupid,string isallowlogin,string userleftmoney,string usercount,string userheadlinkname,string userlastlogintime,string guid)
{
string sql = string.Format("update  usertable set ID=@ID,userid=@userid,userfileid=@userfileid,username=@username,userpass=@userpass,usergroupid=@usergroupid,isallowlogin=@isallowlogin,userleftmoney=@userleftmoney,usercount=@usercount,userheadlinkname=@userheadlinkname,userlastlogintime=@userlastlogintime where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@userid",userid),
                     new SqlParameter("@userfileid",userfileid),
                     new SqlParameter("@username",username),
                     new SqlParameter("@userpass",userpass),
                     new SqlParameter("@usergroupid",usergroupid),
                     new SqlParameter("@isallowlogin",isallowlogin),
                     new SqlParameter("@userleftmoney",userleftmoney),
                     new SqlParameter("@usercount",usercount),
                     new SqlParameter("@userheadlinkname",userheadlinkname),
                     new SqlParameter("@userlastlogintime",userlastlogintime),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from usertable where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, userid, userfileid, username, userpass, usergroupid, isallowlogin, userleftmoney, usercount, userheadlinkname, userlastlogintime, CreatTime from usertable where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} userid,userfileid,username,userpass,usergroupid,isallowlogin,userleftmoney,usercount,userheadlinkname,userlastlogintime, ID,CreatTime from usertable where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

