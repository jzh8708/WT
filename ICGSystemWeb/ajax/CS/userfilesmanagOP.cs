using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  HGX
{
public class userfilesmanagOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string userid,string fileid,string filename,string filesize,string filetype,string filepath,string isshare,string sharepath,string uploadtime,string guid)
{
string sql = string.Format("insert into userfilesmanag (ID,userid,fileid,filename,filesize,filetype,filepath,isshare,sharepath,uploadtime) values(@ID,@userid,@fileid,@filename,@filesize,@filetype,@filepath,@isshare,@sharepath,@uploadtime)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@userid",userid),
                  new SqlParameter("@fileid",fileid),
                  new SqlParameter("@filename",filename),
                  new SqlParameter("@filesize",filesize),
                  new SqlParameter("@filetype",filetype),
                  new SqlParameter("@filepath",filepath),
                  new SqlParameter("@isshare",isshare),
                  new SqlParameter("@sharepath",sharepath),
                  new SqlParameter("@uploadtime",uploadtime)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string userid,string fileid,string filename,string filesize,string filetype,string filepath,string isshare,string sharepath,string uploadtime,string guid)
{
string sql = string.Format("update  userfilesmanag set ID=@ID,userid=@userid,fileid=@fileid,filename=@filename,filesize=@filesize,filetype=@filetype,filepath=@filepath,isshare=@isshare,sharepath=@sharepath,uploadtime=@uploadtime where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@userid",userid),
                     new SqlParameter("@fileid",fileid),
                     new SqlParameter("@filename",filename),
                     new SqlParameter("@filesize",filesize),
                     new SqlParameter("@filetype",filetype),
                     new SqlParameter("@filepath",filepath),
                     new SqlParameter("@isshare",isshare),
                     new SqlParameter("@sharepath",sharepath),
                     new SqlParameter("@uploadtime",uploadtime),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from userfilesmanag where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, userid, fileid, filename, filesize, filetype, filepath, isshare, sharepath, uploadtime, CreatTime from userfilesmanag where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} userid,fileid,filename,filesize,filetype,filepath,isshare,sharepath,uploadtime, ID,CreatTime from userfilesmanag where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

