using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
 namespace  test
{
public class test_tableOP {
 //实例化
DBHelper DB = new DBHelper();
//新增
public string AddDB(string ID,string UserName,string UserPass,string Age,string guid)
{
string sql = string.Format("insert into test_table (ID,UserName,UserPass,Age) values(@ID,@UserName,@UserPass,@Age)");
SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@UserName",UserName),
                  new SqlParameter("@UserPass",UserPass),
                  new SqlParameter("@Age",Age)
                        };
DB.ExecuteNonQuery(sql, sqlpara);
return guid;
}
//修改
public string UpdateDB(string ID,string UserName,string UserPass,string Age,string guid)
{
string sql = string.Format("update  test_table set ID=@ID,UserName=@UserName,UserPass=@UserPass,Age=@Age where ID=@guid");
SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@UserName",UserName),
                     new SqlParameter("@UserPass",UserPass),
                     new SqlParameter("@Age",Age),
                      new SqlParameter("@guid", guid)
                           };
 return DB.ExecuteNonQuery(sql, para).ToString();
}
//删除
public string DeleteDB(string guid)
{
string sql = string.Format("delete from test_table where ID=@ID");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid)};
return DB.ExecuteNonQuery(sql, para).ToString();
}
//获取一条ID值为guid的数据
 public DataTable GetOneDataByGuid(string guid)
{
 string sql = string.Format("select  ID, UserName, UserPass, Age, CreatTime from test_table where ID=@guid");
SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid)};
return DB.GetDataTable(sql, para);
}
//获取用户定义数量的数据列表
public DataTable GetTopNumberData(int pagesize, string endtime)
{
string sql = string.Format("select top {0} UserName,UserPass,Age, ID,CreatTime from test_table where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
 return DB.GetDataTable(sql, null);
}
}
}

