﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBS;
using System.Data.SqlClient;
using System.Data;

namespace ICGSystemWeb.ajax
{
    public partial class myajax : System.Web.UI.Page
    {
        private DBHelper db = new DBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");

            if (obj == null || obj.ToString() == "") { Response.Write("nodata"); Response.End(); }

            string type = obj.ToString();

            obj = null;
            //新增项目
            if (type == "new")
                type = NewPro();
            //删除项目
            else if (type == "delete")
                type = delete();
            //获取项目列表
            else if (type == "getprodt")
                type = getprodt();
            //获取某项目的所有table
            else if (type == "getprotableDT")
                type = getprotableDT();
            //点击一个表获取所有的字段
            else if (type == "getonevallist")
                type = getonevallist();
            //删除表
            else if (type == "deletetable")
                type = deletetable();
            //获取用户的枚举值
            else if (type == "getenumdata")
                type = GetEnumData();


            Response.Write(type);
            Response.End();
        }

        //获取用户的所有枚举值
        private string GetEnumData()
        {
            string sql = string.Format("select ID,Name from ICGEnumTab where UserID='{0}' ", Request.Form.Get("userid"));

            if (Request.Form.Get("str") != null && Request.Form.Get("str") != "")
                sql = string.Format("select ID,Name from ICGEnumTab where UserID='{0}' and Name like '%{1}%'", Request.Form.Get("userid"), Request.Form.Get("str"));

            sql = CommonData.DataTableToJson(db.GetDataTable(sql, null));
            if (sql == "")
                sql = "nodata";
            return sql;
        }

        //删除表
        private string deletetable()
        {
            string sql = string.Format("delete from ICGUserTable where tableid='{0}'", Request.Form.Get("id"));
            return db.ExecuteNonQuery(sql, null).ToString();
        }

        //点击一个表获取所有的字段
        private string getonevallist()
        {
            string sql = string.Format("SELECT  tablevalues   from  ICGUserTable where  tableid='{0}'", Request.Form.Get("tableid"));
            DataTable dt = db.GetDataTable(sql, null);
            if (dt == null || dt.Rows.Count == 0)
                return "nodata";
            return dt.Rows[0][0].ToString();
        }

        //获取该项目的所有table
        private string getprotableDT()
        {
            string sql = string.Format("select TABLENAME,TABLEID ,CREATTIME  from  ICGUserTable where proid='{0}' order by CREATTIME desc ", Request.Form.Get("proid"));
            return CommonData.DataTableToJson(db.GetDataTable(sql, null));
        }

        //获取用户的项目列表
        private string getprodt()
        {
            string sql = string.Format("select  PROID, PRONAME, ISPUBLICPRO, PROCEATETIME from ICGPro where userid='{0}' order by PROCEATETIME desc ", Request.Form.Get("userid"));
            DataTable dt = db.GetDataTable(sql, null);
            sql = CommonData.DataTableToJson(dt);
            if (sql == "")
                sql = "nodata";
            return sql;
        }

        //删除项目
        private string delete()
        {
            string sql = string.Format("delete from ICGPro where proid='{0}'", Request.Form.Get("id"));
            db.ExecuteNonQuery(sql, null);
            sql = string.Format("delete from ICGUserTable where proid='{0}'", Request.Form.Get("id"));
            db.ExecuteNonQuery(sql, null);
            return "1";
        }

        //new新项目
        public string NewPro()
        {
            if (Request.Form.Get("userproid") == "-1")
            {
                string proid = Guid.NewGuid().ToString();
                string sql = string.Format("insert into ICGPro(userid,proid,proname,ispublicpro,proceatetime) values (@userid,@proid,@proname,@isp,@proceatetime)");
                SqlParameter[] sqlpara = new SqlParameter[]
                   {
                       new SqlParameter("@userid",Request.Form.Get("userid")),
                       new SqlParameter("@proid",proid),
                       new SqlParameter("@proname",Request.Form.Get("name")),
                       new SqlParameter("@isp",int.Parse(Request.Form.Get("isp"))),
                       new SqlParameter("@proceatetime",DateTime.Now)
                   };
                db.ExecuteNonQuery(sql, sqlpara);
                sql = null;
                sqlpara = null;
                return proid;//guid
            }
            else
            {
                string sql = string.Format("update ICGPro set proname=@proname,ispublicpro=@isp   where proid=@proid");
                SqlParameter[] sqlpara = new SqlParameter[]
                   {
                       new SqlParameter("@proid",Request.Form.Get("userproid")),
                       new SqlParameter("@proname",Request.Form.Get("name")),
                       new SqlParameter("@isp",int.Parse(Request.Form.Get("isp"))),
                       new SqlParameter("@proceatetime",DateTime.Now)
                   };
                return db.ExecuteNonQuery(sql, sqlpara).ToString();
            }
        }
    }
}