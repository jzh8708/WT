﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ICGPage.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="ICGSystemWeb.Main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMaaster" runat="server">
    <div class="sortable row-fluid">
				<a data-rel="tooltip" title="6 new members." class="well span3 top-block" >
					<span class="icon32 icon-red icon-user"></span>
					<div>Total Members</div>
					<div>507</div>
					<span class="notification">6</span>
				</a>

				<a data-rel="tooltip" class="well span3 top-block" id="anewpro">
					<span class="icon32 icon-color icon-star-on"></span>
					<div>创建</div>
					<div>&nbsp;</div>
					<span class="notification green">0</span>
				</a>

				<a data-rel="tooltip" title="$34 new sales." class="well span3 top-block" >
					<span class="icon32 icon-color icon-cart"></span>
					<div>Sales</div>
					<div>$13320</div>
					<span class="notification yellow">$34</span>
				</a>
				
				<a data-rel="tooltip" title="12 new messages." class="well span3 top-block" >
					<span class="icon32 icon-color icon-envelope-closed"></span>
					<div>Messages</div>
					<div>25</div>
					<span class="notification red">12</span>
				</a>
			</div>

    <div class="row-fluid" id="divmyprolist">
				<div class="box span12">
					<div class="box-header well">
						<h2><i class="icon icon-color icon-add"></i> ICG创建项目</h2>
						<div class="box-icon">
							<a  class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a  class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a  class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                        <table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th width="40%">项目名称</th>
								  <th width="10%">项目类型</th>
								  <th width="20%">创建时间</th>
                                  <th width="30%">操作</th>
							  </tr>
						  </thead>
						  <tbody id="tbodymylistpro">

					      </tbody>
					   </table>
                        <div id="newprodivpeoplelistitle" align="left" style="display:none">
					        名称：<input type="text"  id="newprobtntext" />
					        公开：<input  type="checkbox"  id="newprobtncheck"/>
                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="保存" id="btnpronews" class="btn btn-primary"/>
                        </div>
                        <input type="button" id="btnaddnewprodata" value="新建" class="btn btn-primary" />
                  </div>
    			</div>
	</div>		

    <div class="row-fluid" id="divmyprolisttable" style="display:none">
				<div class="box span12">
					<div class="box-header well">
						<h2><i class="icon icon-color icon-add"></i> ICG表</h2>
						<div class="box-icon">
							<a class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table id="tablemyprolisttable" class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th width="60%">表名</th>
								  <th width="20%">更新时间</th>
                                  <th width="20%">操作</th>
							  </tr>
						  </thead>   
						  <tbody id="tbodymylistprotable">
							<tr>
								<td>David R</td>
								<td class="center">2012/01/01</td>
								<td class="center">
									<a class="btn btn-info" >
										<i class="icon-edit icon-white"></i>  
										查看                                           
									</a>
									<a class="btn btn-danger" >
										<i class="icon-trash icon-white"></i> 
										删除
									</a>
								</td>
							</tr>
					     </tbody>
					   </table>
					<input type="button" value="创建新表" id="btnpronewstable" class="btn btn-primary"/>
                    <input type="button" value="返回" id="btnback01" class="btn" />
                  </div>
    			</div>
	</div>
    	
    <div class="row-fluid" id="divcreatenew"  style="display:none">
				<div class="box span12">
					<div class="box-header well">
						<h2><i class="icon icon-color icon-add"></i> ICG管理表(ID ,CreatTime 表中必须有)</h2>
						<div class="box-icon">
							<a  class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a  class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a  class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                    表名：<input type="text" id="txttablename" /><br />
                    <table class="table">
		                <thead>
			                <tr>
				                <th width="2%">ID</th>
				                <th width="15%">字段名称</th>
                                <th width="25%">说明</th>
				                <th width="30%">数据类型</th>
                                <th width="10">查询字段</th>
                                <th width="8%">必填</th>                                  
			                </tr>
		                </thead>   
		                <tbody id="MytbodyContList">
		                <tr>
			                <td>1</td>
			                <td class="center"><input type="text" name="txtname" id="txtname1"  style="width:99px" /></td>
                            <td><input type="text" name="txtdescription" id="txtdescription1" /></td> 
			                <td class="center">
                                <select id="selecttype1"  onchange="changeType(1)"  style="width:99px">
                                    <option myvalue="nvarchar" selected="selected">nvarchar</option>
                                    <option myvalue="int">int</option>
                                    <option myvalue="datetime">datetime</option>
                                    <option myvalue="float">float</option>
                                    <option myvalue="enum">enum</option>
                                </select>
                                <a id="lablea1">长度:<input style="width:40px" type="text" id="datalength1"  value="50" /></a>
                                <a id="lableaenum1" style="display:none"></a>
                                <input type="text" myid='0'  myname=''  value="0"  style='width:100px'  id="selectnum1" />
                            </td>
                            <td class="center">
				                <input type="checkbox" checked="checked" name="mycheckbox"  id="checkboxquerylist1"/>
			                </td>
			                <td class="center">
				                <input type="checkbox" checked="checked" name="mycheckbox"  id="checkboxmust1"/>
			                </td>                            
		                </tr>                      
		                </tbody>
	                </table>
                    <input type="button" value="提交" id="btngo" align="center" class="btn btn-primary" />
                    <input type="button" value="返回" id="btnback02" class="btn" />
                  </div>
    			</div>
	</div>

    <script type="text/javascript" src="js/jquery-1.8.0.js"></script>
    <script type="text/javascript" src="js/bootstrap-modal.js"></script>

    <script type="text/javascript">
        var idarray = []; //count array
        idarray.push(1); //defualt
        var userid = "feifei", tiptime = 2000, userproid = -1, usertableid = -1;
        var userproname = "";

        $(function () {
            //点击名称列(新增行)
            var sid = "-1";
            $("input[name='txtname']").click(function () {
                var id = $(this).attr("id");
                if (sid == id) { return; } //check is clicked
                sid = id;
                id = id.substr(id.length - 1); //get number values
                AddnewRow(id);
            });
            //点击提交
            $("#btngo").click(function () {
                var objarray = [];
                var temps = "";
                var mytid = "", mytname = "", isenum = "";

                objarray.push("ID#ID#nvarchar#36#1###0#0");

                for (var k = 1; k <= idarray.length; k++) {
                    if ($("#txtname" + k).val() == "" || $("#txtname" + k).val() == "ID" || $("#txtname" + k).val() == "CreatTime") {
                        continue;
                    }
                    temps = ""; //must clear data for down code exq
                    temps = $("#txtname" + k).val() + "#" + $("#txtdescription" + k).val() + "#";
                    temps = temps + $("#selecttype" + k).find("option:selected").attr("myvalue") + "#" + $("#datalength" + k).val() + "#";
                    temps = temps + ($("#checkboxmust" + k).attr("checked") == "checked" ? "1" : "0") + "#";
                    //bug begin
                    mytid = $("#selectnum" + k).find("option:selected").attr("myid");
                    if (mytid == "undefined" || mytid == undefined)
                        mytid = "";

                    mytname = $("#selectnum" + k).find("option:selected").attr("myname");
                    if (mytname == "undefined" || mytname == undefined)
                        mytname = "";

                    isenum = $("#selectnum" + k).attr("myenumid"); //获取次字段是否为枚举

                    temps = temps + mytid + "#";
                    temps = temps + mytname + "#";
                    temps = temps + isenum + "#";
                    temps = temps + ($("#checkboxquerylist" + k).attr("checked") == "checked" ? "1" : "0");
                    //bug end
                    objarray.push(temps);
                }
                objarray.push("CreatTime#创建时间#datetime#50#0###0#1");


                if ($("#txttablename").val() == "") {
                    alerttip("请输入表名!");
                    return;
                }
                if (objarray.length < 1) {
                    alerttip("请输入完整的数据项!");
                    return;
                }
                //alert(objarray.toString());
                //return;
                waittips(); //等待中......
                $.post("ajax/MainAjax.aspx?" + new Date().toTimeString(), { "userproname": userproname, "usertableid": usertableid, "tablename": $("#txttablename").val(), "userid": userid, "type": "new", "data": objarray.toString(), "proid": userproid }, function (data) {
                    if (data.length > 0) {
                        var arrayre = data.split('#');
                        usertableid = data[0];
                        asyncbox.tips("成功", 'success', tiptime);
                        window.open("ajax/SQL/" + arrayre[0]);
                    }
                });
            });
            //点击创建按钮显示table创建界面
            $("#anewpro").click(function () {
                $("#divnewedit").show();
            });
            //新项目提交
            $("#btnpronews").click(function () {
                var name = $("#newprobtntext").val();
                if (name == "") {
                    alerttip("请输入项目名称");
                    return;
                }
                var isp = $("#newprobtncheck").attr("checked") == "checked" ? "1" : "0";
                $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userproid": userproid, "userid": userid, "type": "new", "name": name, "isp": isp }, function (data) {
                    $("#newprobtntext").val("");
                    //新增
                    if (userproid == -1) {
                        Addnewprolist(data, name, isp, "刚才");
                        success("新增成功!");
                    }
                    //修改
                    else if (data == "1") {
                        GetproDT();
                        userproid = -1;
                        $("#newprobtncheck").attr("checked", false);
                        success("修改成功");
                    }
                    $("#newprodivpeoplelistitle").hide(); //隐藏面板
                });
            });

            function GetproDT() {
                $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userid": userid, "type": "getprodt" }, function (data) {
                    if (data == "nodata") {
                        return;
                    }
                    var obj = $.parseJSON(data);
                    $("#tbodymylistpro").html("");
                    for (var i = 0; i < obj.length; i++)
                        Addnewprolist(obj[i].PROID, obj[i].PRONAME, obj[i].ISPUBLICPRO, obj[i].PROCEATETIME)
                });
            }

            GetproDT();

            //创建新表
            $("#btnpronewstable").click(function () {
                usertableid = -1;
                idarray = [];
                $("#divmyprolisttable").hide();
                $("#divcreatenew").show();
                $("#txttablename").val("");
                $("#MytbodyContList").html("");
                AddnewRow(0);
            });
            //BACK
            $("#btnback01").click(function () {
                $("#divmyprolist").show();
                $("#divmyprolisttable").hide();
                $("#divcreatenew").hide();
            });
            //back
            $("#btnback02").click(function () {
                $("#divmyprolist").hide();
                $("#divmyprolisttable").show();
                $("#divcreatenew").hide();
                $("#txttablename").val("");
                GetproTable();
            });
            //shin peo
            $("#btnaddnewprodata").click(function () {
                $("#newprodivpeoplelistitle").show();
                $(this).hide();
                userproid = -1;
            });
        });
        //dom end

        //具体实现新の行の定法
        function AddnewRow(newid) {
            newid = parseInt(newid) + 1;
            if (!ckexistd(newid)) {
                $("#MytbodyContList").append("<tr><td>" + newid + "</td><td class='center'><input type='text' onclick='AddnewRow(" + newid + ")'  name='txtname' id='txtname" + newid + "' style='width:99px' /></td><td><input type='text' name='txtdescription' id='txtdescription" + newid + "' /></td><td class='center'><select id='selecttype" + newid + "'  onchange='changeType(" + newid + ")'  style='width:99px'><option myvalue='nvarchar' selected='selected'>nvarchar</option><option myvalue='int'>int</option><option myvalue='datetime'>datetime</option><option myvalue='float'>float</option><option myvalue='enum'>enum</option></select><a id='lablea" + newid + "'>长度:<input style='width:40px' type='text' id='datalength" + newid + "'  value='50' /></a><select  style='width:100px;display:none'   myenumid='0'   id='selectnum" + newid + "'></select></td><td class='center'><input type='checkbox' checked='checked' name='mycheckbox'  id='checkboxquerylist" + newid + "'/></td><td class='center'><input type='checkbox' name='mycheckbox' id='checkboxmust" + newid + "'/></td></tr>");
                idarray.push(newid);
                $("#selecttype" + newid).trigger("liszt:updated");
                $("#selecttype" + newid).chosen();
                $("#checkboxquerylist" + newid).uniform();
                $("#checkboxmust" + newid).uniform();
            }
        }
        //判断是否为第二次点击
        function ckexistd(val) {
            for (var i = (idarray.length - 1); i > -1; i--) {
                if (idarray[i] == val)
                    return true;
            }
            return false;
        }
        //下拉列表点击变换类型
        function changeType(id) {
            var myvalue = $("#selecttype" + id).find("option:selected").attr("myvalue");
            if (myvalue == "nvarchar") {
                $("#lablea" + id).show();
                //$("#selectnum" + id).hide();
                $("#selectnum" + id + "_chzn").hide();
                $("#selectnum" + id).attr("myenumid", "0"); //不是枚举
            }
            else if (myvalue == "enum") {
                //$("#selectnum" + id).show();
                $("#lablea" + id).hide();
                $("#selectnum" + id).attr("myenumid", "1"); //是枚举
                GetUserEnumdata(id); //获取枚举列表
                $("#selectnum" + id + "_chzn").show();//显示chonse
            }
            else {
                $("#lablea" + id).hide();
                //$("#selectnum" + id).hide();
                $("#selectnum" + id).attr("myenumid", "0"); //不是枚举
                $("#selectnum" + id + "_chzn").hide();
            }
        }
        //获取该用户的所有枚举LIST
        var enumobj = null;
        function GetUserEnumdata(nid) {
            if (enumobj == null) {
                $("#selectnum" + nid).html("");
                $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userid": userid, "type": "getenumdata" }, function (data) {
                    if (data == "nodata") {
                        alerttip("您还没有建立枚举数据、请先建立。");
                        return;
                    }
                    var obj = $.parseJSON(data);
                    enumobj = obj;
                    alerttip("当前为您查询出" + obj.length + "条枚举数据，您还可以自定义添加枚举");
                    for (var i = 0; i < obj.length; i++)
                        $("#selectnum" + nid).append("<option myid='" + obj[i].ID + "' myname='" + obj[i].Name + "'>" + obj[i].Name + "</option>");

                    $("#selectnum" + nid).trigger("liszt:updated");
                    $("#selectnum" + nid).chosen(); //変更このさまシ

                    obj = null;
                });
            }
            else {
                $("#selectnum" + nid).html("");
                var obj = enumobj; //$.parseJSON(data);
                alerttip("当前为您查询出" + obj.length + "条枚举数据，您还可以自定义添加枚举,添加后请刷新页面获取才能生效！");
                for (var i = 0; i < obj.length; i++) {
                    $("#selectnum" + nid).append("<option myid='" + obj[i].ID + "' myname='" + obj[i].Name + "'>" + obj[i].Name + "</option>");
                }
                $("#selectnum" + nid).trigger("liszt:updated");
                $("#selectnum" + nid).chosen(); //変更このさまシ
                obj = null;
            }
        }
        //项目列表的具体新增方法
        function Addnewprolist(proid, name, isp, time) {
            $("#tbodymylistpro").append("<tr id='trpro" + proid + "'><td>" + name + "</td><td class='center'>" + (isp == 1 ? "公开" : "私人") + "</td><td class='center'>" + time + "</td><td class='center'><a class='btn btn-success' onclick=viewpro('" + name + "','" + proid + "')><i class='icon-zoom-in icon-white'></i>查看</a>&nbsp;<a class='btn btn-info' onclick=editpro('" + proid + "','" + name + "'," + isp + ")><i class='icon-edit icon-white'></i> 编辑 </a>&nbsp;<a class='btn btn-danger' onclick=deletepro('" + proid + "')><i class='icon-trash icon-white'></i> 删除</a></td></tr>");
        }
        //编辑项目
        function editpro(proid, name, isp) {
            $("#newprodivpeoplelistitle").show(); //显示面板
            $("#btnaddnewprodata").hide();
            userproid = proid;
            $("#newprobtntext").val(name);
            if (isp == 1 || isp == "1")
                $("#newprobtncheck").parent().attr("class", "checked"); //控制样式
            else
                $("#newprobtncheck").parent().attr("class", ""); //控制样式
        }
        //查看项目
        function viewpro(name, proid) {
            userproname = name;
            userproid = proid;
            $("#divmyprolist").hide();
            $("#divmyprolisttable").show();
            GetproTable(); //获取数据列表
        }
        //删除项目
        function deletepro(proid) {
            if (confirm("确定删除该项目吗?") == false)
                return;
            $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userid": userid, "type": "delete", "id": proid }, function (data) {
                if (data == "1") {
                    $("#trpro" + proid).remove();
                    success("删除成功!");
                }
            });
        }
        //获取项目的Table
        function GetproTable() {
            $("#tbodymylistprotable").html("");
            $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userid": userid, "type": "getprotableDT", "proid": userproid }, function (data) {
                if (data == "nodata")
                    return;
                var obj = $.parseJSON(data); //TABLENAME,TABLEID ,CREATTIME 
                for (var k = 0; k < obj.length; k++) {
                    $("#tbodymylistprotable").append("<tr id='trtable" + obj[k].TABLEID + "'><td>" + obj[k].TABLENAME + "</td><td class='center'>" + obj[k].CREATTIME + "</td><td class='center'><a class='btn btn-info' onclick=viewdeletetable('view','" + obj[k].TABLEID + "','" + obj[k].TABLENAME + "')><i class='icon-edit icon-white'></i>查看</a>&nbsp;<a onclick=viewdeletetable('delete','" + obj[k].TABLEID + "','" + obj[k].TABLENAME + "') class='btn btn-danger'><i class='icon-trash icon-white'></i> 删除</a></td></tr>");
                }
            });
        }
        //查看or删除表
        function viewdeletetable(type, id,name) {
            if (type == "view") {
                usertableid = id;
                $("#txttablename").val(name);
                $("#divmyprolisttable").hide();
                $("#divcreatenew").show();
                Getvallist(usertableid);
            }
            else if (type == "delete") {
                if (confirm("确定删除该表记录吗?") == false)
                    return;
                $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userid": userid, "type": "deletetable", "id": id }, function (data) {
                    if (data == "1") {
                        success("删除成功!");
                        GetproTable();
                    }
                });
            }
        }
        //点击一个表获取所有的字段
        function Getvallist(tableid) {
            $("#MytbodyContList").html("");
            idarray = [];//clear data
            $.post("ajax/myajax.aspx?" + new Date().toTimeString(), { "userid": userid, "type": "getonevallist", "tableid": tableid }, function (data) {
                if (data == "nodata")
                    return;
                var obj = $.parseJSON(data); //TABLENAME,TABLEID ,CREATTIME
                var newid = -1;
                for (var k = 0; k < obj.length; k++) {
                    if (obj[k].TNAME == "" || obj[k].TNAME == "undefined")
                        continue;
                    newid = (k + 1);
                    $("#MytbodyContList").append("<tr><td>" + newid + "</td><td class='center'><input type='text' value='" + obj[k].TNAME + "' onclick='AddnewRow(" + newid + ")'  name='txtname' id='txtname" + newid + "' style='width:99px' /></td><td><input type='text' name='txtdescription'  value='" + obj[k].TDESCRIPTION + "' id='txtdescription" + newid + "' /></td><td class='center'><select  data-rel='chosen' id='selecttype" + newid + "'  onchange='changeType(" + newid + ")'  style='width:99px'><option myvalue='nvarchar' selected='selected'>nvarchar</option><option myvalue='int'>int</option><option myvalue='datetime'>datetime</option><option myvalue='float'>float</option><option myvalue='enum'>enum</option></select><a id='lablea" + newid + "'>长度:<input style='width:40px' type='text' value='" + obj[k].TLENGTH + "' id='datalength" + newid + "'  value='" + obj[k].TLENGTH + "' /></a><select  style='width:98px;'   myenumid='" + obj[k].ISENUM + "'   id='selectnum" + newid + "'></select></td><td class='center'><input type='checkbox' name='mycheckbox'  id='checkboxquerylist" + newid + "'/></td><td class='center'><input type='checkbox' name='mycheckbox'  id='checkboxmust" + newid + "'/></td></tr>");

                    $("#selecttype" + newid).val(obj[k].SELECTVAL);

                    if (obj[k].SELECTVAL == "nvarchar") {
                        $("#lablea" + newid).show();
                        $("#selectnum" + newid).hide();
                    }
                    else if (obj[k].ISENUM == "1") {//是枚举
                        $("#lablea" + newid).hide();
                        $("#selectnum" + newid).show(); //显示枚举元素
                        $("#selectnum" + newid).html("");
                        $("#selectnum" + newid).attr("myenumid", "1"); //枚举
                        $("#selectnum" + newid).append("<option myid='" + obj[k].ENUMID + "' myname='" + obj[k].ENUMNAME + "'>" + obj[k].ENUMNAME + "</option>");
                        $("#selectnum" + newid).chosen();
                    }
                    else {
                        $("#lablea" + newid).hide();
                        $("#selectnum" + newid).hide();
                    }

                    if (obj[k].TCHECKBOXVAL == "1") {
                        $("#checkboxmust" + newid).attr("checked", true);
                    }

                    if (obj[k].QUERYCOLUMN == "1") {
                        $("#checkboxquerylist" + newid).attr("checked", true);
                    }

                    $("#selecttype" + newid).trigger("liszt:updated");
                    $("#selecttype" + newid).chosen();

                    $("#checkboxquerylist" + newid).uniform();
                    $("#checkboxmust" + newid).uniform();

                    idarray.push(k + 1);
                }
            });
        }


        /////----------------------common JS-------------------------------------
        //错误
        var timeoutlength = 3000;
        function error(name) {
            $.noty({ "text": name + "</br>" + new Date().toTimeString(), "layout": "topRight", "type": "error", "timeout": timeoutlength });
            return;
            asyncbox.tips(name, 'error', tiptime);
        }
        //成功
        function success(name) {
            $.noty({ "text": name + "</br>" + new Date().toTimeString(), "layout": "topRight", "type": "success", "timeout": timeoutlength });
            return;
            asyncbox.tips(name, 'success', tiptime);
        }
        //消息
        function alerttip(name) {
            $.noty({ "text": name + "</br>" + new Date().toTimeString(), "layout": "topRight", "type": "success", "timeout": timeoutlength });  //aler
            return;
            asyncbox.tips(name, 'alert', tiptime);
        }
        //等待
        function waittips() {
            asyncbox.tips("请稍后...!", 'wait', tiptime * 3);
        }
    </script>
</asp:Content>
