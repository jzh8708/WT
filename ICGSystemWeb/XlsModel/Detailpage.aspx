﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Detailpage.aspx.cs" Inherits="ICGSystemWeb.XlsModel.Detailpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>模版</title>
    <link type="text/css" href="css/lanrenzhijia.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/jquery.quicksand.js" type="text/javascript"></script>
    <script src="js/jquery.easing.js" type="text/javascript"></script>
    <script src="js/lanrenzhijia.js" type="text/javascript"></script>
    <script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="wrapper">
            <div class="portfolio-content">
                <br /><br />
                <a style="font-size:28px; cursor:none">
                    模版名称：<%=xlsname %> 
                </a><br /><br /><br /><br />
                <p></p>
                <h3>介绍：<%=xlsshougai%></h3>
                <br />
                <h4>下载所需金额：<%=needmoney %>元  &nbsp;&nbsp;&nbsp; 下载次数:<%=downloadcount %> 次</h4>
                <br />
                <input type="button" value="下载" id="btndownload" onclick="window.open('download.aspx?ID=<%=id %>')" />
                <br /> <br />
                <ul class="portfolio-area">
                    <asp:Repeater runat="server" ID="modeldatalist">
                        <ItemTemplate>
                            <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
                                <div>
                                    <span class="image-block">
                                        <a class="image-zoom" href="<%#Eval("filepath") %>" rel="prettyPhoto[gallery]">
                                            <img width="225" height="140" src="<%#Eval("filepath") %>" />
                                        </a>
                                    </span>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="column-clear">
                    </div>
                </ul>
            </div>
        </div>
    </div>
    </form>
    <br /><br /><br />
    <div align="center">
     <a  style="font-size:18px" href="XlsList.aspx">返回列表</a>
    </div>
    <br /><br />
</body>
</html>
