﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using DBS;
using System.Text;

namespace ICGSystemWeb.XlsModel
{
    public partial class download : System.Web.UI.Page
    {
        private DBHelper db = new DBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["ID"];

            if (id == null || id == "") return;

            string sql = string.Format("select filepath,needmoney from xlstable   where ID='{0}'", id);

            DataTable dt = db.GetDataTable(sql);

            //余额不足
            if (CommonData.Pay(double.Parse(dt.Rows[0]["needmoney"].ToString())) == "-1")
            {
                CommonData.AddLog("购买了模版，余额不足",0,0);//日志
                Response.Write("余额不足");
                Response.End();
                return;//余额不足
            }

            CommonData.AddLog("购买下载了模版", Convert.ToDouble(double.Parse(dt.Rows[0]["needmoney"].ToString())),0);//日志

            string xlspath = dt.Rows[0]["filepath"].ToString();//名称

            dt = null;

            string sFileName = Server.MapPath(xlspath);

            FileStream fileStream = new FileStream(sFileName, FileMode.Open);

            long fileSize = fileStream.Length;

            byte[] fileBuffer = new byte[fileSize];

            fileStream.Read(fileBuffer, 0, (int)fileSize);

            //如果不写fileStream.Close()语句，用户在下载过程中选择取消，将不能再次下载
            fileStream.Close();

            Context.Response.ContentType = "application/octet-stream";
            Context.Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(sFileName, Encoding.UTF8));
            Context.Response.AddHeader("Content-Length", fileSize.ToString());

            Context.Response.BinaryWrite(fileBuffer);
            Context.Response.End();
            Context.Response.Close();
        }
    }
}