﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using DBS;


namespace ICGSystemWeb.XlsModel
{
    public partial class XlsList : System.Web.UI.Page
    {
        DBHelper db = new DBHelper();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Getdata();
            }
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        private void Getdata()
        {
            string sql = string.Format("select * from xlstable order by uploadtime desc ");
            DataTable dt = db.GetDataTable(sql);
            modeldatalist.DataSource = dt;
            modeldatalist.DataBind();
        }
    }
}