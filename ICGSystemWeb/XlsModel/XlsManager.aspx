﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="XlsManager.aspx.cs" Inherits="ICGSystemWeb.XlsModel.XlsManager" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>模版管理系统</title>
    <!-- The styles -->
    <link href="../css/bootstrap-cerulean.css" rel="stylesheet" />
    <link type="text/css" href="../css/bootstrap-responsive.css" rel="stylesheet" />
    <link type="text/css" href="../css/charisma-app.css" rel="stylesheet" />
    <link type="text/css" href="../css/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
    <link type="text/css" href='../css/fullcalendar.css' rel='stylesheet' />
    <link type="text/css" href='../css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link type="text/css" href='../css/chosen.css' rel='stylesheet' />
    <link type="text/css" href='../css/uniform.default.css' rel='stylesheet' />
    <link type="text/css" href='../css/colorbox.css' rel='stylesheet' />
    <link type="text/css" href='../css/jquery.cleditor.css' rel='stylesheet' />
    <link type="text/css" href='../css/jquery.noty.css' rel='stylesheet' />
    <link type="text/css" href='../css/noty_theme_default.css' rel='stylesheet' />
    <link type="text/css" href='../css/elfinder.min.css' rel='stylesheet' />
    <link type="text/css" href='../css/elfinder.theme.css' rel='stylesheet' />
    <link type="text/css" href='../css/jquery.iphone.toggle.css' rel='stylesheet' />
    <link type="text/css" href='../css/opa-icons.css' rel='stylesheet' />
    <link type="text/css" href='../css/uploadify.css' rel='stylesheet' />
    <link href="../js/asyncbox/skins/ZCMS/asyncbox.css" type="text/css" rel="stylesheet" />
    <link href="../Script/validationEngine/css/validationEngine.jquery.css" rel="stylesheet"
        type="text/css" />
    <!--jquery and javascript begin-->
    <script type="text/javascript" src="../js/jquery-1.8.0.js"></script>
    <script language="javascript" type="text/javascript" src="../Script/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src='../Script/validationEngine/languages/jquery.validationEngine-zh_CN.js'></script>
    <script type="text/javascript" src='../Script/validationEngine/jquery.validationEngine.js'></script>
    <script type="text/javascript" src="../js/asyncbox/AsyncBox.v1.4.5.js"></script>
    <script type="text/javascript" src="../Script/ajax_upload.js"></script>
    <script type="text/javascript">
        var imgarray = [];
        var xlspath = "", pagesize = 0, endtime = "", filesize = 0;
        $(function () {

            jQuery("#divadd").validationEngine(); //验证
            //新增
            $("#btnadd").click(function () {
                userfilesmanagID = -1;
                $("#divadd").show();
                $("#divlist").hide();
            });
            //取消新增
            $("#btncancel").click(function () {
                $("#divadd").hide();
                $("#divlist").show();
            });

            // 文件上传 ------------------------- begin(华丽的分隔符) ---------------------------
            var button = $('#btnaddfile'), interval;
            new AjaxUpload(button, {
                action: 'FileXLs.ashx',
                name: 'imgFile',
                onSubmit: function (file, ext) {
                    this.disable();
                    interval = window.setInterval(function () {
                        var text = button.text();
                    }, 200);
                },
                onComplete: function (file, response) {
                    window.clearInterval(interval);
                    this.enable();
                    response = response.replace(/<pre>/ig, "").replace(/<\/pre>/ig, ""); //过滤
                    var obj = $.parseJSON(response);
                    if (obj[0].filetruename == "ferror") { alert("该文件类型不允许上传!"); return false; }
                    if (obj[0].filetruename == "big") { alert("文件过大!"); return false; }

                    xlspath = obj[0].filepath;

                    $("#xlsname").html(obj[0].filetruename); //显示xls的文件路径

                    filesize = obj[0].filesize;
                }
            });

            // 文件上传  img 
            var button = $('#btxlsimg'), interval;
            new AjaxUpload(button, {
                action: 'FileXLsIMG.ashx',
                name: 'imgFile',
                onSubmit: function (file, ext) {
                    this.disable();
                    interval = window.setInterval(function () {
                        var text = button.text();
                    }, 200);
                },
                onComplete: function (file, response) {
                    window.clearInterval(interval);
                    this.enable();
                    response = response.replace(/<pre>/ig, "").replace(/<\/pre>/ig, ""); //过滤
                    var obj = $.parseJSON(response);
                    if (obj[0].filetruename == "ferror") { alert("该文件类型不允许上传!"); return false; }
                    if (obj[0].filetruename == "big") { alert("文件过大!"); return false; }

                    imgarray.push(obj[0].filepath); //路径和大小放入数组中

                    $("#xlsimgname").append("<img  src='" + obj[0].filepath + "' /><br/>");
                }
            });


            //保存方法
            $("#btnsave").click(function () {
                if (!jQuery("#divadd").validationEngine("validate")) {
                    return false;
                }
                var temparray = [];
                var para = {
                    "type": "add",
                    "txtxlsname": $("#txtname").val(),
                    "txtshougai": $("#txtshougai").val(),
                    "txtneedmoney": $("#txtneedmoney").val(),
                    "filesize": filesize,
                    "txtneedcount": "0",
                    "txtpertocount": $("#txtpertocount").val(),
                    "imgarray": imgarray.toString(),
                    "xlspath": xlspath
                };

                $.post("ajax.aspx?" + new Date().toTimeString(), para, function (data) {
                    endtime = ""; //时间默认值
                    userfilesmanagID = -1;
                    if (data.length == 36) {
                        success("保存成功！");
                        temparray = null;
                    }
                    $("#divadd").hide();
                    $("#divlist").show();
                    getlist(); //获取前20条
                });
            });
            //页面第一次加载进行渲染
            function load() {
                var obj = $("select[name='SLData']");
                $(obj).each(function () {
                    $(this).chosen();
                });
            }
            //加载页面渲染
            load();
            getlist();
            $("#btnmore").click(function () {
                getlist();
            });
        })//dom end
        //getone
        function getone(id) {
            userfilesmanagID = id;
            $("#divadd").show();
            $("#divlist").hide();

            $.post("ajax.aspx?" + new Date().toTimeString(), { "type": "getone", "id": id }, function (data) {
                var obj = $.parseJSON(data);
                for (var s = 0; s < myidarray.length; s++) {
                    if (myidarray[s] == "filename") {
                        $("input[myid='" + myidarray[s] + "']").val(obj[0][myidarray[s]].substr(0, obj[0][myidarray[s]].indexOf(".")));
                        $("#bnametype").html(obj[0][myidarray[s]].substr(obj[0][myidarray[s]].indexOf(".")));
                        continue;
                    }
                    $("input[myid='" + myidarray[s] + "']").val(obj[0][myidarray[s]]);
                }
            });

        } //获取10行数据
        function getlist() {
            $.post("ajax.aspx?" + new Date().toTimeString(), { "type": "getlist", "pagesize": pagesize, "endtime": endtime }, function (data) {
                if (data == "nodata") {
                    alerttip("无数据、您可以新增数据！");
                    $("#btnmore").hide();
                }
                var obj = $.parseJSON(data);

                if (obj.length < pagesize) { $("#btnmore").hide(); }

                if (endtime == "")
                    $("#tbodydatalist").html("");

                for (var i = 0; i < obj.length; i++) {
                    $("#tbodydatalist").append("<tr id='tr" + obj[i].ID + "'><td class='center'>" + obj[i].xlsname.replace(" 0:00:00", "") + "</td><td class='center'>" + "<img style='width:32px;height:32px' src=" + obj[i].xlstopimgpath + " /></td><td class='center'>" + obj[i].uploadtime.replace(" 0:00:00", "") + "</td><td class='center'><a class='btn btn-info' onclick=domains('edit','" + obj[i].ID + "')><i class='icon-edit icon-white'></i>修改</a>&nbsp;<a class='btn btn-danger' onclick=domains('delete','" + obj[i].ID + "')><i class='icon-trash icon-white'></i> 删除</a></td></tr>");
                    if (i == (obj.length - 1)) {
                        endtime = obj[i].uploaduername;
                    }
                }
            })
        } //domain

        function domains(type, id) {
            if (type == "delete") {
                if (confirm("确定删除吗?") == false)
                    return;
                $.post("ajax.aspx?" + new Date().toTimeString(), { "type": "delete", "id": id, "userid": userid }, function (data) {
                    if (data == "0") {
                        error("您没有权限删除此条数据");
                        return;
                    }
                    else if (data == "1") {
                        $("#tr" + id).remove();
                        success("删除成功!");
                    }
                });
            }
            else if (type == "view") {
                SLarray = [];
                getone(id);
            }
        }

        /////----------------------common JS-------------------------------------
        //错误
        var timeoutlength = 3000;
        function error(name) {
            $.noty({ "text": name + "</br>" + new Date().toTimeString(), "layout": "topRight", "type": "error", "timeout": timeoutlength });
            return;
            asyncbox.tips(name, 'error', tiptime);
        }
        //成功
        function success(name) {
            $.noty({ "text": name + "</br>" + new Date().toTimeString(), "layout": "topRight", "type": "success", "timeout": timeoutlength });
            return;
            asyncbox.tips(name, 'success', tiptime);
        }
        //消息
        function alerttip(name) {
            $.noty({ "text": name + "</br>" + new Date().toTimeString(), "layout": "topRight", "type": "success", "timeout": timeoutlength });  //aler
            return;
            asyncbox.tips(name, 'alert', tiptime);
        }
        //等待
        function waittips() {
            asyncbox.tips("请稍后...!", 'wait', tiptime * 3);
        }
    </script>
</head>
<body>
    <!-- content starts -->
    <div class="row-fluid sortable" id="divadd" style="display: none">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2>
                    <i class="icon-edit"></i>新增数据</h2>
                <div class="box-icon">
                    <a href="../#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i>
                    </a><a href="../#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal">
                <fieldset>
                    <div class="control-group success">
                        <label class="control-label" for="inputSuccess">
                            名称：</label>
                        <div class="controls">
                            <input class="validate[required,maxSize[300],custom[noSpecialCaracters]]" id="txtname"
                                type="text" />
                        </div>
                    </div>
                    <div class="control-group success">
                        <label class="control-label" for="inputSuccess">
                            介绍：</label>
                        <div class="controls">
                            <input class="validate[required,maxSize[36],custom[noSpecialCaracters]]" id="txtshougai"
                                type="text" />
                        </div>
                    </div>
                    <div class="control-group success">
                        <label class="control-label" for="inputSuccess">
                            下载所需金额：</label>
                        <div class="controls">
                            <input class="validate[required,maxSize[200],custom[noSpecialCaracters]]" id="txtneedmoney"
                                type="text" /><b id="bnametype"></b>
                        </div>
                    </div>
                    <div class="control-group success">
                        <label class="control-label" for="inputSuccess">
                            查看一次多少积分：</label>
                        <div class="controls">
                            <input class="validate[required,custom[number]]" id="txtpertocount" type="text" />
                        </div>
                    </div>
                    <div class="control-group success">
                        <label class="control-label" for="inputSuccess">
                            请选择文件</label>
                        <div class="controls" align="left">
                            <a id="xlsname"></a><span class="icon32 icon-color icon-link" id="btnaddfile"></span>
                            上传
                        </div>
                    </div>
                    <div class="control-group success">
                        <label class="control-label" for="inputSuccess">
                            请选择展示图片</label>
                        <div class="controls" align="left">
                            <a id="xlsimgname"></a><span class="icon32 icon-color icon-link" id="btxlsimg"></span>
                            上传
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" class="btn btn-primary" id="btnsave">
                            保存</button>
                        <input type="button" class="btn" value="取消" id="btncancel" />
                    </div>
                </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="row-fluid sortable" id="divlist">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <i class="icon-edit"></i>数据列表</h2>
                <div class="box-icon">
                    <a href="../#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i>
                    </a><a href="../#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div align="center">
                    <span class="icon32 icon-color icon-add" id="btnadd"></span>
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>
                                <th>
                                    名称
                                </th>
                                <th>
                                    图标
                                </th>
                                <th>
                                    更新时间
                                </th>
                                <th width="18%">
                                    操作
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbodydatalist">
                        </tbody>
                    </table>
                    <span class="icon32 icon-color icon-arrowthick-s" id="btnmore"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery UI -->
    <script src="../js/jquery-ui-1.8.21.custom.min.js"></script>
    <!-- transition / effect library -->
    <script src="../js/bootstrap-transition.js"></script>
    <!-- alert enhancer library -->
    <script src="../js/bootstrap-alert.js"></script>
    <!-- modal / dialog library -->
    <script src="../js/bootstrap-modal.js"></script>
    <!-- custom dropdown library -->
    <script src="../js/bootstrap-dropdown.js"></script>
    <!-- scrolspy library -->
    <script src="../js/bootstrap-scrollspy.js"></script>
    <!-- library for creating tabs -->
    <script src="../js/bootstrap-tab.js"></script>
    <!-- library for advanced tooltip -->
    <script src="../js/bootstrap-tooltip.js"></script>
    <!-- popover effect library -->
    <script src="../js/bootstrap-popover.js"></script>
    <!-- button enhancer library -->
    <script src="../js/bootstrap-button.js"></script>
    <!-- accordion library (optional, not used in demo) -->
    <script src="../js/bootstrap-collapse.js"></script>
    <!-- carousel slideshow library (optional, not used in demo) -->
    <script src="../js/bootstrap-carousel.js"></script>
    <!-- autocomplete library -->
    <script src="../js/bootstrap-typeahead.js"></script>
    <!-- tour library -->
    <script src="../js/bootstrap-tour.js"></script>
    <!-- library for cookie management -->
    <script src="../js/jquery.cookie.js"></script>
    <!-- calander plugin -->
    <script src="../js/fullcalendar.min.js"></script>
    <!-- data table plugin -->
    <script src="../js/jquery.dataTables.min.js"></script>
    <!-- select or dropdown enhancer -->
    <script src="../js/jquery.chosen.min.js"></script>
    <!-- checkbox, radio, and file input styler -->
    <script src="../js/jquery.uniform.min.js"></script>
    <!-- plugin for gallery image view -->
    <script src="../js/jquery.colorbox.min.js"></script>
    <!-- rich text editor library -->
    <script src="../js/jquery.cleditor.min.js"></script>
    <!-- notification plugin -->
    <script src="../js/jquery.noty.js"></script>
    <!-- file manager library -->
    <script src="../js/jquery.elfinder.min.js"></script>
    <!-- star rating plugin -->
    <script src="../js/jquery.raty.min.js"></script>
    <!-- for iOS style toggle switch -->
    <script src="../js/jquery.iphone.toggle.js"></script>
    <!-- autogrowing textarea plugin -->
    <script src="../js/jquery.autogrow-textarea.js"></script>
    <!-- multiple file upload plugin -->
    <script src="../js/jquery.uploadify-3.1.min.js"></script>
    <!-- history.js for cross-browser state change on ajax -->
    <script src="../js/jquery.history.js"></script>
    <!-- application script for Charisma demo -->
    <script src="../js/charisma.js"></script>
</body>
</html>
