﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VideoList.aspx.cs" Inherits="ICGSystemWeb.XlsModel.VideoList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>视频</title>
    <link type="text/css" href="css/lanrenzhijia.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/jquery.quicksand.js" type="text/javascript"></script>
    <script src="js/jquery.easing.js" type="text/javascript"></script>
    <script src="js/lanrenzhijia.js" type="text/javascript"></script>
    <script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="wrapper">
            <div class="portfolio-content">
                <ul class="portfolio-area">
                    <asp:Repeater runat="server" ID="modeldatalist">
                        <ItemTemplate>
                            <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
                                <div>
                                    <span class="image-block"><a class="image-zoom" href="../video/Play.aspx?ID=<%#Eval("guid") %>"
                                         
                                        title="<%#Eval("title") %>">
                                        <img width="225" height="140" src="../userajax/<%#Eval("poster") %>" alt="<%#Eval("title") %>"
                                            title="<%#Eval("title") %>" />
                                    </a></span>
                                    <div class="home-portfolio-text">
                                        <h2 class="post-title-portfolio">
                                            <a href="../video/Play.aspx?ID=<%#Eval("guid") %>" rel="bookmark" title="<%#Eval("title") %>">
                                                <%#Eval("title")%></a></h2>
                                        <p class="post-subtitle-portfolio">
                                            下载次数: 0</p>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="column-clear">
                    </div>
                </ul>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
