﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace ICGSystemWeb.userajax
{
    /// <summary>
    /// Summary description for FileMail
    /// </summary>
    public class FileXLsIMG : IHttpHandler
    {
        private const int maxlength = 209715200;

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count == 0)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("No files received.");
            }
            else
            {
                context.Response.ContentType = "text/plain";
                HttpPostedFile uploadedfile = context.Request.Files[0];

                string FileName = GetfilenameLaster(uploadedfile.FileName);
                string type = GetFileLasterString(FileName);
                string FileType = uploadedfile.ContentType;
                int FileSize = uploadedfile.ContentLength;

                if (type == "error")
                {
                    context.Response.Write(GetDtToJson("ferror", "ferror", "ferror", "ferror"));
                    context.Response.End();
                    return;
                }

                string uploadpath = "";
                string configstr = System.Configuration.ConfigurationManager.AppSettings["MailFilesPath"] + DateTime.Now.Year + "/" + DateTime.Now.Month + "/";
                uploadpath = HttpContext.Current.Server.MapPath(configstr);


                //没有文件路径则创建路径
                if (!System.IO.Directory.Exists(uploadpath))
                    System.IO.Directory.CreateDirectory(uploadpath);

                string trueFileName = Guid.NewGuid().ToString().Replace("-", "f") + FileName;
                string fullpathname = uploadpath + trueFileName;//服务器绝对路径
                try
                {
                    uploadedfile.SaveAs(fullpathname);
                }
                catch
                {
                    context.Response.Write("17"); return;//失败
                }

                trueFileName = GetDtToJson(FileName, configstr + trueFileName, FileSize.ToString(), GetFiletypeByname(FileName));//json formate output
                context.Response.Write(trueFileName);
                context.Response.End();
            }
        }


        /// <summary>
        /// 根据附件产生table返回json字符串
        /// </summary>
        /// <param name="filetruename">文件名称</param>
        /// <param name="filepath">文件路径</param>
        /// <param name="filesize">文件大小</param>
        /// <param name="type">类型</param>
        /// <returns>string for json</returns>
        private string GetDtToJson(string filetruename, string filepath, string filesize, string type)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("filetruename", typeof(string));
            dt.Columns.Add("filepath", typeof(string));
            dt.Columns.Add("filesize", typeof(string));
            dt.Columns.Add("type", typeof(string));
            DataRow dw = dt.NewRow();
            object[] array = new object[] { filetruename, filepath, filesize, type };
            dw.ItemArray = array;
            array = null;
            dt.Rows.Add(dw);
            dw = null;
            return CommonData.DataTableToJson(dt);
        }

        /// <summary>
        /// 获取文件类型
        /// </summary>
        /// <param name="filename">文件名称</param>
        /// <returns></returns>
        private string GetFiletypeByname(string filename)
        {
            return filename.Substring(filename.LastIndexOf('.') + 1);
        }

        /// <summary>
        /// 获取文件上传后缀名
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetfilenameLaster(string filename)
        {
            int index = filename.LastIndexOf("\\");
            if (index > 0)
            {
                index += 1;
                return filename.Substring(index);
            }
            return filename;
        }

        //获取文件类型是否合法
        private string GetFileLasterString(string filename)
        {
            string resultstring = "";
            string str = filename.Substring(filename.LastIndexOf(".") + 1);
            switch (str)
            {
                case "exe":
                    resultstring = "error";
                    break;
                case "sql":
                    resultstring = "error";
                    break;
                case "dat":
                    resultstring = "error";
                    break;
                case "config":
                    resultstring = "error";
                    break;
                case "bin":
                    resultstring = "error";
                    break;
                case "aspx":
                    resultstring = "error";
                    break;
                case "cs":
                    resultstring = "error";
                    break;
                case "java":
                    resultstring = "error";
                    break;
                case "class":
                    resultstring = "error";
                    break;
                case "php":
                    resultstring = "error";
                    break;
                case "asp":
                    resultstring = "error";
                    break;
                default:
                    resultstring = "";
                    break;
            }
            return resultstring;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}