﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DBS;
using System.Data.SqlClient;

namespace ICGSystemWeb.XlsModel
{
    public partial class ajax : System.Web.UI.Page
    {

        private DBHelper db = new DBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //新增
            if (type == "add")
                type = Add();
            //查询
            else if (type == "getlist")
                type = getlist();

            Response.Write(type);
            Response.End();

        }


        /// <summary>
        /// 查询列表
        /// </summary>
        /// <returns></returns>
        private string getlist()
        {
            string sql = @"SELECT    [ID]
                  ,[type]
                  ,[xlsname]
                  ,[xlsshougai]
                  ,[xlstopimgpath]
                  ,[needcount]
                  ,[tocount]
                  ,[needmoney]
                  ,[downloadcount]
                  ,[filepath]
                  ,[filesize]
                  ,[uploadtime]
                  ,[uploaduserid]
                  ,[uploaduername]
              FROM xlstable ";
            return CommonData.DataTableToJson(db.GetDataTable(sql));
        }


        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        private string Add()
        {
            string img = Request.Form.Get("imgarray");
            string[] imgarray = null;

            string guid = Guid.NewGuid().ToString();

            if (img != null && img != "")
                imgarray = img.Split(',');

            string sql = string.Format("insert into xlstable([ID],[xlsname],[xlsshougai],[xlstopimgpath],[tocount],[needmoney],[filepath],[filesize],[uploaduserid],[uploaduername]) values (@ID,@xlsname,@xlsshougai,@xlstopimgpath,@tocount,@needmoney,@filepath,@filesize,@uploaduserid,@uploaduername)");
            SqlParameter[] para = new SqlParameter[]{
            new SqlParameter("@ID",guid),
            new SqlParameter("@xlsname",Request.Form.Get("txtxlsname")),
            new SqlParameter("@xlsshougai",Request.Form.Get("txtshougai")),
            new SqlParameter("@tocount",Request.Form.Get("txtpertocount")),
            new SqlParameter("@needmoney",Request.Form.Get("txtneedmoney")),
            new SqlParameter("@filepath",Request.Form.Get("xlspath")),
            new SqlParameter("@filesize",Request.Form.Get("filesize")),
            new SqlParameter("@uploaduserid",CommonData.GetUserID()),
            new SqlParameter("@xlstopimgpath",imgarray[0]),
            new SqlParameter("@uploaduername",CommonData.GetLoginUserName())
            };

            db.ExecuteNonQuery(sql, para);

            sql = null;

            if (imgarray != null)
            {
                for (int i = 0; i < imgarray.Length; i++)
                {
                    sql = string.Format("insert into xlsimg([ID],[xlsID],[filepath]) values ('{0}','{1}','{2}')", Guid.NewGuid().ToString(), guid, imgarray[i]);
                    db.ExecuteNonQuery(sql);
                }
            }

            sql = null;

            return guid;
        }
    }
}