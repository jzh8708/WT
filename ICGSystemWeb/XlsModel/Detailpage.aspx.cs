﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data.SqlClient;
using System.Data;
using DBS;



namespace ICGSystemWeb.XlsModel
{
    public partial class Detailpage : System.Web.UI.Page
    {
        private DBHelper db = new DBHelper();

        public string xlsname = "";

        public string xlsshougai = "";

        public string needmoney = "";

        public string downloadcount = "";

        public string id = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Getdata();
                AddCount();
            }
        }

        /// <summary>
        /// 加载数据
        /// </summary>
        private void Getdata()
        {
            id = Request.QueryString["ID"];
            if (id == null || id == "") return;


            string sql = string.Format("select * from xlstable   where ID='{0}'", id);

            DataTable dt = db.GetDataTable(sql);

            xlsname = dt.Rows[0]["xlsname"].ToString();//名称
            xlsshougai = dt.Rows[0]["xlsshougai"].ToString();
            needmoney = dt.Rows[0]["needmoney"].ToString();
            downloadcount = dt.Rows[0]["downloadcount"].ToString();

            sql = string.Format("select filepath  from xlsimg where xlsID='{0}' order by uploadtime desc", id);
            dt = db.GetDataTable(sql);

            modeldatalist.DataSource = dt;
            modeldatalist.DataBind();

            dt = null;
        }

        private void AddCount()
        {
            string sql = string.Format("update usertable set usercount+=(select tocount from xlstable where ID='{0}') where userid='{1}'", id, CommonData.GetUserID());
            db.ExecuteNonQuery(sql);
            return;
        }
    }
}