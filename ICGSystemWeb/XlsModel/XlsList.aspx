﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="XlsList.aspx.cs" Inherits="ICGSystemWeb.XlsModel.XlsList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>模版</title>
    <link type="text/css" href="css/lanrenzhijia.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="js/jquery.quicksand.js" type="text/javascript"></script>
    <script src="js/jquery.easing.js" type="text/javascript"></script>
    <script src="js/lanrenzhijia.js" type="text/javascript"></script>
    <script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <div class="wrapper">
            <div class="portfolio-content">
                <ul class="portfolio-area">
                    <asp:Repeater runat="server" ID="modeldatalist">
                        <ItemTemplate>
                            <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">
                                <div>
                                    <span class="image-block"><a class="image-zoom"  href="Detailpage.aspx?ID=<%#Eval("ID") %>" href="<%#Eval("xlstopimgpath") %>"
                                        title="<%#Eval("xlsname") %>">
                                        <img width="225" height="140" src="<%#Eval("xlstopimgpath") %>" alt="<%#Eval("xlsname") %>"
                                            title="<%#Eval("xlsname") %>" />
                                    </a></span>
                                    <div class="home-portfolio-text">
                                        <h2 class="post-title-portfolio">
                                            <a href="Detailpage.aspx?ID=<%#Eval("ID") %>" rel="bookmark" title="<%#Eval("xlsname") %>">
                                                <%#Eval("xlsname") %></a></h2>
                                        <p class="post-subtitle-portfolio">
                                            下载次数:
                                            <%#Eval("downloadcount")%></p>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="column-clear">
                    </div>
                </ul>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
