using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
using MyDAL;
using System.Web;

namespace ICGSystemWeb
{
    //ajax底层处理代码
    public partial class login : System.Web.UI.Page
    {
        //实例化
        DBHelper DB = new DBHelper();
        usertableOP DAL = new usertableOP();
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //获取对应的枚举项
            if (type == "getenumitembyid")
                type = GetEnumItemData(Request.Form.Get("myenumid"), Request.Form.Get("userid"));
            //获取用户定义数量的数据列表
            else if (type == "getlist")
                type = GetTopNumberData();
            //获取一天修改数据
            else if (type == "getone")
                type = getone(Request.Form.Get("id").Trim());
            //删除
            else if (type == "delete")
                type = delete(Request.Form.Get("id"), Request.Form.Get("userid"));

            else if (type == "login")
            {
                type = GetLoginResult(Request.Form.Get("name"), HttpUtility.UrlDecode(Request.Form.Get("pass")));
            }

            Response.Write(type);
            Response.End();
        }

        /// <summary>
        /// 外网用户登录
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        private string GetLoginResult(string name, string pass)
        {
            string sql = string.Format("select  usertype,userid,isallowlogin  from usertable where username=@username and userpass=@userpass ");

            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@userpass", pass), new SqlParameter("@username", name) };

            DataTable dt = DB.GetDataTable(sql, para);

            para = null;

            sql = null;

            if (dt == null || dt.Rows.Count == 0)
                return "-100";//登录失败

            if (dt.Rows[0]["isallowlogin"].ToString() == "0")
                return "99";//不允许登录

            CommonData.SetCookie(CommonData.usercode, dt.Rows[0]["userid"].ToString());

            CommonData.AddLog("用户登录:" + name);

            return dt.Rows[0]["usertype"].ToString();//用户类型
        }


        //删除
        private string delete(string id, string userid)
        {
            return DAL.DeleteDB(id);
        }

        //获取一个
        private string getone(string id)
        {
            id = CommonData.DataTableToJson(DAL.GetOneDataByGuid(id));
            id = id == "" ? "nodata" : id;
            return id;
        }

        //获取用户定义数量的数据列表
        private string GetTopNumberData()
        {
            string endtime = Request.Form.Get("endtime");
            if (endtime == "") endtime = DateTime.Now.AddDays(1).ToString();
            endtime = CommonData.DataTableToJson(DAL.GetTopNumberData(int.Parse(Request.Form.Get("pagesize")), endtime));
            if (endtime == "")
                endtime = "nodata";
            return endtime;
        }

        //获取枚举项BY ID
        private string GetEnumItemData(string enumid, string userid)
        {
            if (enumid == null || enumid == "" || userid == null || userid == "") return CommonData.nodata;

            string sql = string.Format("select  NAME,VAL from ICGEnumItemTab where enumid='{0}' and userid='{1}' ", enumid, userid);
            sql = CommonData.DataTableToJson(DB.GetDataTable(sql, null));

            if (sql == "") sql = CommonData.nodata;
            return sql;
        }

    }
}