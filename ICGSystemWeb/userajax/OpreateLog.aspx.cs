﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBS;

namespace ICGSystemWeb
{
    /// <summary>
    /// <summary>Ajax处理页面(用户日志)</summary>
    /// <creator>陈飞</creator>
    /// <createdate>2013-08-07</createdate>
    /// <modifier></modifier>
    /// <modifynote></modifynote>
    /// <modifydate></modifydate>
    /// <other></other>
    /// </summary>
    public partial class OpreateLog : System.Web.UI.Page
    {

        private DBHelper db = new DBHelper();

        /// <summary>
        /// 获取userid from Session
        /// </summary>
        /// <returns></returns>
        private string GetSession()
        {
            string userid = CommonData.GetUserID();

            if (userid == null) return null;

            return userid;
        }

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            //非法请求Begin
            if (GetSession() == null || GetSession() == "")
            {
                Response.Write("-999"); Response.End();
            }
            //非法请求End

            object obj = Request.Form.Get("type");

            if (obj == null || obj.ToString() == "") { Response.Write("error"); Response.End(); }

            string type = obj.ToString();

            switch (type)
            {   //获取数据表
                case "Get":
                    type = GetDT(Request.Form.Get("toptime"));
                    break;
                //时间段搜索
                case "Searchdate":
                    type = GetDTCOM(Request.Form.Get("d1"), Request.Form.Get("d2"));
                    break;
                //多条删除
                case "delist":
                    type = Delete(Request.Form.Get("id"));
                    break;
                case "de":
                    type = Delete(Request.Form.Get("id"));
                    break;
                //获取日志总数
                case "Getlogcount":
                    type = Getlogcount();
                    break;
            }

            Response.Write(type);
            Response.End();

        }


        /// <summary>
        /// 查询数据日志总数
        /// </summary>
        /// <returns></returns>
        private string Getlogcount()
        {
            string type = CommonData.GetUserTypeByUserID();

            string sql = "";

            //外网
            if (type == "0")
                sql = string.Format("select count(0)  from TAB_OPERATELOG where USERID='{0}'", CommonData.GetUserID());
            else
                sql = string.Format("select count(0)  from TAB_OPERATELOG ", CommonData.GetUserID());

            return db.ExecuteSingleString(sql);
        }


        /// <summary>
        /// 按照时间搜索
        /// </summary>
        /// <param name="d1">时间1</param>
        /// <param name="d2">时间2</param>
        /// <returns></returns>
        private string GetDTCOM(string  t1, string  t2)
        {
            string type = CommonData.GetUserTypeByUserID();

            string sql = "";

            //外网
            if (type == "0")
                sql = string.Format("select   GUID,OPREATETIME,ORGNAME,CONTENT,OTHERUSE  from TAB_OPERATELOG where USERID='{0}' and OPREATETIME>'{1}' and  OPREATETIME<'{2}' order by OPREATETIME desc ", CommonData.GetUserID(), t1, t2);
            else
                sql = string.Format("select   GUID,OPREATETIME,ORGNAME,CONTENT,OTHERUSE  from TAB_OPERATELOG where OPREATETIME>'{0}' and OPREATETIME<'{1}'  order by OPREATETIME desc", t1, t2);

            string result = CommonData.DataTableToJson(db.GetDataTable(sql));

            if (result == "")
                return "no";

            return result;
        }

        /// <summary>
        /// 删除多条
        /// </summary>
        /// <param name="guid">id</param>
        /// <returns></returns>
        private string Delete(string idlist)
        {

            string type = CommonData.GetUserTypeByUserID();
            //外网(不能删除)
            if (type == "0")
            {
                type = null;
                return "no";
            }

            if (idlist == null || idlist == "")
                return "no";

            string[] array = idlist.Split(',');

            string sql = "";

            foreach (var item in array)
            {
                sql = string.Format("delete from TAB_OPERATELOG where GUID='{0}'", item);
                db.ExecuteSingleString(sql);
            }

            return "yes";
        }

        /// <summary>
        /// 分页获取
        /// </summary>
        /// <returns></returns>
        private string GetDT(string toptime)
        {

            DateTime tt = DateTime.Now;

            if (toptime == null || toptime == "")
                tt = DateTime.Now;
            else
                tt = Convert.ToDateTime(toptime);

            string type = CommonData.GetUserTypeByUserID();

            string sql = "";

            //外网
            if (type == "0")
                sql = string.Format("select top 20 GUID,OPREATETIME,ORGNAME,CONTENT,OTHERUSE  from TAB_OPERATELOG where USERID='{0}' and OPREATETIME<'{1}' order by OPREATETIME desc ", CommonData.GetUserID(), tt);
            else
                sql = string.Format("select top 20 GUID,OPREATETIME,ORGNAME,CONTENT,OTHERUSE  from TAB_OPERATELOG where OPREATETIME<'{0}'  order by OPREATETIME desc", tt);

            toptime = CommonData.DataTableToJson(db.GetDataTable(sql));

            if (toptime == "")
                return "no";
            
            return toptime;
        }
    }
}