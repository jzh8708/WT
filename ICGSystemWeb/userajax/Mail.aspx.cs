﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBS;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace ICGSystemWeb.userajax
{
    public partial class Mail : System.Web.UI.Page
    {
        DBHelper DB = new DBHelper();

        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //非法请求Begin
            if (CommonData.GetUserID() == null || CommonData.GetUserID() == "")
            {
                Response.Write("-999"); Response.End();
            }
            //非法请求End

            object obj = Request.Form.Get("type");

            if (obj == null || obj.ToString() == "") { Response.Write("error"); Response.End(); }

            //收件箱 发件箱 回收站
            if (obj.ToString() == "getmysenddata")
                Response.Write(getmysenddata(CommonData.GetUserID(), int.Parse(Request.Form.Get("searchtype"))));
            //收件箱删除
            else if (obj.ToString() == "deleteinbox")
                Response.Write(deletevirtual(CommonData.GetUserID(), Request.Form.Get("idlist")).ToString());
            //发件箱删除
            else if (obj.ToString() == "deleteoutbox")
                Response.Write(deletevirtual(CommonData.GetUserID(), Request.Form.Get("idlist"), 2).ToString());
            //还原
            else if (obj.ToString() == "backview")
                Response.Write(UpdateToBackBox(Request.Form.Get("idlist")).ToString());
            //彻底删除
            else if (obj.ToString() == "deleteon")
                Response.Write(deleteon(Request.Form.Get("idlist")).ToString());
            //查看
            else if (obj.ToString() == "view")
                Response.Write(UpdateNumStatue(Request.Form.Get("idlist")));
            //发送新邮件
            else if (obj.ToString() == "addmsg")
                Response.Write(AddMsg() + "");
            //查看附件
            else if (obj.ToString() == "file")
                Response.Write(GetOneMailFiles(Request.Form.Get("id")));
            //删除自己上传的附件
            else if (obj.ToString() == "deletefile")
                Response.Write(DeleteFileByMyself(Request.Form.Get("pathlist")).ToString());
            //删除自己上传的附件
            else if (obj.ToString() == "deletefileone")
                Response.Write(DeleteFileByMyself(Request.Form.Get("pathlist"), 1).ToString());
            //获取机构名称
            else if (obj.ToString() == "getorgname")
                Response.Write(GetOrganizationName());
            //查询用户消息
            else if (obj.ToString() == "getusernews")
                Response.Write(GetNewsByNotRead(CommonData.GetUserID()));

            Response.End();
        }

        /// <summary>
        /// 根据条件查询对应的机构名称，用户id
        /// </summary>
        /// <param name="wh">条件</param>
        /// <returns>字符串</returns>
        private string GetOrganizationName()
        {
            string searchval = Request.Form.Get("searchval"), result;

            string sql = string.Format("select userid as USERID ,usernamenike as MYUSERNAME from usertable");
            if (searchval == null || searchval == "")
                result = CommonData.DataTableToJson(DB.GetDataTable(sql));
            else
                sql = string.Format("select userid as USERID ,usernamenike as MYUSERNAME from usertable where usernamenike like '%{0}%'", searchval);

            result = CommonData.DataTableToJson(DB.GetDataTable(sql));

            if (result == "") return "no";

            return result;
        }

        /// <summary>
        /// 删除自己上传的
        /// </summary>
        /// <param name="pathlist">路径数组</param>
        /// <returns></returns>
        private int DeleteFileByMyself(string pathlist)
        {
            if (pathlist == null || pathlist == "") return -1;

            string[] list = pathlist.Split(',');

            foreach (string a in list)
                if (File.Exists(HttpContext.Current.Server.MapPath(a)))
                    File.Delete(HttpContext.Current.Server.MapPath(a));

            return 1;
        }

        /// <summary>
        /// 删除自己上传的
        /// </summary>
        /// <param name="pathlist">路径</param>
        /// <param name="pathlist">删除单个</param>
        /// <returns></returns>
        private int DeleteFileByMyself(string pathlist, int isone)
        {
            pathlist = HttpUtility.UrlDecode(pathlist);

            if (pathlist == null || pathlist == "") return -1;

            if (File.Exists(HttpContext.Current.Server.MapPath(pathlist)))
                File.Delete(HttpContext.Current.Server.MapPath(pathlist));

            return 1;
        }

        /// <summary>
        /// 获取一条邮件的所有附件
        /// </summary>
        /// <param name="intID">id</param>
        /// <returns>json string</returns>
        private string GetOneMailFiles(string intid)
        {
            if (intid == null || intid == "" || intid.Contains(",") == false) return "-1";

            string[] array = intid.Split(',');
            string cloumns = @"FILEID    
                            ,FILEPATH  
                            ,FILETYPE  
                            ,FILESIZE  
                            ,USINGCOUNT
                            ,FILENAMES AS FILENAME";
            string sql = string.Format("select {0} from TAB_MAILFILES where FILEID='{1}'", cloumns, array[0]);

            intid = CommonData.DataTableToJson(DB.GetDataTable(sql));

            if (intid == "")
                return "no";//无数据
            return intid;
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <returns></returns>
        private int AddMsg()
        {
            if (CommonData.GetUserID() == null || Request.Form.Get("topeople") == null || Request.Form.Get("topeople") == "" || CommonData.GetUserID() == "") return -1;

            string topeople = Request.Form.Get("topeople");
            string userid = CommonData.GetUserID();//login user id
            string[] touseridlist = topeople.Split(',');
            string content = Request.Form.Get("content");
            string title = Request.Form.Get("title");
            string filepath = Request.Form.Get("filepath");
            int Isattachfile = filepath == null || filepath == "" ? 0 : 1;
            int UsingCount = 2 * touseridlist.Length;

            string guid = Guid.NewGuid().ToString();

            string aa = string.Format("insert into TAB_MAIL(ID,TITLE,CONTENT,ISATTACHFILE,USINGCOUNT) values ('{0}','{1}','{2}','{3}',{4})", guid, title, content, Isattachfile, UsingCount);
            DB.ExecuteNonQuery(aa);
            aa = null;

            CommonData.AddLog("发送了邮件:" + title);

            for (int i = 0; i < touseridlist.Length; i++)
            {
                //邮件发送 转发 begin

                string sql = string.Format("insert into TAB_MAILSENDRESERVED ({0}) values (@FID,@MAILGUID,@SENDUSERID,@RESERVEDUSERID,@SENDTIME,@ISBACKMSG,@STATUE,@SENDUSERNAME,@RESERVEDNAME)", CommonData.TAB_MAILSENDRESERVEDCloumns);
                SqlParameter[] para = new SqlParameter[] { 
                    new SqlParameter("@FID",Guid.NewGuid().ToString()),
                    new SqlParameter("@MAILGUID",guid),
                    new SqlParameter("@SENDUSERID",userid),
                    new SqlParameter("@RESERVEDUSERID",touseridlist[i]),
                    new SqlParameter("@SENDTIME",Convert.ToDateTime(Request.Form.Get("ttime"))),
                    new SqlParameter("@ISBACKMSG","0"),
                    new SqlParameter("@STATUE","0"),
                    new SqlParameter("@SENDUSERNAME",GetUserNameByUserID(userid)),
                    new SqlParameter("@RESERVEDNAME",GetUserNameByUserID(touseridlist[i]))
                };
                DB.ExecuteNonQuery(sql, para);
                para = new SqlParameter[] { 
                    new SqlParameter("@FID",Guid.NewGuid().ToString()),
                    new SqlParameter("@MAILGUID",guid),
                    new SqlParameter("@SENDUSERID",userid),
                    new SqlParameter("@RESERVEDUSERID","-100"),//固定值
                    new SqlParameter("@SENDTIME",Convert.ToDateTime(Request.Form.Get("ttime"))),
                    new SqlParameter("@ISBACKMSG","0"),
                    new SqlParameter("@STATUE","0"),
                    new SqlParameter("@SENDUSERNAME",GetUserNameByUserID(userid)),
                    new SqlParameter("@RESERVEDNAME",GetUserNameByUserID(touseridlist[i]))
                };
                DB.ExecuteNonQuery(sql, para);
            }

            if (filepath == null || filepath == "") return -1;

            string[] filepatharray = filepath.Split(',');//附件路径
            string[] filename = Request.Form.Get("filename").Split(',');//附件名称
            string[] filesize = Request.Form.Get("filesize").Split(',');//附件大小

            for (int i = 0; i < filepatharray.Length; i++)
            {
                string sql = string.Format("insert into TAB_MAILFILES(FILEID,FILEPATH,FILETYPE,FILESIZE,USINGCOUNT,FILENAMES) values (@FILEID,@FILEPATH,@FILETYPE,@FILESIZE,@USINGCOUNT,@FILENAMES)");
                SqlParameter[] para = new SqlParameter[] {
                                              new SqlParameter("@FILEID",guid),
                                              new SqlParameter("@FILEPATH",filepatharray[i]),
                                              new SqlParameter("@FILETYPE",filename[i].Substring(filename[i].LastIndexOf('.') + 1)),
                                              new SqlParameter("@FILESIZE",Math.Round(decimal.Parse(filesize[i]), 2)),
                                              new SqlParameter("@USINGCOUNT", 2 * touseridlist.Length),
                                              new SqlParameter("@FILENAMES",filename[i])
                                            };
                DB.ExecuteNonQuery(sql, para);
            }
            return 1;
        }


        /// <summary>
        /// 根据id查询对应的用户名称[ok]
        /// </summary>
        /// <param name="intid">用户id</param>
        /// <returns></returns>
        private string GetUserNameByUserID(string userid)
        {
            string sql = string.Format("select usernamenike from usertable where userid='{0}'", userid);

            return DB.ExecuteSingleString(sql, null);
        }


        /// <summary>
        /// 查看状态//状态 (0:未读, 1:已读，2:已发送)
        /// </summary>
        /// <param name="intid">id</param>
        /// <returns></returns>
        private string UpdateNumStatue(string intid)
        {
            if (intid == null || intid == "" || intid.Contains(",") == false) return "-1";

            string[] array = intid.Split(',');

            string sql = string.Format("update TAB_MAILSENDRESERVED set STATUE=1 where FID='{0}'", array[1]);

            DB.ExecuteNonQuery(sql);

            return CommonData.DataTableToJson(GetFullMail(array[0]));//查询一条完整的邮件记录
        }

        private DataTable GetFullMail(string mailid)
        {
            string sql = string.Format("select ID ,TITLE,CONTENT,ISATTACHFILE ,SENDUSERNAME,RESERVEDNAME,SENDTIME from TAB_MAIL,TAB_MAILSENDRESERVED    where  TAB_MAIL.ID=TAB_MAILSENDRESERVED.MAILGUID   and   TAB_MAIL.ID='{0}'", mailid);

            return DB.GetDataTable(sql);
        }


        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="intid">id</param>
        /// <returns></returns>
        private int deleteon(string intid)
        {
            if (intid == null || intid == "" || intid.Contains(",") == false) return -1;

            string[] array = intid.Split(',');

            string sql = string.Format("delete from TAB_MAILSENDRESERVED where FID='{0}'", array[1]);
            DB.ExecuteNonQuery(sql, null);
            return 1;
        }


        /// <summary>
        /// 从回收站返回到队员的box
        /// </summary>
        /// <param name="intid">id</param>
        /// <returns></returns>
        private int UpdateToBackBox(string intid)
        {
            if (intid == null || intid == "" || intid.Contains(",") == false) return -1;

            string[] array = intid.Split(',');

            string sql = string.Format("update  TAB_MAILSENDRESERVED  set ISDELETE=0  where FID='{0}'", array[1]);

            DB.ExecuteNonQuery(sql);

            return 1;
        }

        /// <summary>
        /// 获取收件箱0，发送箱1，回收站2
        /// </summary>
        /// <param name="userid">当前登录人id</param>
        /// <param name="searchtype">查找数据类型</param>
        /// <returns></returns>
        private string getmysenddata(string userid, int searchtype)
        {
            string table = "TAB_MAILSENDRESERVED";
            string sql = "";
            if (searchtype == 0)
                sql = string.Format("select {0},TITLE,ID,ISATTACHFILE  from {1},TAB_MAIL where TAB_MAILSENDRESERVED.MAILGUID=TAB_MAIL.ID   and  ISDELETE=0   and  RESERVEDUSERID='{2}' and    SENDTIME<getdate() order by SENDTIME desc  ", CommonData.TAB_MAILSENDRESERVEDCloumns, table, userid);
            else if (searchtype == 1)
                sql = string.Format("select {0},TITLE,ID,ISATTACHFILE  from {1},TAB_MAIL where  TAB_MAILSENDRESERVED.MAILGUID=TAB_MAIL.ID  and  ISDELETE=0 and   SENDUSERID='{2}'   and  RESERVEDUSERID='-100'  order by SENDTIME desc  ", CommonData.TAB_MAILSENDRESERVEDCloumns, table, userid);
            else if (searchtype == 2)
                sql = string.Format("select {0},TITLE,ID,ISATTACHFILE from {1},TAB_MAIL where   TAB_MAILSENDRESERVED.MAILGUID=TAB_MAIL.ID  and     ISDELETE=1 and  ( SENDUSERID='{2}' or RESERVEDUSERID='{2}')   order by SENDTIME desc  ", CommonData.TAB_MAILSENDRESERVEDCloumns, table, userid);

            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }


        /// <summary>
        /// 查看未读消息总数
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        private string GetNewsByNotRead(string userid)
        {
            string sql = string.Format("select  count(0)  from {1} where  ISDELETE=0   and  RESERVEDUSERID='{2}' and SENDTIME<getdate()   and  STATUE=0  ", CommonData.TAB_MAILSENDRESERVEDCloumns, "TAB_MAILSENDRESERVED", userid);

            return DB.ExecuteSingleString(sql);
        }


        /// <summary>
        /// 删除收件箱
        /// </summary>
        /// <param name="userid">login id</param>
        /// <param name="idlist">id的数组</param>
        /// <returns></returns>
        private int deletevirtual(string userid, string idlist)
        {
            if (userid == null || idlist == null || userid == "" || idlist == "" || idlist.Contains(",") == false) return -1;

            string[] array = idlist.Split(',');
            string sql = "";

            sql = string.Format("update  TAB_MAILSENDRESERVED  set ISDELETE=1  where FID='{0}' and RESERVEDUSERID='{1}'", array[1], userid);
            DB.ExecuteNonQuery(sql, null);

            return 1;
        }

        /// <summary>
        /// 删除send件箱
        /// </summary>
        /// <param name="userid">login id</param>
        /// <param name="idlist">id的数组</param>
        /// <returns></returns>
        private int deletevirtual(string userid, string idlist, int updatesend)
        {
            if (userid == null || idlist == null || userid == "" || idlist == "" || idlist.Contains(",") == false) return -1;

            string[] array = idlist.Split(',');
            string sql = "";

            sql = string.Format("update  TAB_MAILSENDRESERVED  set ISDELETE=1  where FID='{0}' and SENDUSERID='{1}'", array[1], userid);
            DB.ExecuteNonQuery(sql, null);

            return 1;
        }
    }
}