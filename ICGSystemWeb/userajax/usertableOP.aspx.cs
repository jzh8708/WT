using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
using MyDAL;

namespace ICGSystemWeb
{
    //ajax底层处理代码
    public partial class usertableOPajax : System.Web.UI.Page
    {
        //实例化
        DBHelper DB = new DBHelper();
        usertableOP DAL = new usertableOP();
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //获取对应的枚举项
            if (type == "getenumitembyid")
                type = GetEnumItemData(Request.Form.Get("myenumid"), Request.Form.Get("userid"));
            //新增
            else if (type == "add")
                type = AddOrUpdateDB();
            //获取用户定义数量的数据列表
            else if (type == "getlist")
                type = GetTopNumberData();
            //获取一天修改数据
            else if (type == "getone")
                type = getone(Request.Form.Get("id").Trim());
            //删除
            else if (type == "delete")
                type = delete(Request.Form.Get("id"), Request.Form.Get("userid"));

            Response.Write(type);
            Response.End();
        }
        //删除
        private string delete(string id, string userid)
        {
            return DAL.DeleteDB(id);
        }

        //获取一个
        private string getone(string id)
        {
            id = CommonData.DataTableToJson(DAL.GetOneDataByGuid(id));
            id = id == "" ? "nodata" : id;
            return id;
        }

        //获取用户定义数量的数据列表
        private string GetTopNumberData()
        {
            string endtime = Request.Form.Get("endtime");
            if (endtime == "") endtime = DateTime.Now.AddDays(1).ToString();
            endtime = CommonData.DataTableToJson(DAL.GetTopNumberData(int.Parse(Request.Form.Get("pagesize")), endtime));
            if (endtime == "")
                endtime = "nodata";
            return endtime;
        }

        //获取枚举项BY ID
        private string GetEnumItemData(string enumid, string userid)
        {
            if (enumid == null || enumid == "" || userid == null || userid == "") return CommonData.nodata;

            string sql = string.Format("select  NAME,VAL from ICGEnumItemTab where enumid='{0}' and userid='{1}' ", enumid, userid);
            sql = CommonData.DataTableToJson(DB.GetDataTable(sql, null));

            if (sql == "") sql = CommonData.nodata;
            return sql;
        }


        /// <summary>
        /// 新增或者修改
        /// </summary>
        /// <returns></returns>
        private string AddOrUpdateDB()
        {
            string[] a = Request.Form.Get("myidarray").Split(',');
            string guid = Request.Form.Get("usertableID");

            if (guid == "-1")
            {
                if (int.Parse(DAL.Getexists(a[2].Trim())) > 0)
                {
                    return "8766";//存在
                }
                guid = Guid.NewGuid().ToString();
                DAL.AddDB(guid, guid, Guid.NewGuid().ToString(), a[2].Trim(), a[3], Request.Form.Get("usergroupid"), a[5], a[6], a[7], a[8], DateTime.Now.ToString(), guid, a[10], a[11]);
                a = null;

                CommonData.AddLog("新增了用户:" + a[2]);

                return guid;
            }
            else
            {
                CommonData.AddLog("修改了用户信息:" + a[2]);

                DAL.UpdateDB(a[2].Trim(), a[3], Request.Form.Get("usergroupid"), a[5], a[6], a[7], a[8], guid, a[10], a[11]);
                a = null;
                return guid;
            }
        }
    }
}