using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
using MyDAL;

namespace ICGSystemWeb
{
    //ajax底层处理代码
    public partial class userfilesmanagOPajax : System.Web.UI.Page
    {
        //实例化
        DBHelper DB = new DBHelper();
        userfilesmanagOP DAL = new userfilesmanagOP();
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //获取对应的枚举项
            if (type == "getenumitembyid")
                type = GetEnumItemData(Request.Form.Get("myenumid"), Request.Form.Get("userid"));
            //新增
            else if (type == "add")
                type = AddOrUpdateDB();
            //获取用户定义数量的数据列表
            else if (type == "getlist")
                type = GetTopNumberData();
            //获取一天修改数据
            else if (type == "getone")
                type = getone(Request.Form.Get("id").Trim());
            //删除
            else if (type == "delete")
                type = delete(Request.Form.Get("id"), Request.Form.Get("userid"));
            //上传文件直接新增到数据库
            else if (type == "AddByupload")
                type = AddByupload(Request.Form.Get("filename"), Request.Form.Get("filepath"), Request.Form.Get("filesize"));
            //获取用户已上传的文件类型【搜索使用】
            else if (type == "getfiletypeaa")
                type = getfiletype();
            //根据类别搜索
            else if (type == "searchtype")
                type = searchtype(Request.Form.Get("name"));


            Response.Write(type);
            Response.End();
        }


        /// <summary>
        /// 根据类别搜索
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private string searchtype(string type)
        {
            if (type == "全部")
                type = "";
            string sql = string.Format("select   userid,fileid,filename,filesize,filetype,filepath,isshare,sharepath,uploadtime, ID,CreatTime from userfilesmanag where userid='{0}' and filetype like '%{1}%' order by uploadtime desc ", CommonData.GetUserID(), type);
            return CommonData.DataTableToJson(DB.GetDataTable(sql, null));
        }


        /// <summary>
        /// 获取用户已上传的文件类型【搜索使用】
        /// </summary>
        /// <returns></returns>
        private string getfiletype()
        {
            string sql = string.Format("select filetype from userfilesmanag where userid='{0}' group by filetype", CommonData.GetUserID());
            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }

        /// <summary>
        /// 上传文件直接新增到数据库
        /// </summary>
        /// <param name="name"></param>
        /// <param name="path"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private string AddByupload(string name, string path, string size)
        {
            string guid = Guid.NewGuid().ToString();
            string sql = string.Format("insert into userfilesmanag([ID],[userid],[fileid],[filename],[filesize],[filetype],[filepath]) values (@ID,@userid,@fileid,@filename,@filesize,@filetype,@filepath)");
            SqlParameter[] sqlpara = new SqlParameter[] { 
                new SqlParameter("@ID",guid),
                new SqlParameter("@userid",CommonData.GetUserID()),
                new SqlParameter("@fileid",guid),
                new SqlParameter("@filename",name),
                new SqlParameter("@filesize",size),
                new SqlParameter("@filetype",name.Substring(name.LastIndexOf('.')+1).ToLower()),
                new SqlParameter("@filepath",path)
                 };
            DB.ExecuteNonQuery(sql, sqlpara);
            return guid;
        }


        //删除
        private string delete(string id, string userid)
        {
            return DAL.DeleteDB(id);
        }

        //获取一个
        private string getone(string id)
        {
            id = CommonData.DataTableToJson(DAL.GetOneDataByGuid(id));
            id = id == "" ? "nodata" : id;
            return id;
        }

        //获取用户定义数量的数据列表
        private string GetTopNumberData()
        {
            string endtime = Request.Form.Get("endtime");
            if (endtime == "") endtime = DateTime.Now.AddDays(1).ToString();
            endtime = CommonData.DataTableToJson(DAL.GetTopNumberData(int.Parse(Request.Form.Get("pagesize")), endtime));
            if (endtime == "")
                endtime = "nodata";
            return endtime;
        }

        //获取枚举项BY ID
        private string GetEnumItemData(string enumid, string userid)
        {
            if (enumid == null || enumid == "" || userid == null || userid == "") return CommonData.nodata;

            string sql = string.Format("select  NAME,VAL from ICGEnumItemTab where enumid='{0}' and userid='{1}' ", enumid, userid);
            sql = CommonData.DataTableToJson(DB.GetDataTable(sql, null));

            if (sql == "") sql = CommonData.nodata;
            return sql;
        }


        private string AddOrUpdateDB()
        {
            string[] a = Request.Form.Get("myidarray").Split(',');
            string guid = Request.Form.Get("userfilesmanagID");
            if (guid == "-1")
            {
                guid = Guid.NewGuid().ToString();
                DAL.AddDB(guid, CommonData.GetUserID(), a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
                a = null;
                return guid;
            }
            else
            {
                DAL.UpdateDB(guid,CommonData.GetUserID(), a[1], a[2], a[3], a[4], a[5], a[6], a[7]);
                a = null;
                return guid;
            }
        }
    }
}