﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyDAL;

namespace ICGSystemWeb.userajax
{
    public partial class desktopconfigOP : System.Web.UI.Page
    {
        usertableOP dal = new usertableOP();

        protected void Page_Load(object sender, EventArgs e)
        {
            //CommonData.SetCookie("usercode", "test");

            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //查询用户购买的app
            if (type == "getlistall")
                type = GetUserList(CommonData.GetUserID());
            //获取用户信息
            else if (type == "getuserinfo")
                type = GetUserInfo(CommonData.GetUserID());

            Response.Write(type);
            Response.End();
        }

        /// <summary>
        /// 获取用户app
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        private string GetUserList(string userid)
        {
            return CommonData.DataTableToJson(dal.GetList(userid));
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        private string GetUserInfo(string userid)
        {
            return CommonData.DataTableToJson(dal.GetUserInfo(userid));
        }

        /// <summary>
        /// 查询用户未读消息
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        private string GetNews(string userid)
        {

            return CommonData.DataTableToJson(dal.GetNews(userid, 0));
        }
    }
}