﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBS;
using System.Data;
using System.Data.SqlClient;


namespace ICGSystemWeb.userajax
{
    public partial class Video : System.Web.UI.Page
    {
        DBHelper DB = new DBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            //非法请求Begin
            if (CommonData.GetUserID() == null || CommonData.GetUserID() == "")
            {
                Response.Write("-999"); Response.End();
            }
            //非法请求End

            object obj = Request.Form.Get("type");

            if (obj == null || obj.ToString() == "") { Response.Write("error"); Response.End(); }

            //新增视频
            if (obj.ToString() == "add")
                Response.Write(AddVIDEO());

            else if (obj.ToString() == "getlist")
                Response.Write(GetList());

            else if (obj.ToString() == "getlist2")
                Response.Write(GetList(Request.Form.Get("id")));

            Response.End();
        }

        /// <summary>
        /// 添加视频
        /// </summary>
        /// <returns></returns>
        private string AddVIDEO()
        {
            string sql = string.Format("insert into TAB_VIDEO(guid,title,href,poster,videosize) values (@guid,@title,@href,@poster,@videosize)");
            SqlParameter[] para = new SqlParameter[] {
            new SqlParameter("@guid",Guid.NewGuid().ToString()),
            new SqlParameter("@title",Request.Form.Get("title")),
            new SqlParameter("@href",Request.Form.Get("href")),
            new SqlParameter("@videosize", Convert.ToDouble(Request.Form.Get("videosize"))),
            new SqlParameter("@poster",Request.Form.Get("poster"))
            };

            DB.ExecuteNonQuery(sql, para);

            CommonData.AddLog("新增了视频：" + Request.Form.Get("title"));

            return "1";
        }

        /// <summary>
        /// 获取用户上传的列表
        /// </summary>
        /// <returns></returns>
        private string GetList()
        {
            object toptime = Request.Form.Get("toptime");

            if (toptime == null || toptime.ToString() == "")
                toptime = DateTime.Now;

            string sql = string.Format("select top 10  guid,title,href,poster,videosize,createtime from TAB_VIDEO  where  createtime<'{0}' order by createtime desc ", Convert.ToDateTime(toptime));

            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }

        /// <summary>
        /// 获取用户上传的列表
        /// </summary>
        /// <returns></returns>
        private string GetList(string id)
        {
            object toptime = Request.Form.Get("toptime");

            if (toptime == null || toptime.ToString() == "")
                toptime = DateTime.Now;

            string sql = string.Format("select   guid,title,href,poster,videosize,createtime from TAB_VIDEO  where  guid='{0}' ", id);

            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }
    }
}