using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
using MyDAL;

namespace ICGSystemWeb
{
    //ajax底层处理代码
    public partial class useropreatlogOPajax : System.Web.UI.Page
    {
        //实例化
        DBHelper DB = new DBHelper();
        useropreatlogOP DAL = new useropreatlogOP();
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //获取对应的枚举项
            if (type == "getenumitembyid")
                type = GetEnumItemData(Request.Form.Get("myenumid"), Request.Form.Get("userid"));
            //新增
            else if (type == "add")
                type = AddOrUpdateDB();
            //获取用户定义数量的数据列表
            else if (type == "getlist")
                type = GetTopNumberData();
            //获取一天修改数据
            else if (type == "getone")
                type = getone(Request.Form.Get("id").Trim());
            //删除
            else if (type == "delete")
                type = delete(Request.Form.Get("id"), Request.Form.Get("userid"));

            else if (type == "clickadd")
                type = clickadd(Request.Form.Get("title"));


            Response.Write(type);
            Response.End();
        }


        private string clickadd(string name)
        {
            CommonData.AddLog("用户(" + CommonData.GetLoginUserName() + ")使用了：" + name,0,0);
            return "1";
        }

        //删除
        private string delete(string id, string userid)
        {
            return DAL.DeleteDB(id);
        }

        //获取一个
        private string getone(string id)
        {
            id = CommonData.DataTableToJson(DAL.GetOneDataByGuid(id));
            id = id == "" ? "nodata" : id;
            return id;
        }

        //获取用户定义数量的数据列表
        private string GetTopNumberData()
        {
            string endtime = Request.Form.Get("endtime");
            if (endtime == "") endtime = DateTime.Now.AddDays(1).ToString();
            endtime = CommonData.DataTableToJson(DAL.GetTopNumberData(int.Parse(Request.Form.Get("pagesize")), endtime));
            if (endtime == "")
                endtime = "nodata";
            return endtime;
        }

        //获取枚举项BY ID
        private string GetEnumItemData(string enumid, string userid)
        {
            if (enumid == null || enumid == "" || userid == null || userid == "") return CommonData.nodata;

            string sql = string.Format("select  NAME,VAL from ICGEnumItemTab where enumid='{0}' and userid='{1}' ", enumid, userid);
            sql = CommonData.DataTableToJson(DB.GetDataTable(sql, null));

            if (sql == "") sql = CommonData.nodata;
            return sql;
        }




        private string AddOrUpdateDB()
        {
            string[] a = Request.Form.Get("myidarray").Split(',');
            string guid = Request.Form.Get("useropreatlogID");
            if (guid == "-1")
            {
                guid = Guid.NewGuid().ToString();
                DAL.AddDB(guid, a[0], a[1], a[2], a[3], a[4], guid);
                a = null;
                return guid;
            }
            else
            {
                DAL.UpdateDB(guid, a[0], a[1], a[2], a[3], a[4], guid);
                a = null;
                return guid;
            }
        }
    }
}