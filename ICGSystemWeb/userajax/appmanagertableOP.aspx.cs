using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
using MyDAL;
namespace ICGSystemWeb
{
    //ajax底层处理代码
    public partial class appmanagertableOPajax : System.Web.UI.Page
    {
        //实例化
        DBHelper DB = new DBHelper();
        appmanagertableOP DAL = new appmanagertableOP();
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Request.Form.Get("type");
            if (obj == null || obj.ToString() == "") { obj = null; Response.Write("nodata"); Response.End(); }//非法请求end
            string type = obj.ToString();
            obj = null;
            //获取对应的枚举项
            if (type == "getenumitembyid")
                type = GetEnumItemData(Request.Form.Get("myenumid"), Request.Form.Get("userid"));
            //新增
            else if (type == "add")
                type = AddOrUpdateDB();
            //获取用户定义数量的数据列表
            else if (type == "getlist")
                type = GetTopNumberData();
            //获取一天修改数据
            else if (type == "getone")
                type = getone(Request.Form.Get("id").Trim());
            //删除
            else if (type == "delete")
                type = delete(Request.Form.Get("id"), Request.Form.Get("userid"));
            //获取正在使用该应用的用户
            else if (type == "GetUsingUserListData")
                type = GetUsingUserListData(Request.Form.Get("appid"));
            //获取未使用该应用的用户
            else if (type == "GetUsingUserListData0")
                type = GetUsingUserListDataNO(Request.Form.Get("appid"));
            //分配
            else if (type == "Allocation")
                type = Allocation(Request.Form.Get("useridlist"), Request.Form.Get("appid"), Request.Form.Get("usebegintime"), Request.Form.Get("useendtime"), Request.Form.Get("trycount"), Request.Form.Get("buymoney"));
            //获取app信息
            else if (type == "getappidinfo")
                type = GetAppidInfo(Request.Form.Get("appid"));

            Response.Write(type);
            Response.End();
        }

        private string GetAppidInfo(string appid)
        {
            string sql = string.Format("select  top 1  usebegintime,useendtime,appcount,trycounts from appmanageruser  where appid='{0}' order by CreatTime asc ", appid);
            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }

        /// <summary>
        /// 分配
        /// </summary>
        /// <param name="useridlist"></param>
        /// <param name="appid"></param>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        /// <param name="trycount"></param>
        /// <param name="buymoney"></param>
        /// <returns></returns>
        private string Allocation(string useridlist, string appid, string t1, string t2, string trycount, string buymoney)
        {
            if (useridlist == null || useridlist == "")
                return "0";

            string[] array = useridlist.Split(',');
            if (array == null || array.Length == 0)
                return "0";

            string sql = "";


            ///删除之前的(用户没有购买的)data
            sql = string.Format("delete from appmanageruser where appid='{0}' and isbought=0 ", appid);
            DB.ExecuteNonQuery(sql);
            ///end


            SqlParameter[] para = null;
            DateTime tt1 = t1 == null || t1 == "" ? DateTime.Now.AddYears(-1) : Convert.ToDateTime(t1);
            DateTime tt2 = t2 == null || t2 == "" ? DateTime.Now.AddYears(10) : Convert.ToDateTime(t2);
            buymoney = buymoney == null || buymoney == "" ? "0 ": buymoney;
            trycount = trycount == null || trycount == "" ? "0 " : trycount;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == null || array[i] == "")
                    continue;
                sql = string.Format("insert into appmanageruser([appid],[userid],[username],[usebegintime],[useendtime],[appcount],[isbought],[trycounts]) values (@appid,@userid,@username,@usebegintime,@useendtime,@appcount,@isbought,@trycounts)");
                para = new SqlParameter[] {
                            new SqlParameter("@appid",appid),
                            new SqlParameter("@userid",array[i]),
                            new SqlParameter("@username",CommonData.GetLoginUserName(array[i])),
                            new SqlParameter("@usebegintime",tt1),
                            new SqlParameter("@useendtime",tt2),
                            new SqlParameter("@appcount",buymoney),
                            new SqlParameter("@isbought","0"),
                            new SqlParameter("@trycounts",trycount)
                };
                DB.ExecuteNonQuery(sql, para);
                sql = null;
                para = null;
            }
            sql = null;
            para = null;
            return "1";
        }

        /// <summary>
        /// 获取未使用该应用的用户
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        private string GetUsingUserListDataNO(string appid)
        {
            string sql = string.Format(" select usernamenike as  username,userid from usertable  where  userid not in (select userid from  appmanageruser where  appid='{0}')", appid);
            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }

        /// <summary>
        /// 获取正在使用该应用的用户
        /// </summary>
        /// <param name="appid"></param>
        /// <returns></returns>
        private string GetUsingUserListData(string appid)
        {
            string sql = string.Format(" select   username,userid from appmanageruser where appid='{0}'", appid);
            return CommonData.DataTableToJson(DB.GetDataTable(sql));
        }

        //删除
        private string delete(string id, string userid)
        {
            return DAL.DeleteDB(id);
        }

        //获取一个
        private string getone(string id)
        {
            id = CommonData.DataTableToJson(DAL.GetOneDataByGuid(id));
            id = id == "" ? "nodata" : id;
            return id;
        }

        //获取用户定义数量的数据列表
        private string GetTopNumberData()
        {
            string endtime = Request.Form.Get("endtime");
            if (endtime == "") endtime = DateTime.Now.AddDays(1).ToString();
            endtime = CommonData.DataTableToJson(DAL.GetTopNumberData(int.Parse(Request.Form.Get("pagesize")), endtime));
            if (endtime == "")
                endtime = "nodata";
            return endtime;
        }

        //获取枚举项BY ID
        private string GetEnumItemData(string enumid, string userid)
        {
            if (enumid == null || enumid == "" || userid == null || userid == "") return CommonData.nodata;

            string sql = string.Format("select  NAME,VAL from ICGEnumItemTab where enumid='{0}' and userid='{1}' ", enumid, userid);
            sql = CommonData.DataTableToJson(DB.GetDataTable(sql, null));

            if (sql == "") sql = CommonData.nodata;
            return sql;
        }

        private string AddOrUpdateDB()
        {
            string[] a = Request.Form.Get("myidarray").Split(',');
            string guid = Request.Form.Get("appmanagertableID");
            if (guid == "-1")
            {
                guid = Guid.NewGuid().ToString();
                DAL.AddDB(guid, a[1], a[2], a[3], a[4], "", guid);

                CommonData.AddLog("新增了应用:" + a[1]);

                a = null;
                return guid;
            }
            else
            {
                DAL.UpdateDB(guid, a[1], a[2], a[3], a[4], a[5], guid);
                
                CommonData.AddLog("修改了应用:" + a[1]);

                a = null;
                return guid;
            }
        }
    }
}