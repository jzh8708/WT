﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using DBS;

namespace ICGSystemWeb
{
    /// <summary>
    /// 电话薄【收费应用】
    /// </summary>
    public partial class Books2 : System.Web.UI.Page
    {
        private DBHelper DB = new DBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = string.Format("select top 1 appmanageruser.appid,appmanageruser.appcount,appmanageruser.trycounts ,isbought   from appmanageruser,appmanagertable where appmanagertable.ID=appmanageruser.appid and appsrc='{0}' ", "Books2.aspx");
            DataTable dt = DB.GetDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                if (int.Parse(dt.Rows[0]["trycounts"].ToString()) < 1 && dt.Rows[0]["isbought"].ToString() == "0")
                {
                    Response.Write("请购买!");
                }
                else
                {
                    Response.Write(GetISCompentence(dt.Rows[0]["appid"].ToString(), CommonData.GetUserID(), double.Parse(dt.Rows[0]["appcount"].ToString())));
                }
            }
            else
            {
                Response.Write("该用户未权限查看该应用");
            }
            Response.End();
        }
        private string GetISCompentence(string appid, string userid, double money)
        {
            string sql = string.Format("select count(0) from appmanagertable where ID='{0}'", appid);
            if (DB.ExecuteSingleString(sql) != "1")
                return "该应用不存在";

            sql = string.Format("select count(0) from appmanageruser where appid='{0}' and userid='{1}'", appid, userid);
            if (DB.ExecuteSingleString(sql, null) != "1")
                return "该用户未权限查看该应用";

            sql = string.Format("select userleftmoney from usertable where userid='{0}'", userid);
            double leftcount = double.Parse(DB.ExecuteSingleString(sql, null));
            if (leftcount < money || leftcount <= 0)
                return "该用户余额不足以支付该应用的金额";

            sql = string.Format("update usertable set userleftmoney=userleftmoney-{0}", money);
            DB.ExecuteNonQuery(sql, null);//success

            return "购买成功";
        }
    }
}