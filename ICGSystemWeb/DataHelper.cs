﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;


public class DataHelper
{

    public DataHelper()
    {

    }


    //获取当前登录用户ID
    public static string GetUserCode()
    {
        return GetCookie("usercode");
    }
 

    /// <summary>
    /// 获取cookies
    /// </summary>
    /// <param name="key">键</param>
    /// <returns>string</returns>
    public static string GetCookie(string key)
    {
        object obj = CookieHelper.Get(key, key);
        if (obj == null)
        {
            return "";
        }
        else
        {
            return obj.ToString();
        }
    }

    /// <summary>
    /// 设置cookies
    /// </summary>
    /// <param name="key">键</param>
    /// <param name="value">值</param>
    /// <returns>1</returns>
    public static int SetCookie(string key, string value)
    {
        CookieHelper.Set(key, key, value);
        return 1;
    }

    /// <summary>
    /// 清空cookies
    /// </summary>
    public static void ClearCookie()
    {
        CookieHelper.RemoveAll();
    }

    /// <summary>
    /// 设置缓存 val为null时表示清空key的缓存
    /// </summary>
    /// <param name="key">键</param>
    /// <param name="value">值</param>
    public static void SetCache(string key, object value)
    {
        if (value == null)
        {
            HttpRuntime.Cache.Remove(key);
            return;
        }
        if (HttpRuntime.Cache[key] == null)
        {
            HttpRuntime.Cache.Insert(key, value);
        }
        else
        {
            HttpRuntime.Cache[key] = value;
        }
    }

    /// <summary>
    /// 获取缓存
    /// </summary>
    /// <param name="key">键</param>
    /// <returns>object</returns>
    public static object GetCache(string key)
    {
        return HttpRuntime.Cache[key];
    }

    /// <summary>
    /// 获取唯一id
    /// </summary>
    /// <returns></returns>
    public static string GetGuid()
    {
        return System.Guid.NewGuid().ToString();
    }

}