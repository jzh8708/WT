﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using DBS;

namespace ICGSystemWeb
{
    /// <summary>
    /// Summary description for HGXWBS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    [System.Web.Script.Services.ScriptService]
    public class HGXWBS : System.Web.Services.WebService
    {
        private DBHelper DB = new DBHelper();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod(Description = "登录返回用户ID，是否可登录，权限（返回用户“类型，ID，是否可登录）")]
        public string Login(string name, string pass)
        {
            string sql = string.Format("select  usertype,userid,isallowlogin  from usertable where username=@username and userpass=@userpass ");

            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@userpass", pass), new SqlParameter("@username", name) };

            DataTable dt = DB.GetDataTable(sql, para);

            para = null;

            sql = null;

            if (dt == null || dt.Rows.Count == 0)
                return "-100";//登录失败

            if (dt.Rows[0]["isallowlogin"].ToString() == "0")
                return "99";//不允许登录

            CommonData.SetCookie(CommonData.usercode, dt.Rows[0]["userid"].ToString());

            CommonData.AddLog("用户登录:" + name);

            return CommonData.DataTableToJson(dt);//返回用户“类型，ID，是否可登录”
        }


        [WebMethod(Description = "支付")]
        public string Pay(string userid, double money, string appid)
        {
            string sql = string.Format("select count(0) from appmanagertable where ID='{0}'", appid);
            if (DB.ExecuteSingleString(sql) != "1")
                return "该应用不存在";

            sql = string.Format("select count(0) from appmanageruser where appid='{0}' and userid='{1}'", appid, userid);
            if (DB.ExecuteSingleString(sql, null) != "1")
                return "该用户未权限查看该应用";

            sql = string.Format("select userleftmoney from usertable where userid='{0}'", userid);
            if (double.Parse(DB.ExecuteSingleString(sql, null)) < money)
                return "该用户余额不足以支付该应用的金额";

            sql = string.Format("update usertable set userleftmoney=userleftmoney-{0}", money);
            DB.ExecuteNonQuery(sql, null);//success

            return "购买成功";
        }


        [WebMethod(Description = "判断 钱，积分")]
        public string GetUsersMoney(string userid)
        {
            string sql = string.Format("select userleftmoney, usercount from usertable where userid='{0}'", userid);
            return CommonData.DataTableToJson(DB.GetDataTable(sql, null));
        }


        [WebMethod(Description = "扣钱")]
        public string SaleUsersMoney(string userid,double money)
        {
            string sql = string.Format("select userleftmoney from usertable where userid='{0}'", userid);
            if (double.Parse(DB.ExecuteSingleString(sql, null)) < money)
                return "-1";//余额不足，扣钱失败

            sql = string.Format("update usertable set userleftmoney=userleftmoney-{0}", money);
            DB.ExecuteNonQuery(sql, null);//success

            return "1";//扣钱成功
        }


        [WebMethod(Description = "扣积分")]
        public string SaleUsersMoney(string userid, int usercount)
        {
            string sql = string.Format("select usercount from usertable where userid='{0}'", userid);
            if (double.Parse(DB.ExecuteSingleString(sql, null)) < usercount)
                return "-1";//积分不足，扣积分失败

            sql = string.Format("update usertable set usercount=usercount-{0}", usercount);
            DB.ExecuteNonQuery(sql, null);//success

            return "1";//扣积分成功
        }



        [WebMethod(Description = "加积分")]
        public string AddUsersMoney(string userid, int usercount)
        {

            string sql = string.Format("update usertable set usercount=usercount+{0}", usercount);
            DB.ExecuteNonQuery(sql, null);//success

            return "1";//扣积分成功
        }




        [WebMethod(Description = "获取用户详细信息")]
        public string GetUserFullInformatio()
        {
            DataTable dt = new DataTable();

            return CommonData.DataTableToJson(dt);
        }

        [WebMethod(Description = "获取用户已经购买的产品表")]
        public string GetUserBroughtSth(string suerid)
        {
            DataTable dt = new DataTable();

            return CommonData.DataTableToJson(dt);
        }

        [WebMethod(Description = "获取用户是否已经下载该文件")]
        public int getUserFileIsDowndload(string userid, string fileid, string tablename)
        {
            DataTable dt = new DataTable();

            CommonData.DataTableToJson(dt);

            return 1;//1代表已经下载过，其余数字代表未下载过（无权限）
        }

        [WebMethod(Description = "把二进制流的文件存到用户的权限下（文件夹），数据库存放文件所对于的路径")]
        public int SaveFileToUser(string userid, string touserid, byte[] byt2, string formate)
        {
            return 1;
        }
    }
}