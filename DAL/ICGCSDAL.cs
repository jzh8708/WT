﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


//ID ,CreatTime (表中必须有)
namespace DAL
{
    //程序处理(底层)
    public class ICGCSDAL
    {
        /// 创建sql文件
        public string NewCSFile(string namespacename, string classname, string tablename, string path, List<string[]> list)
        {
            StringBuilder sbd = new StringBuilder();

            sbd.Append(Getheaderstring(namespacename, classname));
            sbd.Append(GetAddString(list, tablename, 1));
            sbd.Append(getUpdateString(list, tablename, 1));
            sbd.Append(GetDeleteString(tablename));
            sbd.Append(GetSelectOneDTString(list, tablename));
            sbd.Append(GetTopNumberDatastring(list, tablename));
            sbd.Append(Getdownstring());

            string f = string.Format("{0}", '"');
            Datahelper.NewFile(path, classname + ".cs", sbd.ToString().Replace("'", f).Replace("$", "'"));

            return "";
        }

        //获取查询一条数据的方法
        private string GetSelectOneDTString(List<string[]> list, string tablename)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("//获取一条ID值为guid的数据\n");
            sbd.Append(" public DataTable GetOneDataByGuid(");
            sbd.Append("string guid");
            sbd.Append(")\n");
            sbd.Append("{\n");
            sbd.Append(" string sql = string.Format('select ");
            string[] temps = null;
            for (int k = 0; k < list.Count; k++)
            {
                temps = list[k];
                if (k == list.Count - 1)
                {
                    sbd.Append(" " + temps[0]);
                }
                else
                {
                    sbd.Append(" " + temps[0] + ",");
                }
            }
            sbd.Append(" from " + tablename + " where ID=@guid');\n");
            sbd.Append("SqlParameter[] para = new SqlParameter[] { new SqlParameter('@guid', guid)};\n");
            sbd.Append("return DB.GetDataTable(sql, para);\n");
            sbd.Append("}\n");

            return sbd.ToString();
        }

        //获取删除的字符串
        private string GetDeleteString(string tablename)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("//删除\n");
            sbd.Append("public string DeleteDB(string guid)\n");
            sbd.Append("{\n");
            sbd.Append("string sql = string.Format('delete from " + tablename + " where ID=@ID');\n");
            sbd.Append("SqlParameter[] para = new SqlParameter[] { new SqlParameter('@ID', guid)};\n");
            sbd.Append("return DB.ExecuteNonQuery(sql, para).ToString();\n");
            sbd.Append("}\n");
            return sbd.ToString();
        }

        //获取修改的字符串
        private string getUpdateString(List<string[]> list, string tablename)
        {
            StringBuilder sbd = new StringBuilder();
            StringBuilder sbdsqlpara = new StringBuilder();

            sbd.Append("//修改\n");
            sbd.Append("public string UpdateDB(");

            foreach (string[] temparray in list)
            {
                if (temparray[0].Trim() == "CreatTime") continue;
                if (temparray[2] == "nvarchar")
                {
                    sbd.Append("string " + temparray[0] + ",");
                    continue;
                }
                else if (temparray[2] == "datetime")
                {
                    sbd.Append("DateTime " + temparray[0] + ",");
                    continue;
                }
                sbd.Append(temparray[2] + " " + temparray[0] + ",");//type name
            }
            sbd.Append("string " + "guid");
            sbd.Append(")\n");
            sbd.Append("{\n");
            sbd.Append("string sql = string.Format('update  " + tablename + " set ");

            string[] tarray = null;
            for (int k = 0; k < list.Count; k++)
            {
                tarray = list[k];
                if (tarray[0].Trim() == "CreatTime") continue;
                if (k == list.Count - 2)
                {
                    sbd.Append(tarray[0] + "=@" + tarray[0]);
                    sbdsqlpara.Append("                     new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "," + "\n");
                }
                else
                {
                    sbd.Append(tarray[0] + "=@" + tarray[0] + ",");
                    sbdsqlpara.Append("                     new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "," + "\n");
                }
            }
            sbdsqlpara.Append("                      new SqlParameter('@guid', guid)\n");

            sbd.Append(" where ID=@guid');\n");
            sbd.Append("SqlParameter[] para = new SqlParameter[] \n");
            sbd.Append("                          {\n");
            sbd.Append(sbdsqlpara.ToString());
            sbd.Append("                           };\n");
            sbd.Append(" return DB.ExecuteNonQuery(sql, para).ToString();\n");
            sbd.Append("}\n");

            return sbd.ToString();
        }

        //获取修改的字符串
        private string getUpdateString(List<string[]> list, string tablename, int isallstring)
        {
            StringBuilder sbd = new StringBuilder();
            StringBuilder sbdsqlpara = new StringBuilder();

            sbd.Append("//修改\n");
            sbd.Append("public string UpdateDB(");

            foreach (string[] temparray in list)
            {
                if (temparray[0].Trim() == "CreatTime") continue;
                sbd.Append("string " + temparray[0] + ",");
            }
            sbd.Append("string " + "guid");
            sbd.Append(")\n");
            sbd.Append("{\n");
            sbd.Append("string sql = string.Format('update  " + tablename + " set ");

            string[] tarray = null;
            for (int k = 0; k < list.Count; k++)
            {
                tarray = list[k];
                if (tarray[0].Trim() == "CreatTime") continue;
                if (k == list.Count - 2)
                {
                    sbd.Append(tarray[0] + "=@" + tarray[0]);
                    sbdsqlpara.Append("                     new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "," + "\n");
                }
                else
                {
                    sbd.Append(tarray[0] + "=@" + tarray[0] + ",");
                    sbdsqlpara.Append("                     new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "," + "\n");
                }
            }
            sbdsqlpara.Append("                      new SqlParameter('@guid', guid)\n");

            sbd.Append(" where ID=@guid');\n");
            sbd.Append("SqlParameter[] para = new SqlParameter[] \n");
            sbd.Append("                          {\n");
            sbd.Append(sbdsqlpara.ToString());
            sbd.Append("                           };\n");
            sbd.Append(" return DB.ExecuteNonQuery(sql, para).ToString();\n");
            sbd.Append("}\n");

            return sbd.ToString();
        }


        //获取新增的字符串
        private string GetAddString(List<string[]> list, string tablename)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("//新增\n");
            sbd.Append("public string AddDB(");
            foreach (string[] temparray in list)
            {
                if (temparray[0].Trim() == "CreatTime") continue;
                if (temparray[2] == "nvarchar")
                {
                    sbd.Append("string " + temparray[0] + ",");
                    continue;
                }
                else if (temparray[2] == "datetime")
                {
                    sbd.Append("DateTime " + temparray[0] + ",");
                    continue;
                }
                sbd.Append(temparray[2] + " " + temparray[0] + ",");//type name
            }
            sbd.Append("string " + "guid");
            sbd.Append(")\n");
            sbd.Append("{\n");
            sbd.Append("string sql = string.Format('insert into " + tablename + " (");
            string[] tarray = null;
            StringBuilder sbdat = new StringBuilder();
            StringBuilder sbdsqlpara = new StringBuilder();
            for (int k = 0; k < list.Count; k++)
            {
                tarray = list[k];
                if (tarray[0].Trim() == "CreatTime") continue;//默认为系统时间
                if (k == list.Count - 2)
                {
                    sbd.Append(tarray[0]);
                    sbdat.Append("@" + tarray[0]);
                    sbdsqlpara.Append("                  new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "\n");
                }
                else
                {
                    sbd.Append(tarray[0] + ",");
                    sbdat.Append("@" + tarray[0] + ",");
                    sbdsqlpara.Append("                  new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "," + "\n");
                }
                tarray = null;
            }
            sbd.Append(") values(" + sbdat.ToString() + ")');" + "\n");
            sbd.Append("SqlParameter[] sqlpara = new SqlParameter[]" + "\n");
            sbd.Append("                        {\n");
            sbd.Append(sbdsqlpara.ToString());
            sbd.Append("                        };\n");
            sbd.Append("DB.ExecuteNonQuery(sql, sqlpara);" + "\n");
            sbd.Append("return guid;\n");
            sbd.Append("}\n");
            return sbd.ToString();
        }

        //获取新增的字符串
        private string GetAddString(List<string[]> list, string tablename, int isallstring)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("//新增\n");
            sbd.Append("public string AddDB(");
            foreach (string[] temparray in list)
            {
                if (temparray[0].Trim() == "CreatTime") continue;//默认为系统时间
                sbd.Append("string " + temparray[0] + ",");
            }
            sbd.Append("string " + "guid");
            sbd.Append(")\n");
            sbd.Append("{\n");
            sbd.Append("string sql = string.Format('insert into " + tablename + " (");
            string[] tarray = null;
            StringBuilder sbdat = new StringBuilder();
            StringBuilder sbdsqlpara = new StringBuilder();
            for (int k = 0; k < list.Count; k++)
            {
                tarray = list[k];
                if (tarray[0].Trim() == "CreatTime") continue;//默认为系统时间
                if (k == list.Count - 2)
                {
                    sbd.Append(tarray[0]);
                    sbdat.Append("@" + tarray[0]);
                    sbdsqlpara.Append("                  new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "\n");
                }
                else
                {
                    sbd.Append(tarray[0] + ",");
                    sbdat.Append("@" + tarray[0] + ",");
                    sbdsqlpara.Append("                  new SqlParameter('@" + tarray[0] + "'," + tarray[0] + ")" + "," + "\n");
                }
                tarray = null;
            }
            sbd.Append(") values(" + sbdat.ToString() + ")');" + "\n");
            sbd.Append("SqlParameter[] sqlpara = new SqlParameter[]" + "\n");
            sbd.Append("                        {\n");
            sbd.Append(sbdsqlpara.ToString());
            sbd.Append("                        };\n");
            sbd.Append("DB.ExecuteNonQuery(sql, sqlpara);" + "\n");
            sbd.Append("return guid;\n");
            sbd.Append("}\n");
            return sbd.ToString();
        }

        //获取头部
        private string Getheaderstring(string namespacename, string classname)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("using System;\n");
            sbd.Append("using System.Collections.Generic;\n");
            sbd.Append("using System.Text;\n");
            sbd.Append("using System.Data;\n");
            sbd.Append("using System.Data.SqlClient;\n");
            sbd.Append("using DBS;\n");

            sbd.Append(" namespace  " + namespacename + "\n");
            sbd.Append("{\n");
            sbd.Append("public class " + classname + " {\n");
            sbd.Append(" //实例化\n");
            sbd.Append("DBHelper DB = new DBHelper();\n");
            return sbd.ToString();
        }

        //获取底部
        private string Getdownstring()
        {
            return "}\n" + "}\n";
        }

        //获取用户定义数量的数据列表-字符串
        private string GetTopNumberDatastring(List<string[]> list, string tablename)
        {
            if (list == null || list.Count == 0) return "";

            StringBuilder sbd = new StringBuilder();
            sbd.Append("//获取用户定义数量的数据列表\n");
            sbd.Append("public DataTable GetTopNumberData(int pagesize, string endtime)\n");
            sbd.Append("{\n");
            sbd.Append("string sql = string.Format(#select top {0} ");
            string[] temp = null;
            for (int j = 0; j < list.Count; j++)
            {
                temp = list[j];
                if (temp[0].Trim() == "ID" || temp[0].Trim() == "CreatTime") continue;
                if (temp[8] == "1")//为查询字段
                    sbd.Append(temp[0].Trim() + ",");
            }
            sbd.Append(" ID,CreatTime ");
            sbd.Append("from " + tablename + " where CreatTime<${1}$ order by CreatTime DESC#, pagesize, endtime);\n");
            sbd.Append(" return DB.GetDataTable(sql, null);\n");
            sbd.Append("}\n");

            return sbd.ToString().Replace('#', '"');
        }
    }
}