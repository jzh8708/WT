﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DAL
{
    public class ICGSQLDAl
    {
        /// 创建sql文件
        public string NewSQLFile(string tablename, string path, List<string[]> list)
        {
            if (list == null || list.Count == 0) return "";

            StringBuilder sbd = new StringBuilder();
            sbd.AppendFormat("create table  [{0}]", tablename);
            sbd.Append("\n(");
            string[] temparray = null;
            for (int j = 0; j < list.Count; j++)
            {
                sbd.Append("\n");
                temparray = list[j];
                //name、姓名、nvarchar、10、1
                if (temparray[2] == "nvarchar")
                {
                    //长度大雨3999自动为text类型
                    if (temparray[3] != "" && int.Parse(temparray[3]) > 3999)
                        sbd.Append("[" + temparray[0] + "]  " + " text " + (temparray[4] == "1" ? " not null " : " null ") + (j == (list.Count - 1) ? " " : " , "));
                    else
                        sbd.Append("[" + temparray[0] + "]  " + temparray[2] + "(" + temparray[3] + ")" + (temparray[4] == "1" ? " not null " : " null ") + (j == (list.Count - 1) ? " " : " , "));
                }
                else if (temparray[2] == "datetime")//时间字段[默认值为getdate()系统函数]
                {
                    sbd.Append("[" + temparray[0] + "]  " + temparray[2] + (temparray[4] == "1" ? "  default(getdate())  not null " : "  default(getdate())  null ") + (j == (list.Count - 1) ? " " : " , "));
                }
                else if (temparray[7] == "1")//枚举值
                {
                    sbd.Append("[" + temparray[0] + "]  " + "nvarchar" + (temparray[4] == "1" ? " not null " : " null ") + (j == (list.Count - 1) ? " " : " , "));
                }
                else
                {
                    sbd.Append("[" + temparray[0] + "]  " + temparray[2] + (temparray[4] == "1" ? " not null " : " null ") + (j == (list.Count - 1) ? " " : " , "));
                }
                temparray = null;
            }
            sbd.Append("\n");
            sbd.Append(")");


            path = Datahelper.NewFile(path, tablename + ".sql", sbd.ToString());

            sbd = null;

            list = null;

            return tablename + ".sql";//创建sql语句的最终文件名称
        }
    }
}
