﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    //JS about
    public class ICGJSDAL
    {
        /// 创建html文件
        public string NewHtmlFile(string namespacename, string classname, string tablename, string path, List<string[]> list)
        {
            StringBuilder sbd = new StringBuilder();

            sbd.Append(GetHeadStringJs(list, tablename) + "\n");
            sbd.Append(GetBodyStirng(list, tablename));


            string f = string.Format("{0}", '"');
            Datahelper.NewFile(path, classname + ".html", sbd.ToString().Replace("'", f).Replace("$#*", "'"));

            return "";
        }


        private string sinformate = "$#*";//单引号-->$#*   “”--》‘

        private string GetHeadStringJs(List<string[]> list, string tablename)
        {
            StringBuilder sbd = new StringBuilder();
            string a = @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta charset='utf-8'>
	<title>ICGS</title>
	<!-- The styles -->
	<link   href='css/bootstrap-cerulean.css' rel='stylesheet' />
	<style type='text/css'>
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
    <link type='text/css' href='css/bootstrap-responsive.css' rel='stylesheet' />
	<link type='text/css' href='css/charisma-app.css' rel='stylesheet' />
	<link type='text/css' href='css/jquery-ui-1.8.21.custom.css' rel='stylesheet' />
	<link type='text/css' href=$#*css/fullcalendar.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/fullcalendar.print.css$#* rel=$#*stylesheet$#*  media=$#*print$#* />
	<link type='text/css' href=$#*css/chosen.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/uniform.default.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/colorbox.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/jquery.cleditor.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/jquery.noty.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/noty_theme_default.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/elfinder.min.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/elfinder.theme.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/jquery.iphone.toggle.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/opa-icons.css$#* rel=$#*stylesheet$#* />
	<link type='text/css' href=$#*css/uploadify.css$#* rel=$#*stylesheet$#* />
    <link href='js/asyncbox/skins/ZCMS/asyncbox.css' type='text/css' rel='stylesheet' />
    <link href='Script/validationEngine/css/validationEngine.jquery.css' rel='stylesheet' type='text/css' />
    <!--jquery and javascript begin-->
    <script type='text/javascript' src='js/jquery-1.8.0.js'></script>
    <script language='javascript' type='text/javascript' src='Script/My97DatePicker/WdatePicker.js'></script>
    <script type='text/javascript' src=$#*Script/validationEngine/languages/jquery.validationEngine-zh_CN.js$#*></script>
    <script type='text/javascript' src=$#*Script/validationEngine/jquery.validationEngine.js$#*></script>
    <script type='text/javascript' src='js/asyncbox/AsyncBox.v1.4.5.js'></script>
        <script type='text/javascript'>
            var userid = 'feifei', tiptime = 2000, pagesize = 10, TESTTABID = -1;  //当前主ID
            var SLarray = []; //clear data obj
            var endtime = '';

            //common data begin
            var myidarray = [];
            //for-----code
            ";

            sbd.Append(a);

            foreach (string[] s in list)
            {
                if (s[0] == "ID" || s[0] == "CreatTime") continue;
                sbd.Append("myidarray.push('" + s[0] + "');\n");//添加name
            }

            a = @"			$(function () {
                jQuery('#divadd').validationEngine(); //验证
                //新增
                $('#btnadd').click(function () {";
            sbd.Append(a);

            sbd.Append(tablename + "ID = -1; \n");

            a = @"			$('#divadd').show();
                });
                //取消新增
                $('#btncancel').click(function () {
                    $('#divadd').hide();
                    $('#divlist').show();
                });
                //保存方法
                $('#btnsave').click(function () {
                    if (!jQuery('#divadd').validationEngine('validate')) {
                        return false;
                    }
                    var temparray = [];
                    for (var s = 0; s < myidarray.length; s++) {
                        ";

            sbd.Append(a+"\n");

            //枚举值的循环添加
            foreach (string[] ds in list)
            {
                if (ds[0] == "ID" || ds[0] == "CreatTime") continue;

                //枚举值
                if (ds[7] == "1")
                {
                    sbd.Append(" if (myidarray[s] == '" + ds[0] + "') {");
                    sbd.Append("temparray.push($('select[myid=$#*' + myidarray[s] + '$#*]').find('option:selected').attr('myval'));");
                    sbd.Append("continue;\n");
                    sbd.Append(" }\n");
                }
            }
            sbd.Append("temparray.push($('input[myid=$#*' + myidarray[s] + '$#*]').val());\n");
            sbd.Append(" }\n");

            a = " $.post('userajax/ajax.aspx?' + new Date().toTimeString(), { 'type': 'add', 'myidarray': temparray.toString(), 'userid': userid, '" + tablename + "ID': " + tablename + "ID }, function (data) {\n";
            sbd.Append(a);

            sbd.Append("endtime = ''; //时间默认值\n");
            sbd.Append(tablename + "ID = -1;");
            a = @"if (data.length == 36) {
                            success('保存成功！');
                            for (var s = 0; s < myidarray.length; s++)
                                $('input[myid=$#*' + myidarray[s] + '$#*]').val('');
                            temparray = null;
                        }

                        $('#divadd').hide();
                        $('#divlist').show();
                        getlist(); //获取前20条
                    });
                });
						";
            sbd.Append(a);

            a = @"//select 样式设置KO
                $('.divfeifeisl').click(function () {
                    var myenumid = $(this).children('select').attr('myenumid');
                    if (SLexists(myenumid) == true)
                        return; //第二次点击则直接返回

                    SLarray.push(myenumid);

                    $('select[myenumid=$#*' + myenumid + '$#*]').parent().children().remove($#*div$#*);
                    $('select[myenumid=$#*' + myenumid + '$#*]').removeClass();

                    $('select[myenumid=$#*' + myenumid + '$#*]').html('');
                    $('select[myenumid=$#*' + myenumid + '$#*]').append('<option myval=$#*0$#*>未选择</option>');

                    $.post('userajax/ajax.aspx', { 'type': 'getenumitembyid', 'myenumid': myenumid, 'userid': userid }, function (data) {
                        if (data == 'nodata') {
                            alerttip('没有枚举项数据！');
                            return;
                        }
                        var obj = $.parseJSON(data);

                        alerttip('当前为您查询出' + obj.length + '条枚举数据，您还可以自定义添加枚举');

                        for (var k = 0; k < obj.length; k++) {
                            $('select[myenumid=$#*' + myenumid + '$#*]').append('<option myval=$#*' + obj[k].VAL + '$#*>' + obj[k].NAME + '</option>');
                        }
                        obj = null;

                        $('select[myenumid=$#*' + myenumid + '$#*]').chosen();
                        $('select[myenumid=$#*' + myenumid + '$#*]').parent().children().children('a').attr('class', 'chzn-single chzn-single-with-drop');
                        $('select[myenumid=$#*' + myenumid + '$#*]').parent().children().children('div').attr('style', 'left:0px;top:24px');
                        $('select[myenumid=$#*' + myenumid + '$#*]').parent().children().children('div').children('div').children().focus();
                        $('select[myenumid=$#*' + myenumid + '$#*]').parent().children().children('div').children('ul').children('li:first-child').attr('class', 'active-result result-selected highlighted');
                    });
                });
                //页面第一次加载进行渲染
                function load() {
                    var obj = $('select[name=$#*SLData$#*]');
                    $(obj).each(function () {
                        $(this).chosen();
                    });
                }
                //加载页面渲染
                load();
                getlist();
                $('#btnmore').click(function () {
                    getlist();
                });
            })//dom end
			";
            sbd.Append(a);

            a = @"//getone
            function getone(id) {";
            sbd.Append(a + "\n");


            sbd.Append(tablename + "ID = id;");


            a = @" $('#divadd').show();
                $('#divlist').hide();

                $.post('userajax/ajax.aspx?' + new Date().toTimeString(), { 'type': 'getone', 'id': id }, function (data) {
                    var obj = $.parseJSON(data);
                    for (var s = 0; s < myidarray.length; s++) {

						";
            sbd.Append(a);
            //枚举值的循环添加
            for (int i = 0; i < list.Count; i++)
            {
                string[] ds = list[i];

                if (ds[0] == "ID" || ds[0] == "CreatTime") continue;

                //枚举值
                if (ds[7] == "1")
                {
                    sbd.Append(" if (myidarray[s] == '" + ds[0] + "') {");
                    sbd.Append("var myenumid = $('select[myid=$#*' + myidarray[s] + '$#*]').attr('myenumid');");
                    a = @"$('select[myenumid=$#*' + myenumid + '$#*]').parent().children().remove($#*div$#*);
                            $('select[myenumid=$#*' + myenumid + '$#*]').removeClass();

                            $('select[myenumid=$#*' + myenumid + '$#*]').html('');
                            $('select[myenumid=$#*' + myenumid + '$#*]').append('<option myval=$#*0$#*>未选择</option>');
                                    
                            if (SLexists(myenumid) == false) {

                                SLarray.push(myenumid);
                                $.post('userajax/ajax.aspx?' + new Date().toTimeString(), { 'type': 'getenumitembyid', 'myenumid': myenumid, 'userid': userid }, function (data) {
                                    if (data == 'nodata') {
                                        alerttip('没有枚举项数据！');
                                        return;
                                    }

                                    var sd = $.parseJSON(data);

                                    for (var k = 0; k < sd.length; k++) {";
                    sbd.Append(a + "\n");
                    sbd.Append("if (obj[0]." + ds[0] + " == sd[k].VAL) {\n");
                    a = @"$('select[myenumid=$#*' + myenumid + '$#*]').append('<option selected=$#*selected$#* myval=$#*' + sd[k].VAL + '$#*>' + sd[k].NAME + '</option>');
                                        }
                                        else {
                                            $('select[myenumid=$#*' + myenumid + '$#*]').append('<option myval=$#*' + sd[k].VAL + '$#*>' + sd[k].NAME + '</option>');
                                        }
                                    }
                                    sd = null;
                                    $('select[myenumid=$#*' + myenumid + '$#*]').chosen();
                                });
                            }
                            else {
                                $('select[myenumid=$#*' + myidarray[s] + '$#*]').find('option[myval=$#*' + obj[0][myidarray[s]] + '$#*]').attr('selected', 'selected');
                                $('select[myenumid=$#*' + myenumid + '$#*]').chosen();
                            }
							continue;
                        }";
                    sbd.Append(a + "\n");
                }
                ds = null;
            }

            a = @"  
                 $('input[myid=$#*' + myidarray[s] + '$#*]').val(obj[0][myidarray[s]]);
                      ";

            sbd.Append(a);

            a = @"                    }
                });
            }";
            sbd.Append(a);

            a = @"//获取10行数据
            function getlist() {
                //$('#tbodydatalist').html('');
                $.post('userajax/ajax.aspx?' + new Date().toTimeString(), { 'type': 'getlist', 'pagesize': pagesize, 'endtime': endtime }, function (data) {
                    if (data == 'nodata') {
                        alerttip('无数据、您可以新增数据！');
                        $('#btnmore').hide();
                    }
                    var obj = $.parseJSON(data);

                    if (obj.length < pagesize) { $('#btnmore').hide(); }

                    if (endtime == '')
                        $('#tbodydatalist').html('');

                    for (var i = 0; i < obj.length; i++) {";
            sbd.Append(a + "\n");

            sbd.Append("$('#tbodydatalist').append('<tr id=$#*tr' + obj[i].ID + '$#*>");
            foreach (string[] item in list)
            {
                if (item[0] == "ID" || item[0] == "CreatTime") continue;
                sbd.Append("<td class=$#*center$#*>' + obj[i]." + item[0] + ".replace(' 0:00:00', '') + '</td>");
            }
            sbd.Append("<td class=$#*center$#*><a class=$#*btn btn-success$#* onclick=domains($#*view$#*,$#*' + obj[i].ID + '$#*)><i class=$#*icon-zoom-in icon-white$#*></i>查看</a>&nbsp;<a class=$#*btn btn-info$#* onclick=domains($#*edit$#*,$#*' + obj[i].ID + '$#*)><i class=$#*icon-edit icon-white$#*></i>修改</a>&nbsp;<a class=$#*btn btn-danger$#* onclick=domains($#*delete$#*,$#*' + obj[i].ID + '$#*)><i class=$#*icon-trash icon-white$#*></i> 删除</a></td></tr>');\n");

            a = @" if (i == (obj.length - 1)) {
                            endtime = obj[i].CreatTime;
                        }
                    }

                })
            }";
            sbd.Append(a + "\n");
            a = @"//domain
            function domains(type, id) {
                if (type == 'delete') {
                    if (confirm('确定删除吗?') == false)
                        return;
                    $.post('userajax/ajax.aspx?' + new Date().toTimeString(), { 'type': 'delete', 'id': id, 'userid': userid }, function (data) {
                        if (data == '0') {
                            error('您没有权限删除此条数据');
                            return;
                        }
                        else if (data == '1') {
                            $('#tr' + id).remove();
                            success('删除成功!');
                        }
                    });
                }
                else if (type == 'view') {
                    SLarray = [];
                    getone(id);
                }
            }

            //判断是否为第二次点击
            function SLexists(myenumid) {
                for (var i = 0; i < SLarray.length; i++) {
                    if (SLarray[i] == myenumid)
                        return true;
                }
                return false;
            }
            /////----------------------common JS-------------------------------------
            //错误
            var timeoutlength = 3000;
            function error(name) {
                $.noty({ 'text': name + '</br>' + new Date().toTimeString(), 'layout': 'topRight', 'type': 'error', 'timeout': timeoutlength });
                return;
                asyncbox.tips(name, $#*error$#*, tiptime);
            }
            //成功
            function success(name) {
                $.noty({ 'text': name + '</br>' + new Date().toTimeString(), 'layout': 'topRight', 'type': 'success', 'timeout': timeoutlength });
                return;
                asyncbox.tips(name, $#*success$#*, tiptime);
            }
            //消息
            function alerttip(name) {
                $.noty({ 'text': name + '</br>' + new Date().toTimeString(), 'layout': 'topRight', 'type': 'success', 'timeout': timeoutlength });  //aler
                return;
                asyncbox.tips(name, $#*alert$#*, tiptime);
            }
            //等待
            function waittips() {
                asyncbox.tips('请稍后...!', $#*wait$#*, tiptime * 3);
            }
    </script>
</head>

";
            sbd.Append(a + "\n");


            return sbd.ToString();
        }

        private string GetBodyStirng(List<string[]> list, string tablename)
        {
            if (list == null || list.Count == 0)
                return "";
            StringBuilder abd = new StringBuilder();

            string a = @"<body>
            <!-- content starts -->
			<div class='row-fluid sortable' id='divadd' style='display:none'>
				<div class='box span12'>
					<div class='box-header well' data-original-title>
						<h2><i class='icon-edit'></i> 新增数据</h2>
						<div class='box-icon'>
							<a href='#' class='btn btn-minimize btn-round'><i class='icon-chevron-up'></i></a>
							<a href='#' class='btn btn-close btn-round'><i class='icon-remove'></i></a>
						</div>
					</div>
					<div class='box-content'>
						<form class='form-horizontal'>
							<fieldset>";
            abd.Append(a + "\n");

            foreach (string[] item in list)
            {
                if (item[0] == "ID" || item[0] == "CreatTime") continue;

                //枚举
                if (item[7] == "1")
                {
                    abd.Append("<div class='control-group'>\n");
                    abd.Append("<label class='control-label' for='selectError'>" + item[1] + "：</label>\n");
                    abd.Append("<div class='controls'>\n");
                    abd.Append("<div class='divfeifeisl' style='width:130px'>\n");
                    abd.Append("<select  id='select" + Guid.NewGuid().ToString() + "'  class='validate[" + (item[4] == "1" ? "required," : "") + "custom[noSpecialCaracters]]' myid='" + item[0] + "' name='SLData' myenumid='" + item[5] + "'>\n");
                    abd.Append("<option myval='0'>未选择</option>\n");
                    a = @"  </select>
                                    </div>
								</div>
							  </div>";
                    abd.Append(a + "\n");
                }
                else
                {
                    abd.Append("<div class='control-group success'>\n");
                    abd.Append("<label class='control-label' for='inputSuccess'>" + item[1] + "：</label>\n");
                    abd.Append("<div class='controls'>\n");
                    if (item[2] == "nvarchar")
                    {
                        abd.Append("<input class='validate[" + (item[4] == "1" ? "required," : "") + "maxSize[" + item[3] + "],custom[noSpecialCaracters]]' myid='" + item[0] + "' type='text' />\n");
                    }
                    else if (item[2] == "int")
                    {
                        abd.Append("<input class='validate[" + (item[4] == "1" ? "required," : "") + "custom[integer]]' myid='" + item[0] + "' type='text' />\n");
                    }
                    else if (item[2] == "datetime")
                    {
                        abd.Append("<input class='validate[" + (item[4] == "1" ? "required," : "") + "custom[date]]'    onClick='WdatePicker()'      myid='" + item[0] + "' type='text' />\n");
                    }
                    else if (item[2] == "float")
                    {
                        abd.Append("<input class='validate[" + (item[4] == "1" ? "required," : "") + "custom[number]]' myid='" + item[0] + "' type='text' />\n");
                    }
                    abd.Append("	</div>\n");
                    abd.Append("	</div>\n");
                }
            }

            a = @"<div class='form-actions'>
								<button type='button' class='btn btn-primary' id='btnsave'>保存</button>
                                <input type='button'  class='btn' value='取消' id='btncancel' />
							  </div>

							</fieldset>
						  </form>
					</div>
				</div> 
			</div>


<div class='row-fluid sortable' id='divlist'>
				<div class='box span12'>
					<div class='box-header well' data-original-title>
						<h2><i class='icon-edit'></i> 数据列表</h2>
						<div class='box-icon'>
							<a href='#' class='btn btn-minimize btn-round'><i class='icon-chevron-up'></i></a>
							<a href='#' class='btn btn-close btn-round'><i class='icon-remove'></i></a>
						</div>
					</div>
					<div class='box-content'>
                    <div align='center'>
                    
                    <span class='icon32 icon-color icon-add' id='btnadd'></span>
						<table class='table table-striped table-bordered bootstrap-datatable datatable'>
						  <thead>
							  <tr>




";
            abd.Append(a + "\n");


            foreach (string[] item in list)
            {
                if (item[0] == "ID" || item[0] == "CreatTime") continue;

                abd.Append("<th>" + item[1] + "</th>\n");
            }
            abd.Append("<th width='18%'>操作</th>\n");

            a = @"</tr>
						  </thead>
						  <tbody id='tbodydatalist'>
						  
							</tbody>
							</table>
                            <span class='icon32 icon-color icon-arrowthick-s' id='btnmore'></span>
					</div>
                  </div>
				</div> 
			</div>";

            abd.Append(a + "\n");

            a = @"	<!-- jQuery UI -->
	<script src='js/jquery-ui-1.8.21.custom.min.js'></script>
	<!-- transition / effect library -->
	<script src='js/bootstrap-transition.js'></script>
	<!-- alert enhancer library -->
	<script src='js/bootstrap-alert.js'></script>
	<!-- modal / dialog library -->
	<script src='js/bootstrap-modal.js'></script>
	<!-- custom dropdown library -->
	<script src='js/bootstrap-dropdown.js'></script>
	<!-- scrolspy library -->
	<script src='js/bootstrap-scrollspy.js'></script>
	<!-- library for creating tabs -->
	<script src='js/bootstrap-tab.js'></script>
	<!-- library for advanced tooltip -->
	<script src='js/bootstrap-tooltip.js'></script>
	<!-- popover effect library -->
	<script src='js/bootstrap-popover.js'></script>
	<!-- button enhancer library -->
	<script src='js/bootstrap-button.js'></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src='js/bootstrap-collapse.js'></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src='js/bootstrap-carousel.js'></script>
	<!-- autocomplete library -->
	<script src='js/bootstrap-typeahead.js'></script>
	<!-- tour library -->
	<script src='js/bootstrap-tour.js'></script>
	<!-- library for cookie management -->
	<script src='js/jquery.cookie.js'></script>
	<!-- calander plugin -->
	<script src='js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>
 
	<!-- select or dropdown enhancer -->
	<script src='js/jquery.chosen.min.js'></script>
	<!-- checkbox, radio, and file input styler -->
	<script src='js/jquery.uniform.min.js'></script>
	<!-- plugin for gallery image view -->
	<script src='js/jquery.colorbox.min.js'></script>
	<!-- rich text editor library -->
	<script src='js/jquery.cleditor.min.js'></script>
	<!-- notification plugin -->
	<script src='js/jquery.noty.js'></script>
	<!-- file manager library -->
	<script src='js/jquery.elfinder.min.js'></script>
	<!-- star rating plugin -->
	<script src='js/jquery.raty.min.js'></script>
	<!-- for iOS style toggle switch -->
	<script src='js/jquery.iphone.toggle.js'></script>
	<!-- autogrowing textarea plugin -->
	<script src='js/jquery.autogrow-textarea.js'></script>
	<!-- multiple file upload plugin -->
	<script src='js/jquery.uploadify-3.1.min.js'></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src='js/jquery.history.js'></script>
	<!-- application script for Charisma demo -->
	<script src='js/charisma.js'></script>
</body>
</html>";
            abd.Append(a + "\n");

            return abd.ToString();
        }
    }
}