﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    //ajax处理
    public class ICGAJaxDAL
    {
        //创建ajax页面文件
        public string NewCSFileAspx(string namespacename, string classname, string tablename, string path, List<string[]> list)
        {
            StringBuilder sbd = new StringBuilder();

            sbd.Append("<%@ Page Language='C#' AutoEventWireup='true' CodeBehind='" + tablename + ".aspx.cs' Inherits='" + namespacename + "." + tablename + "ajax' %>");

            string f = string.Format("{0}", '"');

            Datahelper.NewFile(path, classname + ".aspx", sbd.ToString().Replace("'", f));

            return "";
        }

        //创建ajax-cs文件
        public string NewCSFileAjaxCS(string namespacename, string classname, string tablename, string path, List<string[]> list)
        {
            StringBuilder sbd = new StringBuilder();

            sbd.Append(GetheaderAndLoad(namespacename, classname, tablename));

            sbd.Append(getway());
            
            sbd.Append(GetAddOrUpdatestring(tablename, list));

            sbd.Append("}\n");
            sbd.Append("}\n");


            string f = string.Format("{0}", '"');

            Datahelper.NewFile(path, classname + ".aspx.cs", sbd.ToString().Replace("'", f).Replace("$", "'"));//单引号换双引号

            return "";
        }

        private string GetheaderAndLoad(string namespacename, string classname, string tablename)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("using System;\n");
            sbd.Append("using System.Collections.Generic;\n");
            sbd.Append("using System.Text;\n");
            sbd.Append("using System.Data;\n");
            sbd.Append("using System.Data.SqlClient;\n");
            sbd.Append("using DBS;\n");

            sbd.Append(" namespace  " + namespacename + "." + tablename + "ajax\n");
            sbd.Append("{\n");

            sbd.Append("//ajax底层处理代码\n");
            sbd.Append("public partial class " + classname + "ajax  : System.Web.UI.Page\n");
            sbd.Append("{\n");
            sbd.Append(" //实例化\n");
            sbd.Append("DBHelper DB = new DBHelper();\n");
            sbd.Append(tablename + "OP DAL = new " + tablename + "OP();\n");
            sbd.Append("protected void Page_Load(object sender, EventArgs e)\n");
            sbd.Append("{\n");
            sbd.Append("object obj = Request.Form.Get('type');\n");

            sbd.Append("if (obj == null || obj.ToString() == '') { obj = null; Response.Write('nodata'); Response.End(); }//非法请求end\n");

            sbd.Append("string type = obj.ToString();\n");

            sbd.Append("obj = null;\n");

            sbd.Append(@"			//获取对应的枚举项
            if (type == 'getenumitembyid')
                type = GetEnumItemData(Request.Form.Get('myenumid'), Request.Form.Get('userid'));
            //新增
            else if (type == 'add')
                type = AddOrUpdateDB();
            //获取用户定义数量的数据列表
            else if (type == 'getlist')
                type = GetTopNumberData();
            //获取一天修改数据
            else if (type == 'getone')
                type = getone(Request.Form.Get('id').Trim());
            //删除
            else if (type == 'delete')
                type = delete(Request.Form.Get('id'), Request.Form.Get('userid'));

            Response.Write(type);
            Response.End();
        }");
            sbd.Append("\n");

            return sbd.ToString();
        }


        private string getway()
        {
            string a = @"		//删除
        private string delete(string id, string userid)
        {
            return DAL.DeleteDB(id);
        }

        //获取一个
        private string getone(string id)
        {
            id = CommonData.DataTableToJson(DAL.GetOneDataByGuid(id));
            id = id == '' ? 'nodata' : id;
            return id;
        }

        //获取用户定义数量的数据列表
        private string GetTopNumberData()
        {
            string endtime = Request.Form.Get('endtime');
            if (endtime == '') endtime = DateTime.Now.AddDays(1).ToString();
            endtime = CommonData.DataTableToJson(DAL.GetTopNumberData(int.Parse(Request.Form.Get('pagesize')), endtime));
            if (endtime == '')
                endtime = 'nodata';
            return endtime;
        }
		
	    //获取枚举项BY ID
        private string GetEnumItemData(string enumid, string userid)
        {
            if (enumid == null || enumid == '' || userid == null || userid == '') return CommonData.nodata;

            string sql = string.Format('select  NAME,VAL from ICGEnumItemTab where enumid=${0}$ and userid=${1}$ ', enumid, userid);
            sql = CommonData.DataTableToJson(DB.GetDataTable(sql, null));

            if (sql == '') sql = CommonData.nodata;
            return sql;
        }

		";

            return a + "\n";
        }

        //新增或者修改方法字符串
        private string GetAddOrUpdatestring(string tablename, List<string[]> list)
        {
            StringBuilder sbd = new StringBuilder();
            sbd.Append("private string AddOrUpdateDB()\n");
            sbd.Append("{\n");
            sbd.Append("string[] a = Request.Form.Get('myidarray').Split($,$);\n");
            sbd.Append("string guid = Request.Form.Get('" + tablename + "ID');\n");
            sbd.Append("if (guid == '-1')\n");
            sbd.Append("{\n");

            sbd.Append(" guid = Guid.NewGuid().ToString();\n");

            sbd.Append("  DAL.AddDB(guid");

            for (int k = 0; k < list.Count; k++)
            {
                if (list[k][0] == "ID" || list[k][0] == "CreatTime") continue;

                sbd.Append(", a[" + (k - 1) + "]");
            }
            sbd.Append(", guid);\n");

            sbd.Append(" a = null;\n");

            sbd.Append("return guid;\n");
            sbd.Append("}\n");


            sbd.Append("else\n");
            sbd.Append("{\n");
            sbd.Append("  DAL.UpdateDB(guid");
            for (int k = 0; k < list.Count; k++)
            {
                if (list[k][0] == "ID" || list[k][0] == "CreatTime") continue;

                sbd.Append(", a[" + (k - 1) + "]");
            }
            sbd.Append(", guid);\n");
            sbd.Append(" a = null;\n");
            sbd.Append("return guid;\n");
            sbd.Append("}\n");
            sbd.Append("}\n");

            return sbd.ToString();
        }


    }
}