﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DAL
{
    //数据帮助类相关
    public class Datahelper
    {
        /// 写文件
        public static string NewFile(string path, string name, string data)
        {
            if (!System.IO.Directory.Exists(path))
                System.IO.Directory.CreateDirectory(path);

            string pathname = path + name;

            if (File.Exists(pathname))
                File.Delete(pathname);

            FileStream fs = new FileStream(pathname, FileMode.CreateNew, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(data);

            sw.Close();
            fs.Close();

            sw.Dispose();
            fs.Dispose();
            return pathname;
        }
    }
}