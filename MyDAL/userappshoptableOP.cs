using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
namespace MyDAL
{
    public class userappshoptableOP
    {
        //实例化
        DBHelper DB = new DBHelper();
        //新增
        public string AddDB(string ID, string userid, string appname, string isbought, string trycounts, string boughttime, string appicopath, string apppath, string guid)
        {
            string sql = string.Format("insert into userappshoptable (ID,userid,appname,isbought,trycounts,boughttime,appicopath,apppath) values(@ID,@userid,@appname,@isbought,@trycounts,@boughttime,@appicopath,@apppath)");
            SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@userid",userid),
                  new SqlParameter("@appname",appname),
                  new SqlParameter("@isbought",isbought),
                  new SqlParameter("@trycounts",trycounts),
                  new SqlParameter("@boughttime",boughttime),
                  new SqlParameter("@appicopath",appicopath),
                  new SqlParameter("@apppath",apppath)
                        };
            DB.ExecuteNonQuery(sql, sqlpara);
            return guid;
        }
        //修改
        public string UpdateDB(string ID, string userid, string appname, string isbought, string trycounts, string boughttime, string appicopath, string apppath, string guid)
        {
            string sql = string.Format("update  userappshoptable set ID=@ID,userid=@userid,appname=@appname,isbought=@isbought,trycounts=@trycounts,boughttime=@boughttime,appicopath=@appicopath,apppath=@apppath where ID=@guid");
            SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@userid",userid),
                     new SqlParameter("@appname",appname),
                     new SqlParameter("@isbought",isbought),
                     new SqlParameter("@trycounts",trycounts),
                     new SqlParameter("@boughttime",boughttime),
                     new SqlParameter("@appicopath",appicopath),
                     new SqlParameter("@apppath",apppath),
                      new SqlParameter("@guid", guid)
                           };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //删除
        public string DeleteDB(string guid, string userid)
        {
            string sql = string.Format("delete from appmanagertable where ID=@ID and userid=@userid");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid), new SqlParameter("@userid", userid) };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }

        public string Addapp(string id, string userid)
        {
            string sql = "";
            sql = string.Format("select count(0) from appmanagertable where id='{0}' and userid='{1}' ", id, userid);

            if (Convert.ToUInt32(DB.ExecuteScalar(sql, null)) > 0)
                return "-1";//存在

            string tempuserid = Guid.NewGuid().ToString();

            sql = string.Format("insert into appmanagertable([ID],[appid],[appname],[appsrc],[appcount],[appicopath],[userid],[isbought],[boughttime],[trycounts],[CreatTime])  select [ID],[appid],[appname],[appsrc],[appcount],[appicopath],'{1}',[isbought],[boughttime],[trycounts],[CreatTime] from appmanagertable where ID='{0}' and userid='23' ", id, tempuserid);
            DB.ExecuteNonQuery(sql, null);

            sql = string.Format("update appmanagertable set userid='{0}', isbought=1,boughttime=getdate() ,CreatTime=getdate()   where userid='{1}'", userid, tempuserid);
            DB.ExecuteNonQuery(sql, null);

            return "1";
        }


        //获取一条ID值为guid的数据
        public DataTable GetOneDataByGuid(string guid)
        {
            string sql = string.Format("select  ID, userid, appname, isbought, trycounts, boughttime, appicopath, apppath, CreatTime from userappshoptable where ID=@guid");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid) };
            return DB.GetDataTable(sql, para);
        }

        /// <summary>
        /// 查询用户购买的和管理员发布的数据
        /// </summary>
        /// <param name="pagesize"></param>
        /// <param name="endtime"></param>
        /// <returns></returns>
        public DataTable GetTopNumberData(int pagesize, string endtime, string userid)
        {
            string sql = string.Format("select top {0} userid,appname,isbought,trycounts,appcount,boughttime,appicopath, appsrc as apppath, ID,CreatTime from  appmanagertable where CreatTime<'{1}' and    userid='{2}'   or  userid='23'   order by  CreatTime  DESC, isbought DESC ", pagesize, endtime, userid);
            DataTable dt = DB.GetDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                int length = dt.Rows.Count;
                HashSet<string> dtname = new HashSet<string>();
                for (int i = 0; i < length; i++)
                {
                    if (dtname.Contains(dt.Rows[i]["appname"].ToString()))
                    {
                        dt.Rows.RemoveAt(i);
                        length--;
                        continue;
                    }
                    dtname.Add(dt.Rows[i]["appname"].ToString());
                    dt.Rows[i]["isbought"] = dt.Rows[i]["userid"].ToString() == userid ? "1" : "0";
                }
                dtname = null;//清掉指针
            }
            return dt;
        }
    }
}