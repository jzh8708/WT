using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;

namespace MyDAL
{
    public class usertableOP
    {
        //实例化
        DBHelper DB = new DBHelper();
        //新增
        public string AddDB(string ID, string userid, string userfileid, string username, string userpass, string usergroupid, string isallowlogin, string userleftmoney, string usercount, string userheadlinkname, string userlastlogintime, string guid, string usertype, string usernamenike)
        {
            string sql = string.Format("insert into usertable (ID,userid,userfileid,username,userpass,usergroupid,isallowlogin,userleftmoney,usercount,userheadlinkname,userlastlogintime,usertype,usernamenike) values(@ID,@userid,@userfileid,@username,@userpass,@usergroupid,@isallowlogin,@userleftmoney,@usercount,@userheadlinkname,@userlastlogintime,@usertype,@usernamenike)");
            SqlParameter[] sqlpara = new SqlParameter[]
                        {
                      new SqlParameter("@ID",ID),
                      new SqlParameter("@userid",userid),
                      new SqlParameter("@userfileid",userfileid),
                      new SqlParameter("@username",username),
                      new SqlParameter("@userpass",userpass),
                      new SqlParameter("@usergroupid",usergroupid),
                      new SqlParameter("@isallowlogin",isallowlogin),
                      new SqlParameter("@userleftmoney",userleftmoney),
                      new SqlParameter("@usercount",usercount),
                      new SqlParameter("@userheadlinkname",userheadlinkname),
                      new SqlParameter("@userlastlogintime",DateTime.Now.ToString()),
                      new SqlParameter("@usertype",usertype),
                      new SqlParameter("@usernamenike",usernamenike)
                        };
            DB.ExecuteNonQuery(sql, sqlpara);
            return guid;
        }
        //修改
        public string UpdateDB(string username, string userpass, string usergroupid, string isallowlogin, string userleftmoney, string usercount, string userheadlinkname, string guid, string usertype,string usernamenike)
        {
            string sql = string.Format("update  usertable set username=@username,userpass=@userpass,usergroupid=@usergroupid,isallowlogin=@isallowlogin,userleftmoney=@userleftmoney,usercount=@usercount,userheadlinkname=@userheadlinkname,usertype=@usertype,usernamenike=@usernamenike  where ID=@guid");
            SqlParameter[] para = new SqlParameter[] 
                          {
                    new SqlParameter("@username",username),
                    new SqlParameter("@userpass",userpass),
                    new SqlParameter("@usergroupid",usergroupid),
                    new SqlParameter("@isallowlogin",isallowlogin),
                    new SqlParameter("@userleftmoney",userleftmoney),
                    new SqlParameter("@usercount",usercount),
                    new SqlParameter("@userheadlinkname",userheadlinkname),
                    new SqlParameter("@usertype",usertype),
                    new SqlParameter("@usernamenike",usernamenike),
                    new SqlParameter("@guid", guid)
                           };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //删除
        public string DeleteDB(string guid)
        {
            string sql = string.Format("delete from usertable where ID=@ID");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid) };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //获取一条ID值为guid的数据
        public DataTable GetOneDataByGuid(string guid)
        {
            string sql = string.Format("select usernamenike,usertype, ID, userid, userfileid, username, userpass, usergroupid, isallowlogin, userleftmoney, usercount, userheadlinkname, userlastlogintime, CreatTime from usertable where ID=@guid");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid) };
            return DB.GetDataTable(sql, para);
        }
        //获取用户定义数量的数据列表
        public DataTable GetTopNumberData(int pagesize, string endtime)
        {
            string sql = string.Format("select top {0} userid,groupname,userfileid,username,userpass,usergroupid,isallowlogin,userleftmoney,usercount,userheadlinkname,userlastlogintime, usertable.ID as ID,usertable.CreatTime as CreatTime from usertable,grouptable  where  grouptable.groupid=usertable.usergroupid  and usertable.CreatTime<'{1}' order by usertable.CreatTime DESC", pagesize, endtime);
            return DB.GetDataTable(sql, null);
        }
        /// <summary>
        /// 查询个人桌面上的app应用数据【ok】
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <returns></returns>
        public DataTable GetList(string userid)
        {
            string sql = string.Format("select ID,appname,appsrc,appicopath  from appmanagertable,appmanageruser  where  appmanagertable.ID=appmanageruser.appid and appmanageruser.userid='{0}' and (usebegintime<getdate() and useendtime>getdate() or isbought=1  or  trycounts>0) ", userid);
            return DB.GetDataTable(sql, null);
        }

        /// <summary>
        /// 获取用户个人信息
        /// </summary>
        /// <param name="userid">用户id</param>
        /// <returns></returns>
        public DataTable GetUserInfo(string userid)
        {
            string sql = string.Format("select * from usertable where userid='{0}' ", userid);
            return DB.GetDataTable(sql, null);
        }

        /// <summary>
        /// 获取消息
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public DataTable GetNews(string userid, int isread)
        {
            string sql = string.Format("select ID,fromname,content from UserNews where touserid='{0}' and isread={1} ", userid, isread);

            if (isread != 1 && isread != 0)
                sql = string.Format("select ID,fromname,content  from UserNews where touserid='{0}' ");

            return DB.GetDataTable(sql);
        }

        /// <summary>
        /// 查询是否存在
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string Getexists(string name)
        {
            string sql = string.Format("select count(0) from usertable where username='{0}'", name);
            return DB.ExecuteSingleString(sql);
        }
    }
}