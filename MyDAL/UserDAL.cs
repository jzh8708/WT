﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DBS;
using System.Data.SqlClient;

namespace MyDAL
{
    //amdin用戸管理(youtokanri) class
    public class UserDAL
    {
        DBHelper DB = new DBHelper();

        //新增到我的项目table表
        public string AddDB(string userid, string proid, string tablename, string tablevalues)
        {
            string guid = Guid.NewGuid().ToString();
            string sql = string.Format("insert into ICGUserTable(userid, proid, tableid, tablename, tablevalues, creattime) values(@userid,@proid,@tableid,@tablename,@tablevalues,@creattime)");
            SqlParameter[] sqlpara = new SqlParameter[]
                   {
                       new SqlParameter("@userid",userid),
                       new SqlParameter("@proid",proid),
                       new SqlParameter("@tablename",tablename),
                       new SqlParameter("@tableid",guid),
                       new SqlParameter("@tablevalues",tablevalues),
                       new SqlParameter("@creattime",DateTime.Now)
                   };
            DB.ExecuteNonQuery(sql, sqlpara);
            return guid;
        }

        //修改table
        public int Update(string tablename, string val, string id)
        {
            string sql = string.Format("update ICGUserTable set tablename='{0}',tablevalues='{1}', creattime='{3}'  where tableid='{2}'", tablename, val, id, DateTime.Now);
            return DB.ExecuteNonQuery(sql, null);
        }
    }
}
