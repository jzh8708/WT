﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MyDAL
{
    /// <summary>
    /// 公共管理类库
    /// </summary>
    public class CommonData
    {

        public static string nodata = "nodata";

        /// <summary>
        /// datatable转换为json对象
        /// </summary>
        /// <param name="dt">表</param>
        /// <returns>string</returns>
        public static string DataTableToJson(DataTable dt)
        {
            if (dt == null || dt.Rows.Count == 0)
                return "";

            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("[");//转换成多个model的形式 
            int count = dt.Rows.Count;
            for (int i = 0; i < count; i++)
            {
                strbuild.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    strbuild.Append("\"");
                    strbuild.Append(dt.Columns[j].ColumnName);
                    strbuild.Append("\":\"");
                    strbuild.Append(dt.Rows[i][j].ToString());
                    strbuild.Append("\",");
                }
                strbuild.Remove(strbuild.Length - 1, 1);
                strbuild.Append("},");
            }
            strbuild.Remove(strbuild.Length - 1, 1);
            strbuild.Append("]");
            return strbuild.ToString();
        }
    }
}