using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
namespace MyDAL
{
    public class UserNewsOP
    {
        //实例化
        DBHelper DB = new DBHelper();
        //新增
        public string AddDB(string ID, string fromname, string touserid, string toname, string content, string isread, string guid)
        {
            string sql = string.Format("insert into UserNews (ID,fromname,touserid,toname,content,isread) values(@ID,@fromname,@touserid,@toname,@content,@isread)");
            SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@fromname",fromname),
                  new SqlParameter("@touserid",touserid),
                  new SqlParameter("@toname",toname),
                  new SqlParameter("@content",content),
                  new SqlParameter("@isread",isread)
                        };
            DB.ExecuteNonQuery(sql, sqlpara);
            return guid;
        }
        //修改
        public string UpdateDB(string ID, string fromname, string touserid, string toname, string content, string isread, string guid)
        {
            string sql = string.Format("update  UserNews set ID=@ID,fromname=@fromname,touserid=@touserid,toname=@toname,content=@content,isread=@isread where ID=@guid");
            SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@fromname",fromname),
                     new SqlParameter("@touserid",touserid),
                     new SqlParameter("@toname",toname),
                     new SqlParameter("@content",content),
                     new SqlParameter("@isread",isread),
                      new SqlParameter("@guid", guid)
                           };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //删除
        public string DeleteDB(string guid)
        {
            string sql = string.Format("delete from UserNews where ID=@ID");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid) };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //获取一条ID值为guid的数据
        public DataTable GetOneDataByGuid(string guid)
        {
            string sql = string.Format("select  ID, fromname, touserid, toname, content, isread, CreatTime from UserNews where ID=@guid");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid) };
            return DB.GetDataTable(sql, para);
        }
        //获取用户定义数量的数据列表
        public DataTable GetTopNumberData(int pagesize, string endtime)
        {
            string sql = string.Format("select top {0} fromname,touserid,toname,content,isread, ID,CreatTime from UserNews where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
            return DB.GetDataTable(sql, null);
        }
    }
}