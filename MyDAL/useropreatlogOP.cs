using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DBS;
namespace MyDAL
{
    public class useropreatlogOP
    {
        //实例化
        DBHelper DB = new DBHelper();
        //新增
        public string AddDB(string ID, string userid, string title, string opreatsrc, string username, string opreattime, string guid)
        {
            string sql = string.Format("insert into useropreatlog (ID,userid,title,opreatsrc,username,opreattime) values(@ID,@userid,@title,@opreatsrc,@username,@opreattime)");
            SqlParameter[] sqlpara = new SqlParameter[]
                        {
                  new SqlParameter("@ID",ID),
                  new SqlParameter("@userid",userid),
                  new SqlParameter("@title",title),
                  new SqlParameter("@opreatsrc",opreatsrc),
                  new SqlParameter("@username",username),
                  new SqlParameter("@opreattime",opreattime)
                        };
            DB.ExecuteNonQuery(sql, sqlpara);
            return guid;
        }
        //修改
        public string UpdateDB(string ID, string userid, string title, string opreatsrc, string username, string opreattime, string guid)
        {
            string sql = string.Format("update  useropreatlog set ID=@ID,userid=@userid,title=@title,opreatsrc=@opreatsrc,username=@username,opreattime=@opreattime where ID=@guid");
            SqlParameter[] para = new SqlParameter[] 
                          {
                     new SqlParameter("@ID",ID),
                     new SqlParameter("@userid",userid),
                     new SqlParameter("@title",title),
                     new SqlParameter("@opreatsrc",opreatsrc),
                     new SqlParameter("@username",username),
                     new SqlParameter("@opreattime",opreattime),
                      new SqlParameter("@guid", guid)
                           };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //删除
        public string DeleteDB(string guid)
        {
            string sql = string.Format("delete from useropreatlog where ID=@ID");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@ID", guid) };
            return DB.ExecuteNonQuery(sql, para).ToString();
        }
        //获取一条ID值为guid的数据
        public DataTable GetOneDataByGuid(string guid)
        {
            string sql = string.Format("select  ID, userid, title, opreatsrc, username, opreattime, CreatTime from useropreatlog where ID=@guid");
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@guid", guid) };
            return DB.GetDataTable(sql, para);
        }
        //获取用户定义数量的数据列表
        public DataTable GetTopNumberData(int pagesize, string endtime)
        {
            string sql = string.Format("select top {0} userid,title,opreatsrc,username,opreattime, ID,CreatTime from useropreatlog where CreatTime<'{1}' order by CreatTime DESC", pagesize, endtime);
            return DB.GetDataTable(sql, null);
        }
    }
}